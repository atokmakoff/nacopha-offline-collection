## CircleCI config
CircleCI is configured to build and CI all branches. For commits on `master`, it will also deploy the app directly to DHIS.

You need to make sure the following environment variables are configured in CircleCI:

  1. `dhisUser` set the username of a user in DHIS that can deploy apps, probably `admin`
  1. `dhisPass` as the password for that user
  1. `dhisUrl` as the base URL for the DHIS server, e.g. http://mydhis.example.com

