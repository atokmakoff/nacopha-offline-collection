#!/bin/bash
# builds a binary that can be deployed to DHIS2
outputDir=dist
binary=nacopha-webapp.zip

sedi () {
    sed --version >/dev/null 2>&1 && sed -i -- "$@" || sed -i "" "$@"
}

set -euo pipefail
cd `dirname "$0"`

rollbarEnv=${1:?pass 'dev', 'training', 'production' for the name of the Rollbar env as the first param}

rm -fr $outputDir
# TODO build in docker would produce a cleaner, more reproducable result. Something like:
#  - spin up a docker container with yarn, use: cypress/base:8
#  - run: yarn install --frozen-lockfile && yarn build
#  - chown the zip to the correct user ID
yarn run build
cp -r logo manifest.webapp $outputDir
cd $outputDir
# values aren't read from process.env at build time, so we need to hardcode a placeholder then post-process
# the built binary to replace the value.
sedi "s/%%ROLLBAR_ENV%%/$rollbarEnv/" ./static/js/app.*.js
zip -r $binary *
echo "Done, deployable is $PWD/$binary"
