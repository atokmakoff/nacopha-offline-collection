// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// import 'assets/css/nacopha-vue.css'
import * as zz from './components/common'
import { initWithDhisDbLocale } from './i18-init'

Vue.use(BootstrapVue)
Vue.config.productionTip = false

async function init () {
  const i18n = await initWithDhisDbLocale()
  /* eslint-disable no-new */
  new Vue({
    el: '#app',
    i18n: i18n,
    router,
    components: { App },
    template: '<App/>'
  })
}

init().catch(err => {
  zz.consoleError('Failed to init Vue', err)
})
