import Vue from 'vue'
import Router from 'vue-router'
import AdminComponent from '@/components/AdminComponent'
import HomeComponent from '@/components/HomeComponent'
import DataEntryFormsComponent from '@/components/DataEntryFormsComponent'
import UserMgmtComponent from '@/components/UserMgmtComponent'
import EmpGroupMgmtComponent from '@/components/EmpGroupMgmtComponent'
import MessagesComponent from '@/components/MessagesComponent'
import * as zz from '@/components/common'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/home'
    }, {
      path: '/home',
      component: HomeComponent,
      beforeEnter: onlyAllowSautiYetuProgram
    }, {
      name: zz.dataEntryRouteName,
      path: '/data-entry-forms',
      component: DataEntryFormsComponent
    }, {
      path: zz.userMgmtRoute,
      component: UserMgmtComponent,
      beforeEnter: onlyAllowSautiYetuProgram
    }, {
      path: '/emp-group-mgmt',
      component: EmpGroupMgmtComponent,
      beforeEnter: onlyAllowSautiYetuProgram
    }, {
      path: '/messages',
      component: MessagesComponent,
      beforeEnter: onlyAllowSautiYetuProgram
    }, {
      path: '/admin',
      component: AdminComponent
    }, {
      path: '*',
      redirect: '/home'
    }
  ]
})

async function onlyAllowSautiYetuProgram (to, from, next) {
  const isSautiYetu = await zz.isSautiYetuProgram()
  if (isSautiYetu) {
    return next()
  }
  return next({replace: true, name: zz.dataEntryRouteName}) // redirect to data entry page
}
