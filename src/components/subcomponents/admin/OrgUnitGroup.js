import axios from 'axios'
import * as zz from '../../common'

export const OrgUnitGroup = {
  name: 'OrgUnitGroup',
  mixins: [zz.commonMixin],
  data () {
    return {
      groups: [],
      orgUnitGroup: null,
      uiState: 'initial'
    }
  },
  created () {
    this.fetchFormValues()
  },
  methods: {
    async fetchFormValues () {
      this.uiState = 'fetching'
      try {
        await this._fetchOptions()
        await this._fetchExistingValue()
      } catch (err) {
        this.mixinLastDitchErrorHandler('Failed to populate form.', err)
      } finally {
        this.uiState = 'fetched'
      }
    },
    async _fetchOptions () {
      try {
        const resp = await axios.get(`${zz.apiBaseUrl}/organisationUnitGroups`, {
          params: { paging: false }
        })
        const groups = resp.data.organisationUnitGroups
        this.groups = groups
      } catch (err) {
        throw zz.chainedError(err, 'Failed to fetch orgUnit groups')
      }
    },
    async _fetchExistingValue () {
      try {
        const existingValue = await zz.getOrgUnitGroupId()
        this.orgUnitGroup = existingValue
      } catch (err) {
        throw zz.chainedError(err, 'Failed to fetch existing orgUnit group value')
      }
    },
    isValid () {
      return !!this.orgUnitGroup
    },
    async save () {
      this.uiState = 'saving'
      try {
        await zz.saveOrgUnitGroupId(this.orgUnitGroup)
        this.uiState = 'success'
      } catch (err) {
        this.uiState = 'failed'
        this.mixinLastDitchErrorHandler(`Failed while saving orgUnit group`, err)
      }
    }
  }
}
