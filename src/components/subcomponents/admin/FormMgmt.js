import XlsxPopulate from 'xlsx-populate/lib/XlsxPopulate'
import axios from 'axios'
import * as moment from 'moment'
import * as zz from '@/components/common'

export const FormMgmt = {
  name: 'FormMgmt',
  props: [
    'programName',
    'templateTypeName',
    'templateFilenameFragment'
  ],
  data () {
    return {
      templateLastModified: [],
      processingState: 'initial',
      templateFile: null,
      uploadResult: null,
      isLoading: false,
      availableLocales: [],
      templateLocale: null
    }
  },
  created () {
    this.fetchFormValues()
  },
  methods: {
    async fetchFormValues () {
      this.isLoading = true
      const tasks = [
        this._populateTemplateDetails(),
        this._populateLocales()
      ]
      try {
        await Promise.all(tasks)
      } catch (err) {
        const msg = 'Failed to populate form.'
        zz.consoleError(msg, err)
        alert(msg + ' Check console for details.')
      } finally {
        this.isLoading = false
      }
    },
    async _populateLocales () {
      try {
        const resp = await axios.get(`${zz.apiBaseUrl}/locales/db`)
        this.availableLocales = resp.data
      } catch (err) {
        throw zz.chainedError(err, 'Failed to get DB locales')
      }
    },
    async _populateTemplateDetails () {
      try {
        const docs = await this._getExistingTemplates()
        if (!docs) {
          return
        }
        const docsByLocale = docs.reduce((accum, curr) => {
          const currLocale = curr.displayName.substr(-2)
          if (accum[currLocale]) {
            accum[currLocale] = `(WARNING: found more than 1 template for locale='${currLocale}', where we expected only 1.` +
            ` Upload a new template to fix the problem.)`
            return accum
          }
          const utcDate = moment(new Date(curr.lastUpdated + 'Z'))
          accum[currLocale] = utcDate.format()
          return accum
        }, {})
        this.templateLastModified = Object.keys(docsByLocale).reduce((accum, curr) => {
          accum.push({
            locale: curr,
            lastModifiedDate: docsByLocale[curr]
          })
          return accum
        }, [])
      } catch (err) {
        throw zz.chainedError(err, 'Failed to get details about any existing template.')
      }
    },
    async templateFileSelected () {
      this.templateFile = this.$refs.templateFile.files[0]
    },
    async downloadExisting (locale) {
      const bytes = await zz.getTemplateBytesForLocale(this.templateFilenameFragment, locale)
      const workbook = await XlsxPopulate.fromDataAsync(bytes)
      const workbookBytes = await workbook.outputAsync()
      const filename = `${this.templateFilenameFragment}_${locale}.xlsx`
      zz.triggerDownload(workbookBytes, filename)
    },
    async doUpload () {
      this.processingState = 'loading'
      this.uploadResult = null
      // check if there's an existing document (or multiple) that we'll need to delete. The orphaned fileResource will be cleaned up by DHIS
      try {
        await this._cleanUpExistingTemplate()
      } catch (err) {
        zz.consoleError(`Failed to delete any existing documents for locale='${this.templateLocale}'`, err)
        this.processingState = 'failed'
        return
      }
      // create a fileResource
      const formData = new FormData()
      const filename = zz.buildFormTemplateName(this.templateFilenameFragment, this.templateLocale)
      formData.append('file', this.templateFile, filename)
      const opts = {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
      let fileResourceId
      try {
        const frResp = await axios.post(`${zz.apiBaseUrl}/fileResources`, formData, opts)
        fileResourceId = frResp.data.response.fileResource.id
        zz.consoleLog(`Uploaded template with fileResource ID='${fileResourceId}'`)
      } catch (err) {
        zz.consoleError('Failed to upload template file', err)
        this.processingState = 'failed'
        return
      }
      // link the fileResource to a document
      let docUid
      try {
        const dResp = await axios.post(`${zz.apiBaseUrl}/documents`, {
          name: filename,
          external: false,
          url: fileResourceId
        })
        docUid = dResp.data.response.uid
        zz.consoleLog(`Created document with uid='${docUid}'`)
      } catch (err) {
        zz.consoleError('Failed to create a document', err)
        this.processingState = 'failed'
        return
      }
      this.uploadResult = {
        fileResourceId,
        docUid
      }
      this.processingState = 'success'
      await this._populateTemplateDetails()
    },
    async _getExistingTemplates () {
      const getDocResp = await axios.get(`${zz.apiBaseUrl}/documents`, {
        params: {
          filter: `displayName:like:${this.templateFilenameFragment}`,
          fields: 'id,displayName,lastUpdated'
        }
      })
      const docs = getDocResp.data.documents
      if (docs.length === 0) {
        return null
      }
      return docs
    },
    async _cleanUpExistingTemplate () {
      const allDocs = await this._getExistingTemplates()
      if (!allDocs) {
        return
      }
      const formTemplateName = zz.buildFormTemplateName(this.templateFilenameFragment, this.templateLocale)
      const docs = allDocs.filter(e => e.displayName === formTemplateName)
      if (docs.length > 1) {
        console.warn(`Expected 1 existing document to be found for locale='${this.templateLocale}', but found ${docs.length}. Deleting them all!`)
      }
      for (const curr of docs) {
        const id = curr.id
        try {
          await axios.delete(`${zz.apiBaseUrl}/documents/${id}`)
          zz.consoleLog(`Deleted document with id='${id}'`)
        } catch (err) {
          zz.consoleError(`Failed to delete document with id='${id}', you'll have to delete it by hand and try this process again.`)
          throw err
        }
      }
    }
  }
}
