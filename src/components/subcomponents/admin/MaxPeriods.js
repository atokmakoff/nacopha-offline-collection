import * as zz from '../../common'

export const MaxPeriods = {
  name: 'MaxPeriods',
  mixins: [zz.commonMixin],
  data () {
    return {
      theValue: null,
      uiState: 'initial'
    }
  },
  created () {
    this.fetchFormValues()
  },
  methods: {
    async fetchFormValues () {
      this.uiState = 'fetching'
      try {
        await this._fetchExistingValues()
      } catch (err) {
        this.mixinLastDitchErrorHandler('Failed to populate form.', err)
      } finally {
        this.uiState = 'fetched'
      }
    },
    async _fetchExistingValues () {
      try {
        this.theValue = await zz.getMaxReportingPeriods()
      } catch (err) {
        throw zz.chainedError(err, 'Failed to fetch existing max reporting periods value')
      }
    },
    isValid () {
      return this.theValue
    },
    async save () {
      this.uiState = 'saving'
      try {
        await zz.saveMaxReportingPeriods(this.theValue)
        this.uiState = 'success'
      } catch (err) {
        this.uiState = 'failed'
        this.mixinLastDitchErrorHandler(`Failed while saving max reporting periods`, err)
      }
    }
  }
}
