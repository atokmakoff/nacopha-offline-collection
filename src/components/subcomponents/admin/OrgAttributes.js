import axios from 'axios'
import * as zz from '../../common'

export const OrgAttributes = {
  name: 'OrgAttributes',
  mixins: [zz.commonMixin],
  data () {
    return {
      attributes: [],
      village: null,
      groupRegoNum: null,
      socialGroup1: null,
      socialGroup2: null,
      socialGroup3: null,
      economicGroup1: null,
      economicGroup2: null,
      economicGroup3: null,
      communityHivGroup1: null,
      communityHivGroup2: null,
      communityHivGroup3: null,
      uiState: 'initial'
    }
  },
  created () {
    this.fetchFormValues()
  },
  methods: {
    async fetchFormValues () {
      this.uiState = 'fetching'
      try {
        await this._fetchOptions()
        await this._fetchExistingValues()
      } catch (err) {
        this.mixinLastDitchErrorHandler('Failed to populate form.', err)
      } finally {
        this.uiState = 'fetched'
      }
    },
    async _fetchOptions () {
      try {
        const resp = await axios.get(`${zz.apiBaseUrl}/attributes`, {
          params: { paging: false }
        })
        this.attributes = resp.data.attributes
      } catch (err) {
        throw zz.chainedError(err, 'Failed to fetch orgUnit attributes')
      }
    },
    async _fetchExistingValues () {
      try {
        this.village = await zz.getVillageAttrId()
        this.groupRegoNum = await zz.getGroupRegoNumAttrId()
        this.socialGroup1 = await zz.getSocialGroupAttrId(1)
        this.socialGroup2 = await zz.getSocialGroupAttrId(2)
        this.socialGroup3 = await zz.getSocialGroupAttrId(3)
        this.communityHivGroup1 = await zz.getCommunityHivGroupAttrId(1)
        this.communityHivGroup2 = await zz.getCommunityHivGroupAttrId(2)
        this.communityHivGroup3 = await zz.getCommunityHivGroupAttrId(3)
        this.economicGroup1 = await zz.getEconomicGroupAttrId(1)
        this.economicGroup2 = await zz.getEconomicGroupAttrId(2)
        this.economicGroup3 = await zz.getEconomicGroupAttrId(3)
      } catch (err) {
        throw zz.chainedError(err, 'Failed to fetch existing orgUnit attribute values')
      }
    },
    isValid () {
      return this.village &&
        this.socialGroup1 && this.socialGroup2 && this.socialGroup3 &&
        this.communityHivGroup1 && this.communityHivGroup2 && this.communityHivGroup3 &&
        this.economicGroup1 && this.economicGroup2 && this.economicGroup3
    },
    async save () {
      this.uiState = 'saving'
      try {
        const promises = [
          zz.saveVillageAttrId(this.village),
          zz.saveGroupRegoNumAttrId(this.groupRegoNum),
          zz.saveSocialGroupAttrId(1, this.socialGroup1),
          zz.saveSocialGroupAttrId(2, this.socialGroup2),
          zz.saveSocialGroupAttrId(3, this.socialGroup3),
          zz.saveCommunityHivGroupAttrId(1, this.communityHivGroup1),
          zz.saveCommunityHivGroupAttrId(2, this.communityHivGroup2),
          zz.saveCommunityHivGroupAttrId(3, this.communityHivGroup3),
          zz.saveEconomicGroupAttrId(1, this.economicGroup1),
          zz.saveEconomicGroupAttrId(2, this.economicGroup2),
          zz.saveEconomicGroupAttrId(3, this.economicGroup3)
        ]
        await Promise.all(promises)
        this.uiState = 'success'
      } catch (err) {
        this.uiState = 'failed'
        this.mixinLastDitchErrorHandler(`Failed while saving orgUnit attributes`, err)
      }
    }
  }
}
