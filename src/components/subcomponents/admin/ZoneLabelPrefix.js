import * as zz from '../../common'

export const ZoneLabelPrefix = {
  name: 'ZoneLabelPrefix',
  mixins: [zz.commonMixin],
  data () {
    return {
      theValue: null,
      uiState: 'initial'
    }
  },
  created () {
    this.fetchFormValues()
  },
  methods: {
    async fetchFormValues () {
      this.uiState = 'fetching'
      try {
        await this._fetchExistingValues()
      } catch (err) {
        this.mixinLastDitchErrorHandler('Failed to populate form.', err)
      } finally {
        this.uiState = 'fetched'
      }
    },
    async _fetchExistingValues () {
      try {
        this.theValue = await zz.getZoneLabelPrefix()
      } catch (err) {
        throw zz.chainedError(err, 'Failed to fetch existing zone label prefix value')
      }
    },
    isValid () {
      return this.theValue
    },
    async save () {
      this.uiState = 'saving'
      try {
        await zz.saveZoneLabelPrefix(this.theValue)
        this.uiState = 'success'
      } catch (err) {
        this.uiState = 'failed'
        this.mixinLastDitchErrorHandler(`Failed while saving zone label prefix`, err)
      }
    }
  }
}
