import axios from 'axios'
import * as zz from '@/components/common'

export default {
  name: 'DataElements',
  mixins: [zz.commonMixin],
  data () {
    return {
      dataElements: [],
      formDosElements: {}, // key = <programId>_<formId>
      isSYProgramSelectedYet: false,
      programs: [],
      formIds: [1, 2, 3, 4], // TODO read this from input on form
      maxFormCount: 1,
      isValid: false,
      uiState: 'initial'
    }
  },
  created () {
    this.fetchFormValues()
  },
  methods: {
    async fetchFormValues () {
      this.uiState = 'fetching'
      try {
        const promises = [
          this._fetchOptions(),
          this._fetchExistingValues()
        ]
        await Promise.all(promises)
      } catch (err) {
        this.mixinLastDitchErrorHandler('Failed to populate form.', err)
      } finally {
        this.uiState = 'fetched'
      }
    },
    async _fetchOptions () {
      try {
        const resp = await axios.get(`${zz.apiBaseUrl}/dataElements`)
        this.dataElements = resp.data.dataElements
      } catch (err) {
        throw zz.chainedError(err, 'Failed to get the list of data elements.')
      }
    },
    onMaxFormCountChange () {
      const result = []
      for (let i = 1; i <= this.maxFormCount; i++) {
        result.push(i)
      }
      this.formIds = result
    },
    onDosChange () {
      const isAtLeastOneValueEntered = Object.keys(this.formDosElements).some(e => this.formDosElements[e])
      this.isValid = isAtLeastOneValueEntered
    },
    async _fetchExistingValues () {
      const sautiYetuProgramId = await zz.getSautiYetuProgramId()
      this.isSYProgramSelectedYet = !!sautiYetuProgramId
      if (!this.isSYProgramSelectedYet) {
        return
      }
      try {
        const resp = await axios.get(`${zz.apiBaseUrl}/programs`, {
          params: { fields: 'id,displayName', paging: false }
        })
        this.programs = resp.data.programs
        await zz.getMaxFormCount().then(val => {
          this.maxFormCount = val
          this.onMaxFormCountChange()
          const tasks = this.programs.reduce((accum, currProg) => {
            for (const currForm of this.formIds) {
              const key = `${currProg.id}_${currForm}`
              accum.push(async () => { this.formDosElements[key] = await zz.getFormDosId(currProg.id, currForm) })
            }
            return accum
          }, [])
          return Promise.all(tasks.map(e => e()))
        })
        console.debug(`Successfully loaded data element values with maxFormCount=${this.maxFormCount}`)
      } catch (err) {
        throw zz.chainedError(err, 'Failed to get the existing data elements values.')
      }
      this.onDosChange()
    },
    async save () {
      this.uiState = 'saving'
      const suppliedValues = Object.keys(this.formDosElements).filter(e => this.formDosElements[e])
      try {
        const promises = suppliedValues.map(k => {
          const [programId, formId] = k.split('_')
          const value = this.formDosElements[k]
          return zz.saveFormDosId(programId, formId, value)
        })
        promises.push(zz.saveMaxFormCount(this.maxFormCount))
        await Promise.all(promises)
        this.uiState = 'success'
      } catch (err) {
        this.uiState = 'failed'
        this.mixinLastDitchErrorHandler(`Failed while saving data element mappings`, err)
      }
    }
  }
}
