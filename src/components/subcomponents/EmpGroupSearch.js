import * as zz from '../common'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-balham.css'
import 'ag-grid-community'
import {AgGridVue} from 'ag-grid-vue'

export const EmpGroupSearch = {
  name: 'EmpGroupSearch',
  components: {
    'ag-grid-vue': AgGridVue,
  },
  data() {
    return {
      uiStatus: 'initial',
      columnDefs: [
        {headerName: this.$i18n.t('ward'), field: 'wardName', width: 90},
        {
          headerName: this.$i18n.t('group'),
          valueGetter: (p) => p.data.eg.displayName,
          cellRenderer: linkToUserMgmtRenderer,
        },
        {headerName: this.$i18n.t('village'), field: 'village'},
        {headerName: this.$i18n.t('group_reg_num'), field: 'groupRegoNum'},
        {
          headerName: this.$i18n.t('community_hiv_activities'),
          field: 'communityHivActivities',
          cellRenderer: multiLineRenderer,
        },
        {
          headerName: this.$i18n.t('economic_activities'),
          field: 'economicActivities',
          cellRenderer: multiLineRenderer,
        },
        {
          headerName: this.$i18n.t('social_activities'),
          field: 'socialActivities',
          cellRenderer: multiLineRenderer,
        },
      ],
      defaultColDef: {
        valueFormatter: (params) => {
          return params.value || '-'
        },
      },
      rowData: [],
      agGridApi: null,
    }
  },
  created() {
    this.reload()
  },
  methods: {
    async reload() {
      this.uiStatus = 'loading'
      try {
        await this._fetchWardsAndGroups()
        this.uiStatus = 'success'
      } catch (err) {
        zz.consoleError('Failed to search for wards and groups', err)
        this.uiStatus = 'failed'
      }
    },
    getLocaleText() {
      return {
        noRowsToShow: `<h2>${this.$i18n.t('no_table_rows')}</h2>`,
      }
    },
    onGridFilterChanged(params) {
      const displayedRowCount = params.api.getDisplayedRowCount()
      if (displayedRowCount === 0) {
        params.api.showNoRowsOverlay()
      } else {
        params.api.hideOverlay()
      }
    },
    onGridReady(params) {
      params.api.sizeColumnsToFit()
      this.agGridApi = params.api
    },
    cellDoubleClicked(cell) {
      console.log(`${cell.data.egName} has ID ${cell.data.id}`)
    },
    getRowHeight(params) {
      const d = params.data
      const max1 = Math.max(
        d.communityHivActivities.length,
        d.economicActivities.length,
      )
      const max2 = Math.max(max1, d.socialActivities.length)
      const rowCount = max2 || 1
      return rowCount * 25
    },
    async _fetchWardsAndGroups() {
      const wards = await zz.fetchWards()
      const groupVocab = await zz.fetchActivityGroupVocab()
      const attributeIds = await zz.fetchGroupActivityAttributeIds()
      const villageAttrId = await zz.getVillageAttrId()
      const groupRegoNumAttrId = await zz.getGroupRegoNumAttrId()
      this.rowData = wards.reduce((accum, curr) => {
        const wardName = curr.displayName
        for (const currEg of curr.children) {
          const villageName = (
            currEg.attributeValues.find(
              (e) => e.attribute.id === villageAttrId,
            ) || {}
          ).value
          const groupRegoNum = (
            currEg.attributeValues.find(
              (e) => e.attribute.id === groupRegoNumAttrId,
            ) || {}
          ).value
          accum.push({
            id: currEg.id,
            wardName,
            eg: currEg,
            village: villageName,
            groupRegoNum,
            communityHivActivities: getActivities(
              attributeIds.communityHiv,
              currEg.attributeValues,
            ),
            economicActivities: getActivities(
              attributeIds.economic,
              currEg.attributeValues,
            ),
            socialActivities: getActivities(
              attributeIds.social,
              currEg.attributeValues,
            ),
          })
        }
        return accum
      }, [])
      function getActivities(ids, attributeValues) {
        const values = zz.extractActivityGroup(ids, groupVocab, attributeValues)
        return values.filter((e) => e !== null)
      }
    },
  },
}

function multiLineRenderer(params) {
  let result = ''
  for (let i = 0; i < params.value.length; i++) {
    result += `${params.value[i]}<br />`
  }
  return result || '-'
}

function linkToUserMgmtRenderer(params) {
  const paramName = zz.userMgmtOuPreselectQueryParamName
  const route = zz.userMgmtRoute
  const eg = params.data.eg
  return `<a href="#${route}?${paramName}=${eg.id}">${eg.displayName}</a>`
}
