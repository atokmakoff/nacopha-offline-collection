import XlsxPopulate from 'xlsx-populate/lib/XlsxPopulate'
import * as zz from '../common'
const availableUiStates = {
  initial: 'initial',
  failRead: 'failRead',
  failProcessUser: 'failProcessUser',
  failProcessSystem: 'failProcessSystem',
  failVersion: 'failVersion',
  loading: 'loading',
  success: 'success'
}

export const InputUploadForm = {
  name: 'InputUploadForm',
  props: [
    'workbookProcessor'
  ],
  data () {
    return {
      isError: false,
      completedForm: null,
      availableUiStates: availableUiStates,
      validationFailures: [],
      uiState: availableUiStates.initial
    }
  },
  methods: {
    async completedFormSelected () {
      this.completedForm = this.$refs.completedForm.files[0]
    },
    async processSpreadsheet () {
      this.$emit('display-summary', false)
      this.uiState = availableUiStates.loading
      this.validationFailures = []
      let workbook
      try {
        workbook = await XlsxPopulate.fromDataAsync(this.completedForm)
      } catch (err) {
        zz.consoleError(`Failed to read spreadsheet`, err)
        this.uiState = availableUiStates.failRead
        return
      }
      try {
        await this.workbookProcessor(workbook, this.validationFailures)
        if (this.validationFailures.length) {
          this.uiState = availableUiStates.failProcessUser
          const workbookBase64 = await workbook.outputAsync('base64')
          zz.rollbarLog('Form upload failed due to validation failures', {
            validationFailures: this.validationFailures,
            workbook: workbookBase64
          })
          return
        }
        this.uiState = availableUiStates.success
        this.$emit('display-summary', true)
      } catch (err) {
        this.uiState = availableUiStates.failProcessSystem
        zz.consoleError(`Failed while processing workbook`, err)
      }
    }
  }
}
