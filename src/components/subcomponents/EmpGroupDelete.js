import axios from 'axios'
import * as zz from '../common'

export const EmpGroupDelete = {
  name: 'EmpGroupDelete',
  data () {
    return {
      selectedGroup: null,
      uiState: 'initial',
      groups: [],
      memberTeiCount: 0
    }
  },
  created () {
    this._fetchFormValues()
  },
  methods: {
    async _fetchFormValues () {
      try {
        this.uiState = 'fetching'
        const wardsAndGroups = await zz.fetchWardsAndGroups()
        this.groups = wardsAndGroups.filter(e => !e.isWard)
        this.uiState = 'fetched'
      } catch (err) {
        this.uiState = 'fetchingfailed'
        zz.consoleError('Failed to load group names', err)
      }
    },
    isDisplayOptions () {
      return !this.uiState.startsWith('fetching')
    },
    isValid () {
      return this.selectedGroup
    },
    async reload () {
      const stateBefore = this.uiState
      await this._fetchFormValues()
      this.uiState = stateBefore
      this.selectedGroup = null
    },
    async doDelete () {
      this.memberTeiCount = 0
      this.uiState = 'deleting'
      try {
        const resp = await axios.get(`${zz.apiBaseUrl}/trackedEntityInstances`, {
          params: {
            // purposely doing minimal filtering here (no program, etc) because the orgUnit must be truly empty
            fields: 'orgUnit',
            ou: this.selectedGroup,
            paging: false
          }
        })
        this.memberTeiCount = resp.data.trackedEntityInstances.length
        const isGroupEmpty = this.memberTeiCount === 0
        if (!isGroupEmpty) {
          this.uiState = 'failednotempty'
          return
        }
      } catch (err) {
        zz.consoleError(`Failed to count member TEIs of orgUnit='${this.selectedGroup}'`, err)
        this.uiState = 'failed'
        return
      }
      try {
        await axios.delete(`${zz.apiBaseUrl}/organisationUnits/${this.selectedGroup}`)
        zz.resetCache()
      } catch (err) {
        zz.consoleError(`Failed to delete orgUnit record='${this.selectedGroup}'`, err)
        this.uiState = 'failed'
        return
      }
      this.uiState = 'success'
      this.$emit(zz.orgUnitsChangedEvent)
    }
  }
}
