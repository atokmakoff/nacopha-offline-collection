import axios from 'axios'
import * as zz from '@/components/common'
import ZonalMsg from '@/components/subcomponents/ZonalMsgComponent'
const moment = require('moment')

export const CreateTei = {
  name: 'CreateTei',
  mixins: [zz.commonMixin],
  data () {
    const result = {
      uiState: 'initial',
      fullName: '',
      fullNameFailureMsg: '',
      selectedDobYear: '',
      selectedDobMonth: '',
      selectedDobDay: '',
      selectedGender: '',
      genders: [],
      wards: [],
      selectedOrgUnit: '',
      wardsAndGroups: [],
      ctc: null,
      years: [],
      isCtcRequired: false,
      ctcFailureMsg: '',
      collidedCtc: '(no CTC entered)'
    }
    return result
  },
  created () {
    this.fetchFormValues()
  },
  components: {
    'zonal-msg': ZonalMsg
  },
  methods: {
    async fetchFormValues () {
      this.uiState = 'initialising'
      try {
        await Promise.all([
          this._fetchGenders(),
          this._fetchWards()
        ])
        this.years = this._generateYears()
      } catch (err) {
        this.uiState = 'failedinit'
        zz.consoleError('Failed to populate form values', err)
        return
      }
      this.uiState = 'ready'
    },
    async _fetchGenders () {
      try {
        this.genders = await zz.fetchGenders()
      } catch (err) {
        throw zz.chainedError(err, 'Failed to get a list of genders.')
      }
    },
    async _fetchWards () {
      this.wardsAndGroups = await zz.fetchWardsAndGroups()
      this.wards = this.wardsAndGroups.filter(e => e.isWard)
    },
    onOrgUnitSelected () {
      const orgUnit = this.wardsAndGroups.find(e => e.id === this.selectedOrgUnit)
      this.isCtcRequired = !orgUnit.isWard
    },
    isTeiNameValid () {
      const result = zz.validateForSqlInjection(this.fullName, this.$i18n)
      this.fullNameFailureMsg = result.msg
      return result.isValid
    },
    isCtcValid () {
      const result = validateCtc(this.isCtcRequired, this.ctc, this.$i18n)
      this.ctcFailureMsg = result.msg
      return result.isValid
    },
    isDobValidIfSupplied () {
      const dateStr = `${this.selectedDobYear}-${this.selectedDobMonth}-${this.selectedDobDay}`
      if (dateStr.length !== 10) {
        return true
      }
      const mDate = moment(dateStr)
      return mDate.isBefore(moment(today()))
    },
    isFormValid () {
      return this.isTeiNameValid() && this.selectedDobYear && this.selectedDobMonth && this.selectedDobDay &&
        this.isDobValidIfSupplied() && this.selectedGender && this.selectedOrgUnit && this.isCtcValid()
    },
    reset () {
      this.fullName = ''
      this.selectedDobYear = ''
      this.selectedDobMonth = ''
      this.selectedDobDay = ''
      this.selectedGender = ''
      this.selectedOrgUnit = ''
      this.ctc = null
      this.isCtcRequired = false
      this.$refs.fullNameInput.focus()
    },
    _generateYears () {
      const currentYear = new Date().getFullYear()
      const minAgeOfNewTei = 0
      const result = []
      const maxYear = currentYear - minAgeOfNewTei
      const minYear = maxYear - 100
      for (let i = maxYear; i > minYear; i--) {
        result.push(i)
      }
      return result
    },
    async createTei () {
      if (!this.isFormValid()) {
        return
      }
      this.uiState = 'creating'
      let teiId
      let ctcAttrId
      const orgUnit = this.selectedOrgUnit
      try {
        const fullNameAttrId = await zz.getFullNameAttributeId()
        const genderAttrId = await zz.getGenderAttributeId()
        const dobAttrId = await zz.getDobAttributeId()
        const teiType = await zz.getPersonTeiTypeId()
        const dobValue = `${this.selectedDobYear}-${this.selectedDobMonth}-${this.selectedDobDay}`
        const nacophaIdAttrId = await zz.getNacophaIdAttributeId()
        const nacophaIdValue = await getNacophaId(nacophaIdAttrId)
        ctcAttrId = await zz.getCTCAttributeId()
        const body = {
          orgUnit,
          trackedEntityType: teiType,
          attributes: [
            { attribute: fullNameAttrId, value: this.fullName },
            { attribute: dobAttrId, value: dobValue },
            { attribute: genderAttrId, value: this.selectedGender },
            { attribute: nacophaIdAttrId, value: nacophaIdValue },
            { attribute: ctcAttrId, value: this.ctc }
          ]
        }
        const isDuplicate = await this.isDuplicateTei(body)
        if (isDuplicate) {
          this.uiState = 'failed_duplicate'
          return
        }
        const resp = await axios.post(`${zz.apiBaseUrl}/trackedEntityInstances`, body)
        const isImportConflict = (resp.data.response.importSummaries[0].conflicts || [])[0]
        if (isImportConflict) {
          this.uiState = 'failed'
          zz.consoleError('Failed to create a new TEI due to import conflict', resp.data)
          return
        }
        teiId = resp.data.response.importSummaries[0].reference
        console.log(`ID of new record: ${teiId}`)
      } catch (err) {
        const body = err.response.data
        if (err.response && err.response.status === 409) {
          const is409AboutCtc = body.response.importSummaries[0] &&
            body.response.importSummaries[0].conflicts[0] &&
            body.response.importSummaries[0].conflicts[0].value &&
            body.response.importSummaries[0].conflicts[0].value.match(ctcAttrId)
          if (is409AboutCtc) {
            this.collidedCtc = this.ctc // stash it in case the user changes input between error and sending msg
            this.uiState = 'failed_ctc_collision'
            return
          }
          this.uiState = 'failed_409_not_ctc'
        } else {
          this.uiState = 'failed'
        }
        zz.consoleError('Failed to create a new TEI', err)
        return
      }
      const programId = await zz.getProgramId()
      const enrollTeiData = {
        enrollmentDate: today(),
        orgUnit: orgUnit,
        program: programId,
        status: 'ACTIVE',
        trackedEntityInstance: teiId
      }
      try {
        await axios.post(`${zz.apiBaseUrl}/enrollments`, enrollTeiData)
        zz.resetCache()
      } catch (err) {
        this.uiState = 'failed'
        zz.consoleError(`Failed to enroll TEI with ID='${teiId}'`, err)
        return
      }
      this.uiState = 'success'
      this.reset()
      // TODO consider refreshing search table
    },
    async isDuplicateTei (body) {
      if (!body || !body.orgUnit || !body.attributes || !body.trackedEntityType) {
        throw new Error(`Programmer error: supplied body is missing at least one required field, body=${JSON.stringify(body)}`)
      }
      try {
        const nacophaIdAttrId = await zz.getNacophaIdAttributeId()
        const urlWithoutFilters = `${zz.apiBaseUrl}/trackedEntityInstances`
        const params = {
          trackedEntityType: body.trackedEntityType,
          ou: body.orgUnit
        }
        // JS doesn't support repeated object keys and axios doesn't support it either, so we'll roll our own
        const url = body.attributes.filter(e => e.value && e.attribute !== nacophaIdAttrId).reduce((accum, curr) => {
          accum += `filter=${curr.attribute}:EQ:${curr.value}&`
          return accum
        }, urlWithoutFilters + '?')
        const resp = await axios.get(url, { params })
        const count = resp.data.trackedEntityInstances.length
        return count > 0
      } catch (err) {
        throw zz.chainedError(err, `Failed while checking if body=${JSON.stringify(body)} is a duplicate`)
      }
    }
  }
}

function today () {
  const todayInUtc = moment().toISOString().substr(0, 10)
  return todayInUtc
}

export function validateCtc (isCtcRequired, ctc, $i18n) {
  function buildResult (isValid, msg) {
    return {isValid, msg}
  }
  if (!isCtcRequired && !ctc) {
    return buildResult(true, null)
  }
  if (isCtcRequired && !ctc) {
    const msg = $i18n.t('ctc_required_create')
    return buildResult(false, msg)
  }
  const validPatterns = {
    oldShortWithCharFormatRegex: /^[A-Za-z0-9]{3}-[A-Za-z0-9]{3}$/,
    oldShortDigitFormatRegex: /^[0-9]{3}-[0-9]{3}$/,
    fourFourDigitFormatRegex: /^[0-9]{4}-[0-9]{4}$/,
    oldLongDigitFormatRegex: /^[0-9]{5}-[0-9]{6}$/,
    newFormatRegex: /^[0-9]{2}-[0-9]{2}-[0-9]{4}-[0-9]{4}$/,
    fourFourFiveFormatRegex: /^[0-9]{4}-[0-9]{4}-[0-9]{5}$/,
    twoTwoFourNFormatRegex: /^[0-9]{2}-[0-9]{2}-[0-9]{4}-[0-9]{4,9}$/
  }
  const isMatchesAPattern = Object.values(validPatterns).some(p => {
    return p.test(ctc)
  })
  if (!isMatchesAPattern) {
    const msg = $i18n.t('ctc_format_not_ok')
    return buildResult(false, msg)
  }
  return buildResult(true, null)
}

async function getNacophaId (nacophaIdAttrId) {
  try {
    const resp = await axios.get(`${zz.apiBaseUrl}/trackedEntityAttributes/${nacophaIdAttrId}/generate`)
    const result = resp.data.value
    if (!result) {
      throw new Error('Request to generate Nacopha ID succeeded but no value was in the response')
    }
    return result
  } catch (err) {
    throw zz.chainedError(err, 'Failed to generate a Nacopha ID')
  }
}
