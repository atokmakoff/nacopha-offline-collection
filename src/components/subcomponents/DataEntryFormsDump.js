import XlsxPopulate from 'xlsx-populate/lib/XlsxPopulate'
import axios from 'axios'
import * as zz from '@/components/common'
const moment = require('moment')

const loading = 'loading'
const loadingfailed = 'loadingfailed'
const ward = 'ward'
const name = 'name'
const dob = 'dob'
const gender = 'gender'
const ctc = 'ctc'

export const DataEntryFormsDump = {
  name: 'DataEntryFormsDump',
  mixins: [zz.commonMixin],
  data() {
    return {
      uiState: 'initial',
      processedFormCount: 0,
      totalFormCount: '?',
      wards: [],
      selectedOrgUnitCode: '',
      periodStart: '',
      periodEnd: '',
      periods: [],
      endPeriods: [],
      clusterId: null,
    }
  },
  created() {
    this._fetchFormValues()
  },
  methods: {
    async _fetchFormValues() {
      this.uiState = loading
      try {
        await Promise.all([
          this._fetchWards(),
          this._generatePeriods(),
          this._fetchCluster(),
        ])
      } catch (err) {
        this.uiState = loadingfailed
        zz.consoleError(
          'Failed to load data for DataEntryFormsDump component',
          err,
        )
        return
      }
      this.uiState = 'initial'
    },
    async _fetchWards() {
      this.wards = await zz.fetchWards()
    },
    async _generatePeriods() {
      const allPeriods = 999
      const sautiYetuProgramId = await zz.getProgramId()
      const periods = await zz.generatePeriods(
        sautiYetuProgramId,
        new Date(),
        allPeriods,
      )
      periods.reverse() // let's put most recent first for good UX
      this.periods = periods
    },
    async _fetchCluster() {
      try {
        const cluster = await zz.fetchCluster()
        this.clusterId = cluster.id
      } catch (err) {
        throw zz.chainedError(err, 'Failed to get the current cluster')
      }
    },
    isFormValid() {
      return this.selectedOrgUnitCode && this.periodStart && this.periodEnd
    },
    onWardSelected() {
      if (this.periodStart) {
        return
      }
      this.periodStart = this.periods[0]
      this.onPeriodStartSelected()
    },
    onPeriodStartSelected() {
      this.endPeriods = this.periods.filter((e) => e >= this.periodStart)
      if (!this.periodEnd) {
        this.periodEnd = this.periodStart
        return
      }
      if (this.endPeriods.indexOf(this.periodEnd) >= 0) {
        return
      }
      this.periodEnd = ''
    },
    isLoadedSuccessfully() {
      return [loading, loadingfailed].indexOf(this.uiState) < 0
    },
    async doDownload() {
      this.uiState = 'preparingForm'
      this.processedFormCount = 0
      const orgUnit = this.selectedOrgUnitCode
      const startDate = this.periodStart
      const endDate = this.periodEnd
      try {
        const programId = await zz.getProgramId()
        const programName = await zz.getProgramName()
        const today = new Date().toLocaleDateString('sw')
        const orgUnitName = getOrgUnitName(
          this.selectedOrgUnitCode,
          this.clusterId,
          this.$i18n,
          this.wards,
        )
        const fragment = this.$i18n.t('dump_all_data_filename_fragment')
        const reportingPeriod = `${startDate} - ${endDate}`
        const filename = zz.replaceWeirdCharacters(
          `nacopha_${fragment}_${programName}_${reportingPeriod}_${orgUnitName}_${today}.xlsx`,
        )
        let workbook
        try {
          const templateBytes = await zz.getTemplateBytes(
            zz.buildDumpAllDataFormTemplateKey(programId),
          )
          workbook = await XlsxPopulate.fromDataAsync(templateBytes)
        } catch (err) {
          throw zz.chainedError(
            err,
            'Failed to get the "dump all data" template',
          )
        }
        const theData = await getTheData(
          orgUnit,
          this.wards,
          startDate,
          endDate,
          programId,
        )
        const teiTable = theData.teiTable
        const rowData = theData.rowData
        const forms = await zz.fetchDataEntryForms()
        this.totalFormCount = Object.keys(forms).length
        for (const currForm of Object.keys(forms)) {
          try {
            const extraConfig = {
              [zz.keys.orgUnit]: orgUnit,
              [zz.markers.period]: reportingPeriod,
              teiTable,
            }
            await this._createSheet(
              workbook,
              currForm,
              forms[currForm],
              extraConfig,
              rowData,
            )
            this.processedFormCount += 1
          } catch (err) {
            throw zz.chainedError(err, 'Failed to populate a form sheet')
          }
        }
        // this doesn't seem to scroll the view, so be sure to save the template when scrolled to the top
        workbook
          .sheet(0)
          .active(true)
          .activeCell('A1') // make the first sheet active
        let completeWorkbook
        try {
          completeWorkbook = await workbook.outputAsync()
        } catch (err) {
          this.uiStatus = 'failed'
          zz.consoleError(
            'Failed to generate the completed data entry workbook',
            err,
          )
          return
        }
        zz.triggerDownload(completeWorkbook, filename)
      } catch (err) {
        this.uiState = 'failed'
        zz.consoleError(
          `Failed to generate all data download for orgUnit='${orgUnit}', period='${startDate}'`,
          err,
        )
        return
      }
      this.uiState = 'success'
    },
    async _createSheet(
      workbook,
      formId,
      formSections,
      extraConfig,
      allRowData,
    ) {
      const formIdInt = parseInt(formId) // Object.keys() makes all keys a string
      const formIndex = formIdInt - 1
      let sheet = workbook.sheet(formIndex)
      if (!sheet) {
        workbook.addSheet(`Form ${formIdInt}`, formIndex)
        sheet = workbook.sheet(formIndex)
      }
      const headerRow = formSections.reduce((accum, currFormSection) => {
        const headersForThisSection = zz.concatHeaders(
          currFormSection.dataElements,
        )
        return accum.concat(headersForThisSection)
      }, [])
      const rowData = allRowData.filter((r) => {
        for (const curr of r.dataValues) {
          const found = headerRow.find(
            (h) => h[zz.headerIdField] === curr.dataElement,
          )
          if (found) {
            return true
          }
        }
        return false
      })
      headerRow.splice(0, 0, zz.buildColHeader(ward, ward))
      let headerIndex = 2
      headerRow.splice(headerIndex++, 0, zz.buildColHeader(name, name))
      const teiMappingMode = await zz.determineTeiMappingMode(formId)
      if (teiMappingMode !== zz.teiMappingModes.withoutCtcOnly) {
        headerRow.splice(headerIndex++, 0, zz.buildColHeader(ctc, ctc))
      }
      headerRow.splice(headerIndex++, 0, zz.buildColHeader(dob, dob))
      headerRow.splice(headerIndex++, 0, zz.buildColHeader(gender, gender))
      const headerRowEndColLetter = sheet.column(headerRow.length).columnName()
      const hiddenConfigRowNum = zz.findMarkerRow(
        sheet,
        zz.markers.hiddenConfig,
      )
      const hiddenConfigRowStyles = sheet
        .cell('A' + hiddenConfigRowNum)
        .style(['fill']) // FIXME doesn't seem to be able to get the fill colour
      const hiddenConfigRangeAddress = `A${hiddenConfigRowNum}:${headerRowEndColLetter}${hiddenConfigRowNum}`
      const serialisedHeaders = headerRow.map((e) => JSON.stringify(e))
      zz.writeHiddenValueRange(sheet, hiddenConfigRangeAddress, [
        serialisedHeaders,
      ])
      sheet.range(hiddenConfigRangeAddress).style(hiddenConfigRowStyles)
      const dataRowStart = zz.findMarkerRow(sheet, zz.markers.dataStart)
      const dataRowEnd = dataRowStart + rowData.length - 1
      try {
        await setOrgUnitHeader(sheet, extraConfig)
      } catch (err) {
        throw zz.chainedError(err, 'Failed to set orgUnit header')
      }
      const isNoData = dataRowStart > dataRowEnd
      if (isNoData) {
        sheet
          .range(`A${dataRowStart}:AZ${dataRowStart}`)
          .style({
            horizontalAlignment: 'left',
            fill: 'FFFFFF',
          })
          .clear()
        sheet.cell(`A${dataRowEnd + 1}`).value(`(${this.$i18n.t('no_data')})`)
        return
      }
      const initialColumnDefs = {
        ward: {valueType: ward},
        name: {valueType: name},
        dob: {valueType: dob},
        ctc: {valueType: ctc},
        gender: {valueType: gender},
      }
      const columnDefs = formSections.reduce((accum, currFormSection) => {
        currFormSection.dataElements.forEach((currDataElement) => {
          const colHeader = zz.buildColHeaderFromDataElement(currDataElement)
          accum[colHeader[zz.headerIdField]] = currDataElement
        })
        return accum
      }, initialColumnDefs)
      const self = this
      const specialColumnHandlers = await getSpecialColumnHandlers(
        extraConfig.teiTable,
        self,
      )
      const firstRowHeight = sheet.row(dataRowStart).height()
      for (let rowNum = dataRowStart + 1; rowNum <= dataRowEnd; rowNum++) {
        sheet.row(rowNum).height(firstRowHeight)
      }
      async function processCol(i) {
        const currHeader = headerRow[i]
        const currColLetter =
          i < 26
            ? String.fromCharCode(zz.asciiCapitalA + i)
            : 'A' + String.fromCharCode(zz.asciiCapitalA + i - 26)
        const colId = currHeader[zz.headerIdField]
        const colDef = columnDefs[colId]
        const colType = colDef.valueType
        const hasOptions = colDef.optionSetValue
        let strategy = specialColumnHandlers[colType]
        if (!strategy) {
          strategy = self.mixinGetColTypeStrategy(colType, hasOptions)
          strategy.extractFromEvent = buildExtractorFromDataValuesById(colId)
        }
        if (!strategy) {
          throw new Error(
            `Programmer problem: no strategy defined for column='${
              currHeader[zz.headerTitleField]
            }' with type='${colType}'` +
              `, hasOptions='${hasOptions}' on form='${formId}'`,
          )
        }
        if (
          !strategy.valueToDisplay ||
          typeof strategy.valueToDisplay !== 'function'
        ) {
          throw new Error(
            `Programmer problem: there is no 'valueToDisplay()' for '${colType}' columns`,
          )
        }
        sheet
          .range(
            `${currColLetter}${dataRowStart}:${currColLetter}${dataRowEnd}`,
          )
          .clear()
        const firstCellStyle = sheet
          .cell(`${currColLetter}${dataRowStart}`)
          .style([
            'fontSize',
            'fontFamily',
            'fontColor',
            'fill',
            'border',
            'numberFormat',
            'horizontalAlignment',
          ])
        sheet
          .range(
            `${currColLetter}${dataRowStart}:${currColLetter}${dataRowEnd}`,
          )
          .style(firstCellStyle)
        let rowNum = dataRowStart
        for (const curr of rowData) {
          const rawValue = strategy.extractFromEvent(curr)
          if (!rawValue) {
            rowNum++
            continue
          }
          const value = await strategy.valueToDisplay(colDef, rawValue)
          sheet.cell(currColLetter + rowNum).value(value)
          rowNum++
        }
      }
      const tasks = []
      for (let i = 0; i < headerRow.length; i++) {
        tasks.push(processCol(i))
      }
      await Promise.all(tasks)
    },
  },
}

async function getTheData(
  orgUnit,
  wards,
  periodStartMonth,
  periodEndMonth,
  programId,
) {
  // TODO if you ever refactor this, consider throwing this stuff away and
  // using the same approach (the analytics endpoint) that we use for
  // ZonalDataDump. There's less work to resolve values and build the table.
  try {
    const endDate = moment(periodEndMonth)
      .add(1, 'months')
      .subtract(1, 'seconds')
      .format('YYYY-MM-DD')
    const eventsResp = await axios.get(`${zz.apiBaseUrl}/events`, {
      params: {
        orgUnit,
        ouMode: 'DESCENDANTS',
        startDate: periodStartMonth,
        endDate,
        program: programId,
        paging: false,
      },
    })
    const rowData = eventsResp.data.events
    const orgUnitNameLookup = wards.reduce((accum, currWard) => {
      accum.push({id: currWard.id, displayName: currWard.displayName})
      for (const currEG of currWard.children) {
        accum.push({id: currEG.id, displayName: currWard.displayName}) // yep, we want the ward name
      }
      return accum
    }, [])
    const firstCutTeiTable = await zz.fetchTeiTable(orgUnit, orgUnitNameLookup)
    const knownTeiIds = firstCutTeiTable.map((e) => e.id)
    // now we need to go find details for those TEIs that have moved out of the orgUnit sub-hierarchy
    const movedTeis = rowData
      .map((e) => e.trackedEntityInstance)
      .filter((e) => knownTeiIds.indexOf(e) < 0)
    if (!movedTeis.length) {
      return {
        teiTable: firstCutTeiTable,
        rowData,
      }
    }
    console.debug(`Found ${movedTeis.length} TEIs that have been moved`)
    const movedTeisTeiTable = await zz.generateTeiTableFromIds(movedTeis)
    const teiTable = firstCutTeiTable.concat(movedTeisTeiTable)
    return {rowData, teiTable}
  } catch (err) {
    throw zz.chainedError(err, 'Failed to get data')
  }
}

async function setOrgUnitHeader(sheet, extraConfig) {
  const orgUnitHierarchy = await zz.getOrgUnitHierarchy()
  zz.writeValueForMarker(zz.markers.region, orgUnitHierarchy.region, sheet)
  zz.writeValueForMarker(zz.markers.district, orgUnitHierarchy.district, sheet)
  zz.writeValueForMarker(zz.markers.cluster, orgUnitHierarchy.cluster, sheet)
  zz.writeValueForMarker(
    zz.markers.period,
    extraConfig[zz.markers.period],
    sheet,
  )
}

function buildColumnHandler(teiTableField, teiTable) {
  return {
    valueToDisplay: function(colDef, rawValue) {
      const record = teiTable.find((e) => e.id === rawValue)
      if (!record) {
        zz.consoleError(
          `${
            zz.errorCodes.CHNV01
          } Failed to find TEI record for id='${rawValue}' in ` +
            `table. Next step was to get field='${teiTableField}'`,
        )
        return rawValue
      }
      return record[teiTableField]
    },
    extractFromEvent: function(event) {
      return event.trackedEntityInstance
    },
  }
}

function getOrgUnitName(selectedOrgUnitId, clusterId, i18n, wards) {
  if (selectedOrgUnitId === clusterId) {
    return i18n.t('all_wards')
  }
  const ward = wards.find((e) => e.id === selectedOrgUnitId)
  if (!ward) {
    return 'ERROR no ward found'
  }
  return ward.displayName
}

async function getSpecialColumnHandlers(teiTable, self) {
  return {
    ward: buildColumnHandler('orgUnit', teiTable),
    name: buildColumnHandler('fullName', teiTable),
    dob: (function() {
      const handler = buildColumnHandler('dob', teiTable)
      return {
        extractFromEvent: handler.extractFromEvent,
        valueToDisplay: function(colDef, rawValue) {
          const dateStr = handler.valueToDisplay(colDef, rawValue)
          const dateStrategy = self.mixinGetColTypeStrategy(zz.dateType)
          return dateStrategy.valueToDisplay(null, dateStr)
        },
      }
    })(),
    ctc: buildColumnHandler('ctc', teiTable),
    gender: buildColumnHandler('gender', teiTable),
  }
}

function buildExtractorFromDataValuesById(id) {
  return function(event) {
    const dataValue = event.dataValues.find((e) => e.dataElement === id)
    return (dataValue || {}).value
  }
}
