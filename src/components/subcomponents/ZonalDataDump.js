/**
 * NOTE: this feature depends on the DHIS2 analytics to work.
 * This means you *must*:
 *   1. login to DHIS
 *   2. open 'Scheduler' from the top, right menu
 *   3. enable the 'Analytics Tables' job to run, probably with a frequency
 *      like every hour or every day
 *
 * If you don't do this, you won't get any data out of this feature.
 */

import axios from 'axios'
import * as zz from '@/components/common'

const loading = 'loading'
const loadingfailed = 'loadingfailed'
const csvFileKey = 'zonalDataDumpCsvFile'

export const ZonalDataDump = {
  name: 'ZonalDataDump',
  mixins: [zz.commonMixin],
  data() {
    return {
      uiState: 'initial',
      processedClusterCount: 0,
      allClusterIds: [],
      zonesAndClusters: [],
      selectedClusters: [],
      periodStart: '',
      periodEnd: '',
      periods: [],
      endPeriods: [],
    }
  },
  created() {
    this._fetchFormValues()
  },
  methods: {
    async _fetchFormValues() {
      this.uiState = loading
      try {
        await Promise.all([this._generatePeriods(), this._fetchClusters()])
      } catch (err) {
        this.uiState = loadingfailed
        zz.consoleError('Failed to load data for ZonalDataDump component', err)
        return
      }
      this.uiState = 'initial'
    },
    async _fetchClusters() {
      const clusters = await zz.fetchSuperuserAvailableClusters()
      this.allClusterIds = clusters.map((e) => e.id)
      const zones = clusters.reduce((accum, curr) => {
        const key = curr.zoneName
        let entry = accum[key]
        if (!entry) {
          accum[key] = {zone: key, clusters: []}
          entry = accum[key]
        }
        entry.clusters.push(curr)
        return accum
      }, {})
      this.zonesAndClusters = Object.keys(zones).map((k) => zones[k])
    },
    async _generatePeriods() {
      const allPeriods = 999
      const selectedProgramId = await zz.getProgramId()
      const periods = await zz.generatePeriods(
        selectedProgramId,
        new Date(),
        allPeriods,
      )
      periods.reverse() // let's put most recent first for good UX
      this.periods = periods
    },
    isFormValid() {
      return this.selectedClusters.length && this.periodStart && this.periodEnd
    },
    onPeriodStartSelected() {
      this.endPeriods = this.periods.filter((e) => e >= this.periodStart)
      if (!this.periodEnd) {
        this.periodEnd = this.periodStart
        return
      }
      if (this.endPeriods.indexOf(this.periodEnd) >= 0) {
        return
      }
      this.periodEnd = ''
    },
    isLoadedSuccessfully() {
      return [loading, loadingfailed].indexOf(this.uiState) < 0
    },
    async doDownload() {
      this.uiState = 'preparingForm'
      this.processedClusterCount = 0
      const startDate = prepareDateStrForQuery(this.periodStart)
      const endDate = prepareDateStrForQuery(this.periodEnd)
      try {
        localStorage.removeItem(csvFileKey)
        const queryBase = await buildQuery(startDate, endDate)
        let isFirst = true
        for (const currCluster of this.selectedClusters) {
          try {
            let totalPages = 0
            let currPage = 1
            let isMorePages = true
            while (isMorePages) {
              const pagingInfo = await executeQuery(
                queryBase,
                currPage,
                currCluster,
                isFirst,
                this.$i18n,
              )
              totalPages = pagingInfo.pageCount
              console.debug(
                `Processing page ${currPage} of ${totalPages} for cluster ${currCluster}`,
              )
              isFirst = false
              isMorePages = currPage < totalPages
              currPage++
            }
            this.processedClusterCount++
          } catch (err) {
            throw zz.chainedError(
              err,
              `Failed while processing cluster='${currCluster}'`,
            )
          }
        }
        const csvData = localStorage.getItem(csvFileKey)
        const programName = await zz.getProgramName()
        const today = new Date().toLocaleDateString('sw')
        const fragment = this.$i18n.t('zonal_dump_all_data_filename_fragment')
        const reportingPeriod = `${startDate} - ${endDate}`
        const filename = zz.replaceWeirdCharacters(
          `nacopha_${fragment}_${programName}_${reportingPeriod}_${today}.csv`,
        )
        console.debug(`Total size of CSV download=${csvData.length} bytes`)
        zz.triggerDownload(new Blob([csvData], {type: 'text/csv'}), filename)
        localStorage.removeItem(csvFileKey)
      } catch (err) {
        this.uiState = 'failed'
        zz.consoleError(
          `Failed to generate zonal data dump for clusters='${JSON.stringify(
            this.selectedClusters,
          )}', period='${startDate}'`,
          err,
        )
        return
      }
      this.uiState = 'success'
    },
    doSelectAll() {
      this.selectedClusters = [...this.allClusterIds] // clone
    },
    doUnselectAll() {
      this.selectedClusters = []
    },
    doSelectAllZone(zone) {
      zone.clusters.forEach((e) => {
        const theId = e.id
        const isAlreadySelected = this.selectedClusters.indexOf(theId) >= 0
        if (isAlreadySelected) {
          return
        }
        this.selectedClusters.push(theId)
      })
    },
    doUnselectAllZone(zone) {
      zone.clusters.forEach((e) => {
        const theId = e.id
        const foundIndex = this.selectedClusters.indexOf(theId)
        const isNotSelected = foundIndex < 0
        if (isNotSelected) {
          return
        }
        this.selectedClusters.splice(foundIndex, 1)
      })
    },
  },
}

async function executeQuery(
  queryBase,
  pageNum,
  clusterOrgUnit,
  isFirst,
  $i18n,
) {
  const query = `${queryBase}page=${pageNum}&dimension=ou:${clusterOrgUnit}`
  try {
    const resp = await axios.get(query)
    const zoneName = await zz.lookupZoneNameByClusterId(clusterOrgUnit)
    const clusterName = await zz.lookupClusterName(clusterOrgUnit)
    const rawHeaders = resp.data.headers
    const yesLabel = await zz.getYesConstantDisplayName()
    const noLabel = await zz.getNoConstantDisplayName()
    const nameResolver = new NameResolver(resp, {
      booleanTrueLabel: yesLabel,
      booleanFalseLabel: noLabel,
    })
    let csvFile = ''
    const ignoredCols = [
      'psi',
      'ps',
      'eventdate',
      'longitude',
      'latitude',
      'oucode',
      'ouname',
      'ou',
    ]
    if (isFirst) {
      let extraHeaderCols = $i18n.t('zone') + ','
      extraHeaderCols += $i18n.t('cluster') + ','
      extraHeaderCols += $i18n.t('ward') + ','
      extraHeaderCols += $i18n.t('group') + ','
      csvFile +=
        extraHeaderCols +
        rawHeaders
          .filter((e) => !(ignoredCols.indexOf(e.name) >= 0))
          .map((e) => nameResolver.resolve(e.name, null))
      csvFile += '\n'
    }
    const ouIdIndex = rawHeaders.findIndex((e) => e.name === 'ou')
    const ounameIndex = rawHeaders.findIndex((e) => e.name === 'ouname')
    for (const currRow of resp.data.rows) {
      let index = 0
      const wardOrEmpGroupId = currRow[ouIdIndex]
      const possibleWardName = await zz.lookupWardNameByEmpGroupId(
        wardOrEmpGroupId,
      )
      const ounameValue = currRow[ounameIndex]
      let wardName
      let empGroupName
      if (possibleWardName) {
        wardName = possibleWardName
        empGroupName = ounameValue
      } else {
        wardName = ounameValue
        empGroupName = ''
      }
      csvFile += currRow.reduce((accum, currField) => {
        const headerCode = rawHeaders[index]
        index++
        if (ignoredCols.indexOf(headerCode.name) >= 0) {
          return accum
        }
        accum += `,`
        const resolvedValue = nameResolver.resolve(currField, headerCode)
        const isContainsComma = /.*,.*/.test(resolvedValue)
        accum += isContainsComma ? `"${resolvedValue}"` : resolvedValue
        return accum
      }, `${zoneName},${clusterName},${wardName},${empGroupName}`)
      csvFile += '\n'
    }
    const existingCsvFile = localStorage.getItem(csvFileKey) || ''
    localStorage.setItem(csvFileKey, `${existingCsvFile}${csvFile}`)
    return resp.data.metaData.pager
  } catch (err) {
    throw zz.chainedError(err, `Failed to execute query=${query}`)
  }
}

function prepareDateStrForQuery(dateStr) {
  return dateStr.replace(/-/g, '')
}

async function buildQuery(startDateStr, endDateStr) {
  const programId = await zz.getProgramId()
  const programStage = await zz.getProgramStageId()
  const dataElements = await getDataElementIds()
  const fullNameAttrId = await zz.getFullNameAttributeId()
  const ctcAttrId = await zz.getCTCAttributeId()
  const genderAttrId = await zz.getGenderAttributeId()
  const dobAttrId = await zz.getDobAttributeId()
  let result = `${zz.apiBaseUrl}/analytics/events/query/${programId}.json?
    dimension=pe:${startDateStr};${endDateStr}&
    stage=${programStage}&
    displayProperty=NAME&
    outputType=EVENT&
    pageSize=100&
    dimension=${fullNameAttrId}&
    dimension=${ctcAttrId}&
    dimension=${dobAttrId}&
    dimension=${genderAttrId}&`
  for (const curr of dataElements) {
    result += `dimension=${curr}&`
  }
  return result.replace(/\s/g, '')
}

async function getDataElementIds() {
  return zz.fromCache('ZonalDataDump_getDataElementIds', async () => {
    try {
      const formConfig = await zz.fetchDataEntryForms()
      const result = Object.keys(formConfig)
        .sort()
        .reduce((accum, currKey) => {
          const currObj = formConfig[currKey]
          for (const item of currObj) {
            accum = accum.concat(item.dataElements.map((e) => e.id))
          }
          return accum
        }, [])
      return result
    } catch (err) {
      throw zz.chainedError(
        err,
        `Failed to get dataElement IDs for the current program`,
      )
    }
  })
}

function NameResolver(resp, {booleanTrueLabel, booleanFalseLabel}) {
  const itemMapping = resp.data.metaData.items
  const dimensionMapping = _createLookup(resp.data.metaData)
  const colTypeStrategies = {
    TEXT: (code, headerDef) => {
      const dimensionId = (headerDef || {}).name
      const dimension = dimensionMapping[dimensionId]
      let found
      if (dimension) {
        found = dimension.find((e) => e.code === code)
      } else {
        found = itemMapping[code]
      }
      if (!found) {
        return code
      }
      return found.name
    },
    BOOLEAN: (code, headerDef) => {
      if (code === '0') {
        return booleanFalseLabel
      }
      if (code === '1') {
        return booleanTrueLabel
      }
      return ''
    },
    HEADER: (code, headerDef) => {
      const found = itemMapping[code]
      return found ? found.name : code
    },
  }
  const defaultStrategy = (code, headerDef) => {
    return code
  }
  /**
   * Resolves an internal system code to the human readable label.
   * @param code         value of field to resolve
   * @param dimensionId  UID of column so we can look up the vocab, pass null
   *                     for the header itself
   */
  this.resolve = function resolve(code, headerDef) {
    const type = (headerDef || {valueType: 'HEADER'}).valueType
    const strategy = colTypeStrategies[type] || defaultStrategy
    return strategy(code, headerDef)
  }
}

function _createLookup(metaData) {
  const itemMapping = metaData.items
  const dimensionMapping = metaData.dimensions
  const result = Object.keys(dimensionMapping).reduce((accum, currKey) => {
    const resolvedValues = dimensionMapping[currKey].map(
      (e) => itemMapping[e] || e,
    )
    accum[currKey] = resolvedValues
    return accum
  }, {})
  return result
}

export const _testonly = {
  _createLookup,
  NameResolver,
}
