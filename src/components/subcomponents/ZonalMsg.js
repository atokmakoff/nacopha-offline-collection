import axios from 'axios'
import * as zz from '../common'

export const ZonalMsg = {
  name: 'ZonalMsg',
  data () {
    return {
      msgSubject: `${this.$i18n.t('ctc-coll-subject')}, CTC=${this.ctc}`,
      msg: this.$i18n.t('ctc-coll-prepop-msg'),
      uiState: 'initial'
    }
  },
  props: [
    'ctc'
  ],
  methods: {
    async sendZonalUserMsg () {
      this.uiState = 'sending'
      let zoneOrgUnits
      try {
        const resp = await axios.get(`${zz.apiBaseUrl}/me`, {
          params: { fields: 'organisationUnits[id,path]' }
        })
        const zoneObj = resp.data.organisationUnits.reduce((accum, curr) => {
          const justTheFirstIdRegex = /^\/([a-zA-Z]+)\/.*/
          const zone = curr.path.replace(justTheFirstIdRegex, '$1')
          if (!zone) {
            throw new Error(`Could not extract zone from path='${curr.path}'`)
          }
          accum[zone] = true
          return accum
        }, {})
        zoneOrgUnits = Object.keys(zoneObj)
      } catch (err) {
        this.uiState = 'failed'
        zz.consoleError(`Failed while getting zone orgUnit for notification about colliding CTC`, err)
        return
      }
      try {
        console.debug(`Sending message to zone orgUnits=${JSON.stringify(zoneOrgUnits)}`)
        const resp = await axios.post(`${zz.apiBaseUrl}/messageConversations`, {
          subject: this.msgSubject,
          text: this.msg,
          organisationUnits: zoneOrgUnits.map(e => {
            return { id: e }
          })
        })
        const convoUrl = resp.headers.location
        console.log(`message conversation URL=${convoUrl}`)
      } catch (err) {
        this.uiState = 'failed'
        zz.consoleError(`Failed while sending msg='${this.msg}' to zonal orgUnit='${zoneOrgUnits}' about colliding CTC`, err)
        return
      }
      this.uiState = 'success'
    }
  }
}
