import * as axios from 'axios'
import * as zz from '../common'
import InputUploadForm from '@/components/subcomponents/InputUploadFormComponent'
const moment = require('moment')

export const DataEntryUploadForms = {
  name: 'DataEntryUploadForms',
  mixins: [zz.commonMixin],
  components: {
    'input-upload-form': InputUploadForm
  },
  data () {
    return {
      uploadSummary: {},
      isShowSummary: false
    }
  },
  methods: {
    async workbookProcessorFn (workbook, validationFailures) {
      zz.resetCache()
      this.uploadSummary = {}
      let formId = 1
      let reportingPeriod
      let wrappedEvents = []
      let orgUnit = null
      // TODO write programId to downloaded workbook and assert it on upload
      for (const currSheet of workbook.sheets()) {
        try {
          const metadata = await this.getMetadata(currSheet, validationFailures)
          if (validationFailures.length) {
            return
          }
          if (!orgUnit) {
            orgUnit = metadata.embeddedConfig[zz.markers.orgUnit]
          }
          if (!reportingPeriod) {
            const programId = await zz.getProgramId()
            reportingPeriod = metadata.embeddedConfig[zz.markers.period] || zz.die('No reporting period found')
            const openReportingPeriods = await zz.generatePeriods(programId)
            if (!openReportingPeriods.find(e => e === reportingPeriod)) {
              const msg = this.$i18n.t('cant_accept_period') + `='${reportingPeriod}'. ` +
                this.$i18n.t('open_periods_are') + ': ' + openReportingPeriods.join(', ')
              validationFailures.push(msg)
              zz.consoleLog(msg)
              return
            }
          }
          const currSheetWrappedEvents = await this.processData(currSheet, metadata, formId, validationFailures)
          wrappedEvents = wrappedEvents.concat(currSheetWrappedEvents)
          formId++
        } catch (err) {
          const msgPart1 = this.$i18n.t('de-failed-upload-1')
          const msgPart2 = this.$i18n.t('de-failed-upload-2')
          validationFailures.push(`${msgPart1}'${currSheet.name()}'${msgPart2}`)
          zz.consoleError(`Failed while processing sheet '${currSheet.name()}'`, err)
          return
        }
      }
      if (!wrappedEvents.length) {
        validationFailures.push(this.$i18n.t('de_no_data'))
        return
      }
      if (validationFailures.length) {
        console.warn('Found validation failures, refusing to send data to server')
        return
      }
      // FIXME commented because using ?dryRun causes a 500, not sure why yet
      // try {
      //   const events = wrappedEvents.map(e => e.event)
      //   const postEventsDryRunResp = await axios.post(`${zz.apiBaseUrl}/events`, { events }, {
      //     params: {
      //       dryRun: true
      //     }
      //   })
      //   // FIXME do we need to assert success for dry run? Or is 200 enough?
      //   console.log(`Dry run successful for ${postEventsDryRunResp.data.importSummaries.imported} dataElemets`)
      // } catch (err) {
      //   throw zz.chainedError(err, `Failed to validate data on server by doing a dry run`)
      // }
      try {
        const existingEventIds = await fetchExistingEventIds(orgUnit, reportingPeriod)
        const promises = []
        const programId = await zz.getProgramId()
        for (const currWrapped of wrappedEvents) {
          const curr = currWrapped.event
          const formDateOfServiceId = await zz.getFormDosId(programId, currWrapped.form)
          const key = eventLookupKey(curr.trackedEntityInstance, formDateOfServiceId, curr.eventDate)
          const wrapInErrorEnricher = async function (promiseThingy) {
            try {
              await promiseThingy
            } catch (err) {
              err.row = currWrapped.row
              err.form = currWrapped.form
              throw err
            }
          }
          const existingEventId = existingEventIds[key]
          if (existingEventId) {
            this.addUpdatedRecordToSummary(currWrapped.form)
            promises.push(wrapInErrorEnricher(axios.delete(`${zz.apiBaseUrl}/events/${existingEventId}`)))
          } else {
            this.addNewRecordToSummary(currWrapped.form)
          }
          promises.push(wrapInErrorEnricher(axios.post(`${zz.apiBaseUrl}/events`, curr)))
        }
        await Promise.all(promises)
      } catch (err) {
        const addressMsg = `${this.$i18n.t('de_form')} ${err.form}, ${this.$i18n.t('row')} ${err.row}`
        validationFailures.push(addressMsg + '. ' + this.$i18n.t('tech_issues'))
        zz.consoleError(`Failed to POST data entry form data to server`, err)
        return // eslint-disable-line no-useless-return, remove this comment if we add more code below
      }
    },
    addUpdatedRecordToSummary (formId) {
      let form = this.uploadSummary[formId]
      if (!form) {
        this.uploadSummary[formId] = { updated: 0, new: 0 }
        form = this.uploadSummary[formId]
      }
      form.updated += 1
    },
    addNewRecordToSummary (formId) {
      let form = this.uploadSummary[formId]
      if (!form) {
        this.uploadSummary[formId] = { updated: 0, new: 0 }
        form = this.uploadSummary[formId]
      }
      form.new += 1
    },
    onDisplaySummary (isShow) {
      this.isShowSummary = isShow
    },
    async getMetadata (sheet, validationFailures) {
      const headerRowNum = zz.findMarkerRow(sheet, zz.markers.hiddenConfig)
      const headerRow = sheet.row(headerRowNum)
      const headers = []
      let isMoreHeaders = true
      let i = 1
      let embeddedConfig
      while (isMoreHeaders) {
        const value = headerRow.cell(i).value()
        isMoreHeaders = !!value
        if (!isMoreHeaders) {
          break
        }
        i++
        const headerDef = JSON.parse(value)
        const isConfig = headerDef[zz.headerIdField] === zz.markers.hiddenConfig
        if (isConfig) {
          embeddedConfig = headerDef[zz.headerTitleField]
          const uploadedVersion = embeddedConfig[zz.keys.templateVersion]
          if (uploadedVersion === zz.empGroupTemplateVersion) {
            validationFailures.push(this.$i18n.t('eg-form-instead-of-de'))
            return
          }
          if (zz.dataEntryTemplateVersion !== uploadedVersion) {
            zz.consoleLog(`User uploaded template version='${uploadedVersion}' where ` +
              `we expected version='${zz.empGroupTemplateVersion}'`)
            validationFailures.push(this.$i18n.t('old-form-version'))
            return
          }
          break
        }
        headers.push(headerDef)
      }
      const enrichedHeaders = await enrichHeaders(headers)
      return {
        headers: enrichedHeaders,
        embeddedConfig
      }
    },
    async processData (sheet, metadata, formId, validationFailures) {
      console.log(`Processing ${sheet.name()}`)
      const checkValueCellAddress = metadata.embeddedConfig[zz.keys.checkValueCellAddress]
      const checkValue = sheet.cell(checkValueCellAddress).value()
      console.debug(`${sheet.name()} has check value='${checkValue}' in cell='${checkValueCellAddress}'`)
      if (checkValue < 0) {
        const msgPart1 = this.$i18n.t('de-failed-upload-1')
        const msgPart2 = this.$i18n.t('de-form-check-failed')
        validationFailures.push(`${msgPart1}'${sheet.name()}' ${msgPart2}`)
        return
      }
      const dataStartRow = zz.findMarkerRow(sheet, zz.markers.dataStart)
      const dataEndRow = zz.findMarkerRow(sheet, zz.markers.dataEnd)
      const wrappedEvents = []
      for (let currRow = dataStartRow; currRow <= dataEndRow; currRow++) {
        console.debug(`Processing row ${currRow} on form ${sheet.name()}`)
        const isData = sheet.cell(`A${currRow}`).value() || sheet.cell(`B${currRow}`).value()
        if (!isData) {
          console.debug(`No data found in first two cells of row ${currRow} on form ${sheet.name()}, not processing this row.`)
          continue
        }
        // TODO go parallel once we have things working
        try {
          const event = await this._processRow(sheet, metadata, currRow, formId, validationFailures)
          const isDuplicateEvent = wrappedEvents.filter(e => {
            const isTeiMatching = e.event.trackedEntityInstance === event.trackedEntityInstance
            const isEventDateMatching = e.event.eventDate === event.eventDate
            return isTeiMatching && isEventDateMatching
          }).length
          if (isDuplicateEvent) {
            const reason = this.$i18n.t('de-duplicate-event')
            validationFailures.push(this._buildRowAddressedValidationFailMsg(sheet.name(), currRow, reason))
            continue
          }
          wrappedEvents.push({
            form: formId,
            event,
            row: currRow
          })
        } catch (err) {
          const reason = this.$i18n.t('de-failed-upload-2')
          validationFailures.push(this._buildRowAddressedValidationFailMsg(sheet.name(), currRow, reason))
          zz.consoleError(`Failed while processing data entry sheet='${sheet.name()}', row='${currRow}'`, err)
        }
      }
      return wrappedEvents
    },
    _buildRowAddressedValidationFailMsg (sheetName, rowNum, reason) {
      const msgPart1 = this.$i18n.t('de-failed-upload-1')
      const msgPart2 = this.$i18n.t('on-row')
      const sep = reason ? ': ' : ''
      return `${msgPart1}'${sheetName}' ${msgPart2}${rowNum}${sep}${reason}`
    },
    async _processRow (sheet, metadata, rowNum, formId, validationFailures) {
      const sheetName = sheet.name()
      const addressFragment = `${this.$i18n.t('de_form')} ${sheetName}, ${this.$i18n.t('row')} ${rowNum}`
      const dataEndsInColBeforeMarker = 1
      const dataEndCol = zz.findMarkerColNumber(sheet, zz.markers.dataEnd) - dataEndsInColBeforeMarker
      const teiColumnIndex = findColumnIndex(zz.fullNameColHeader, metadata.headers)
      const fullNameRawValue = sheet.column(teiColumnIndex).cell(rowNum).value()
      if (!fullNameRawValue) {
        validationFailures.push(addressFragment + this.$i18n.t('no_name_selected'))
        return
      }
      const programId = await zz.getProgramId()
      const dateOfServiceColHeader = await zz.getFormDosId(programId, formId)
      const eventDateColumnIndex = findColumnIndex(dateOfServiceColHeader, metadata.headers)
      const dateOfServiceRawDayValue = sheet.column(eventDateColumnIndex).cell(rowNum).value()
      if (!dateOfServiceRawDayValue && dateOfServiceRawDayValue !== 0) {
        validationFailures.push(addressFragment + this.$i18n.t('no_dos'))
        return
      }
      const isoCompliantDayValue = ('0' + dateOfServiceRawDayValue).substr(-2)
      const period = metadata.embeddedConfig[zz.markers.period]
      const dateOfService = `${period}-${isoCompliantDayValue}`
      const isDayOfServiceValid = (function (dayValue) {
        const parsed = parseInt(dayValue)
        if (isNaN(parsed)) {
          return false
        }
        if (parsed < 1 || parsed > 31) {
          return false
        }
        return true
      }(dateOfServiceRawDayValue))
      if (!isDayOfServiceValid || isNotInReportingPeriod(period, dateOfService)) {
        const maxMonthDay = moment(period).add(1, 'months').subtract(1, 'seconds').date()
        validationFailures.push(`${addressFragment}, ${this.$i18n.t('dos')}='${dateOfServiceRawDayValue}' (${dateOfService}) ` +
          `${this.$i18n.t('for')} '${fullNameRawValue}' ${this.$i18n.t('not_valid_date_format_or_outside_period')} ${maxMonthDay}.`)
        return
      }
      let teiId
      try {
        const embeddedTeiMapping = metadata.embeddedConfig[zz.keys.fullNameLookup]
        teiId = await this.mixinGetColTypeStrategy(zz.fullNameType).displayToValue(fullNameRawValue, embeddedTeiMapping,
          metadata.embeddedConfig[zz.keys.orgUnit], formId)
      } catch (err) {
        zz.consoleError(`Failed to resolve raw name='${fullNameRawValue}' to a TEI ID`, err)
        const defaultMsg = this.$i18n.t('de_full_name_fail_default')
        const msg = err.nacophaMsg || defaultMsg
        validationFailures.push(`${addressFragment} ${msg}`)
        return
      }
      const wardOrgUnit = metadata.embeddedConfig[zz.keys.orgUnit]
      const enrollmentObj = await getEnrollmentFor(teiId, wardOrgUnit)
      const enrollment = enrollmentObj.enrollment
      const orgUnit = enrollmentObj.orgUnit
      const programStageId = await zz.getProgramStageId()
      const event = {
        trackedEntityInstance: teiId,
        program: programId,
        programStage: programStageId,
        orgUnit,
        enrollment,
        status: 'COMPLETED',
        dataValues: [],
        eventDate: dateOfService
      }
      const specialHandlers = {
        [dateOfServiceColHeader]: function () {
          event.dataValues.push({
            dataElement: dateOfServiceColHeader,
            value: event.eventDate
          })
        },
        [zz.fullNameColHeader]: function () { /* do nothing */ }
      }
      for (let i = 1; i <= dataEndCol; i++) {
        const currHeader = metadata.headers[i - 1]
        const colId = currHeader[zz.headerIdField]
        const currCol = sheet.column(i)
        const rawValue = currCol.cell(rowNum).value()
        if (!rawValue) {
          continue
        }
        const specialHandler = specialHandlers[colId]
        if (specialHandler) {
          specialHandler()
          continue
        }
        const colType = currHeader.valueType
        const hasOptions = currHeader.hasOptions
        const strategy = this.mixinGetColTypeStrategy(colType, hasOptions)
        if (!strategy) {
          throw new Error(`Programmer problem: unsupported column type='${colType}', hasOptions='${hasOptions}' for ` +
            `column='${currHeader[zz.headerTitleField]}' on form='${sheet.name()}}'`)
        }
        try {
          const value = await strategy.displayToValue(rawValue, currHeader.options, metadata.embeddedConfig[zz.keys.orgUnit], formId)
          event.dataValues.push({
            dataElement: colId,
            value: value
          })
        } catch (err) {
          const colLetter = currCol.columnName()
          zz.consoleError(`${addressFragment}, col='${colLetter}': Failed to convert display value='${rawValue}' to raw.`, err)
          validationFailures.push(`${addressFragment}, ${this.$i18n.t('eg-at-col')} '${colLetter}': ${this.$i18n.t('generic_invalid_value')}`)
        }
      }
      return event
    }
  }
}

async function fetchExistingEventIds (orgUnit, reportingPeriod) {
  const endDate = moment(reportingPeriod).add(1, 'months').format('YYYY-MM')
  const program = await zz.getProgramId()
  try {
    const resp = await axios.get(`${zz.apiBaseUrl}/events`, {
      params: {
        orgUnit,
        program,
        fields: 'event,trackedEntityInstance,eventDate,dataValues[dataElement]',
        skipPaging: true,
        ouMode: 'DESCENDANTS',
        startDate: reportingPeriod,
        endDate
      }
    })
    const formDateOfServiceIds = await fetchAllDateOfServiceIds(program)
    return resp.data.events.reduce((accum, curr) => {
      const formDateOfServiceId = curr.dataValues.find(e => e && formDateOfServiceIds.indexOf(e.dataElement) >= 0)
      if (!formDateOfServiceId) {
        // all events *should* be for one of our forms, but we'll ignore it for now rather than explode
        return accum
      }
      const key = eventLookupKey(curr.trackedEntityInstance, formDateOfServiceId.dataElement, curr.eventDate)
      accum[key] = curr.event
      return accum
    }, {})
  } catch (err) {
    throw zz.chainedError(err, `Failed to get events for orgUnit='${orgUnit}', reportingPeriod='${reportingPeriod}'`)
  }
}

async function fetchAllDateOfServiceIds (programId) {
  try {
    const maxFormCount = await zz.getMaxFormCount()
    const promises = []
    for (let i = 1; i <= maxFormCount; i++) {
      promises.push(zz.getFormDosId(programId, i))
    }
    return Promise.all(promises)
  } catch (err) {
    throw zz.chainedError(err, 'Failed to fetch all date of service IDs')
  }
}

function eventLookupKey (tei, dateOfServiceId, eventDate) {
  function assertString (fieldName, val) {
    typeof (val) === 'string' || zz.die(`Programmer error: the "${fieldName}" param must be a string`)
  }
  assertString('tei', tei)
  assertString('dateOfServiceId', dateOfServiceId)
  assertString('eventDate', eventDate)
  const trimmedEventDate = eventDate.substr(0, 10) // server supplies time, local is just date, so unify
  return `${tei}|${dateOfServiceId}|${trimmedEventDate}`
}

async function enrichHeaders (headers) {
  let dataElementLookup
  const idList = headers.map(e => e.id).filter(e => e !== zz.fullNameColHeader).join(',')
  try {
    const resp = await axios.get(`${zz.apiBaseUrl}/dataElements`, {
      params: {
        paging: false,
        filter: `id:in:[${idList}]`,
        fields: `
          id,
          formName,
          displayName,
          valueType,
          optionSetValue,
          optionSet[
            id,
            options[
              code,
              displayName
            ]
          ]`.replace(/\s/g, '')
      }
    })
    dataElementLookup = resp.data.dataElements.reduce((accum, curr) => {
      accum[curr.id] = curr
      return accum
    }, {})
  } catch (err) {
    throw zz.chainedError(err, `Failed while trying to get a list of dataElements to map values to IDs`)
  }
  const result = headers.map(e => {
    if (zz.fullNameColHeader === e[zz.headerIdField]) {
      return e
    }
    const moreInfo = dataElementLookup[e[zz.headerIdField]]
    e.formName = moreInfo.formName
    e.valueType = moreInfo.valueType
    e.hasOptions = moreInfo.optionSetValue
    if (moreInfo.optionSet) {
      e.options = moreInfo.optionSet.options.reduce((accum, curr) => {
        accum[curr.displayName] = curr.code
        return accum
      }, {})
    }
    return e
  })
  return result
}

function isNotInReportingPeriod (reportingPeriod, dateOfService) {
  const mDate = moment(dateOfService)
  if (!mDate.isValid()) {
    return true
  }
  return mDate.isBefore(reportingPeriod, 'month') || mDate.isAfter(reportingPeriod, 'month')
}

function findColumnIndex (colName, headers) {
  const indexInArray = headers.findIndex(e => colName === e.id)
  if (indexInArray < 0) {
    throw new Error(`Failed to find header '${colName}' in headers '${JSON.stringify(headers)}'`)
  }
  const columnIndexesAre1Based = 1
  return indexInArray + columnIndexesAre1Based
}

async function getEnrollmentFor (teiId, wardOrgUnit) { // we ask for wardOrgUnit to help filter down options
  try {
    const programId = await zz.getProgramId()
    const resp = await axios.get(`${zz.apiBaseUrl}/enrollments`, {
      params: {
        fields: 'enrollment,orgUnit',
        ou: wardOrgUnit,
        ouMode: 'DESCENDANTS',
        program: programId,
        programStatus: 'ACTIVE', // status of the *enrollment* in the given program
        skipPaging: true,
        trackedEntityInstance: teiId
      }
    })
    const enrollments = resp.data.enrollments
    if (!enrollments || enrollments.length === 0) {
      // TODO return message to user to "move TEI to same group" to fix problem
      throw new Error(`Response was successful but the body contained no enrollments: ${JSON.stringify(enrollments)}`)
    }
    if (enrollments.length > 1) {
      // TODO return message to user to "move TEI to same group" to fix problem
      throw new Error(`Found ${enrollments.length} enrollments when looking for tei='${teiId}', ` +
        `expected only 1. Found=${JSON.stringify(enrollments)}`)
    }
    return enrollments[0]
  } catch (err) {
    throw zz.chainedError(err, `Failed while getting enrollments for tei='${teiId}'`)
  }
}

export const _testonly = {
  isNotInReportingPeriod
}
