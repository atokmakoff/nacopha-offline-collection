import axios from 'axios'
import * as zz from '../common'

export const EmpGroupRename = {
  name: 'EmpGroupRename',
  data () {
    return {
      selectedGroup: null,
      newGroupName: null,
      uiState: 'initial',
      groups: []
    }
  },
  created () {
    this._fetchFormValues()
  },
  methods: {
    async _fetchFormValues () {
      try {
        this.uiState = 'fetching'
        const wardsAndGroups = await zz.fetchWardsAndGroups()
        this.groups = wardsAndGroups.filter(e => !e.isWard)
        this.uiState = 'fetched'
      } catch (err) {
        this.uiState = 'fetchingfailed'
        zz.consoleError('Failed to load group names', err)
      }
    },
    isDisplayOptions () {
      return !this.uiState.startsWith('fetching')
    },
    isValid () {
      return this.selectedGroup && this.newGroupName && this.newGroupName.trim()
    },
    async reload () {
      const stateBefore = this.uiState
      await this._fetchFormValues()
      this.uiState = stateBefore
      this.selectedGroup = null
    },
    async doRename () {
      this.uiState = 'renaming'
      const url = `${zz.apiBaseUrl}/organisationUnits/${this.selectedGroup}`
      let newRecord
      try {
        const resp = await axios.get(url, {
          params: { fields: '*' }
        })
        const existingRecord = resp.data
        existingRecord.name = this.newGroupName
        existingRecord.shortName = this.newGroupName
        newRecord = existingRecord
      } catch (err) {
        zz.consoleError(`Failed to get existing orgUnit record for '${this.selectedGroup}'`, err)
        this.uiState = 'failed'
        return
      }
      try {
        await axios.put(url, newRecord)
        zz.resetCache()
      } catch (err) {
        zz.consoleError(`Failed to update orgUnit record for '${this.selectedGroup}'`, err)
        this.uiState = 'failed'
        return
      }
      this.uiState = 'success'
      this.$emit(zz.orgUnitsChangedEvent)
    }
  }
}
