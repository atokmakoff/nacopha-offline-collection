import XlsxPopulate from 'xlsx-populate/lib/XlsxPopulate'
import axios from 'axios'
import * as zz from '@/components/common'
import InputUploadForm from '@/components/subcomponents/InputUploadFormComponent'
import EmpGroupSearch from '@/components/subcomponents/EmpGroupSearchComponent'
import EmpGroupRename from '@/components/subcomponents/EmpGroupRenameComponent'
import EmpGroupDelete from '@/components/subcomponents/EmpGroupDeleteComponent'

const ctcAttributeId = 'FvpuJ1Ks9nL' // FIXME make server-side config
const asciiCapitalA = 65
export const teiIdType = 'teiId'
export const empGroupEnrolDateType = 'empGroupEnrollmentDate'
const addressesKey = 'addresses'
const endOfDataText = 'End of data entry' // TODO translate me
export const _mockable = {
  fetchActivityGroupVocab: zz.fetchActivityGroupVocab,
  fetchGroupActivityAttributeIds: zz.fetchGroupActivityAttributeIds,
  fetchAttributeOptions: zz.fetchAttributeOptions
}

export const EmpGroupMgmt = {
  name: 'EmpGroupMgmt',
  mixins: [zz.commonMixin],
  data () {
    return {
      isLoading: true,
      createEmpGroupStatus: 'initial',
      downloadEmpGroupFormStatus: 'initial',
      isError: false,
      empGroupName: '',
      selectedWard: '',
      wards: [],
      selectedEmpGroup: '',
      empGroups: [],
      recordsProcessedCount: 0,
      isShowSummary: false
    }
  },
  created () {
    this.fetchFormValues()
  },
  components: {
    'upload-form': InputUploadForm,
    'emp-group-search': EmpGroupSearch,
    'emp-group-rename': EmpGroupRename,
    'emp-group-delete': EmpGroupDelete
  },
  methods: {
    async fetchFormValues () {
      this.isError = false
      this.isLoading = true
      try {
        await this._fetchWards()
      } catch (err) {
        this.handleError('Failed to fetch wards', err)
      } finally {
        this.isLoading = false
      }
    },
    async _fetchWards () {
      this.wards = await zz.fetchWards()
    },
    isFormValid () {
      return this.empGroupName && this.selectedWard
    },
    async refreshOrgUnits () {
      this.$refs.groupRename.reload()
      this.$refs.groupDelete.reload()
      this.$refs.groupSearch.reload()
      await this._fetchWards()
      if (this.selectedWard) {
        const beforeSelectedGroup = this.selectedEmpGroup
        this.onWardSelected()
        this.selectedEmpGroup = beforeSelectedGroup
      }
    },
    async workbookProcessorFn (workbook, validationFailures) {
      zz.resetCache()
      this.isError = false
      this.recordsProcessedCount = 0
      try {
        const sheet = workbook.sheet(0)
        const records = await this.processUploadedSheet(sheet, validationFailures)
        if (validationFailures.length) {
          return
        }
        console.log(`prepared ${records.length} records`)
        const promises = []
        for (const curr of records) {
          const currRow = curr.row
          delete curr.row // can't send this to the server
          promises.push((async function errorEnricher () {
            try {
              await axios.put(`${zz.apiBaseUrl}/trackedEntityInstances/${curr.trackedEntityInstance}`, curr)
            } catch (err) {
              err.row = currRow
              throw err
            }
          })())
        }
        await Promise.all(promises)
        this.recordsProcessedCount = records.length
      } catch (err) {
        const isCtcCollision = checkCtcCollision(err)
        const addressMsg = err.row ? `${this.$i18n.t('on-row')}${err.row}: ` : ''
        const msg = isCtcCollision ? this.$i18n.t('ctc_collision') : this.$i18n.t('tech_issues')
        validationFailures.push(addressMsg + msg)
        zz.consoleError(`Failed to POST empowerment group form data to server`, err)
        return
      }
      zz.resetCache()
      this.refreshOrgUnits()
    },
    onDisplaySummary (isShow) {
      this.isShowSummary = isShow
    },
    onWardSelected () {
      const ward = this.wards.find(e => e.id === this.selectedWard)
      this.empGroups = ward.children
      this.empGroups.sort(zz.displayNameSorterFn)
      if (this.empGroups.length) {
        this.selectedEmpGroup = this.empGroups[0].id
      } else {
        this.selectedEmpGroup = ''
      }
    },
    async downloadEmpGroupForm () {
      this.isError = false
      this.downloadEmpGroupFormStatus = 'loading'
      try {
        const empGroup = this.empGroups.find(e => e.id === this.selectedEmpGroup) || zz.die('No empGroup found')
        const empGroupNameForDownload = empGroup.displayName
        const today = new Date().toLocaleDateString('sw')
        const filename = 'NACOPHA_SautiYetu_kikundi_' + empGroupNameForDownload + '_' + today + '.xlsx'
        let workbook
        try {
          const templateBytes = await zz.getTemplateBytes(zz.empGroupFormTemplateName)
          workbook = await XlsxPopulate.fromDataAsync(templateBytes)
        } catch (err) {
          throw zz.chainedError(err, 'Failed to get the template')
        }
        try {
          const wardName = this.wards.find(e => e.id === this.selectedWard).displayName
          const embeddedConfig = {
            [zz.keys.templateVersion]: zz.empGroupTemplateVersion,
            empGroup: this.selectedEmpGroup,
            wardName,
            empGroupNameForDownload,
            groupAttributeValues: empGroup.attributeValues
          }
          await this.createSheet(workbook, embeddedConfig)
        } catch (err) {
          throw zz.chainedError(err, 'Failed to populate the template')
        }
        let completeWorkbook
        try {
          completeWorkbook = await workbook.outputAsync()
        } catch (err) {
          throw zz.chainedError(err, 'Failed to generate the completed workbook')
        }
        zz.triggerDownload(completeWorkbook, filename)
      } catch (err) {
        this.downloadEmpGroupFormStatus = 'failed'
        zz.consoleError('Failed to prepare empowerment group form', err)
        return
      }
      this.downloadEmpGroupFormStatus = 'success'
    },
    async createEmpGroup () {
      this.isError = false
      this.createEmpGroupStatus = 'creating'
      try {
        const orgUnit = this.selectedWard
        const body = {
          name: this.empGroupName,
          shortName: this.empGroupName,
          openingDate: '2018-01-01T00:00:00.000Z', // TODO does this date matter?
          parent: {
            id: orgUnit
          }
        }
        const resp = await axios.post(`${zz.apiBaseUrl}/organisationUnits`, body)
        const newId = resp.data.response.uid
        await Promise.all([
          addOrgUnitToGroup(newId),
          addOrgUnitToProgram(newId)
        ])
        console.log(`ID of new record: ${newId}`)
        zz.resetCache()
        this.refreshOrgUnits()
      } catch (err) {
        this.createEmpGroupStatus = 'failed'
        zz.consoleError('Failed to create a new empowerment group', err)
        return
      }
      this.createEmpGroupStatus = 'success'
    },
    async processUploadedSheet (sheet, validationFailures) {
      const hiddenConfigRow = zz.findMarkerRow(sheet, zz.markers.hiddenConfig)
      const hiddenConfigCol = zz.findMarkerColNumber(sheet, zz.markers.hiddenConfig)
      const embeddedConfig = (() => {
        const colName = sheet.column(hiddenConfigCol).columnName()
        const raw = sheet.cell(`${colName}${hiddenConfigRow}`).value()
        const obj = JSON.parse(raw)
        return obj[zz.headerTitleField]
      })()
      const uploadedVersion = embeddedConfig[zz.keys.templateVersion]
      if (uploadedVersion === zz.dataEntryTemplateVersion) {
        validationFailures.push(this.$i18n.t('de-form-instead-of-eg'))
        return
      }
      if (uploadedVersion !== zz.empGroupTemplateVersion) {
        zz.consoleLog(`User uploaded template version='${uploadedVersion}' where ` +
          `we expected version='${zz.empGroupTemplateVersion}'`)
        validationFailures.push(this.$i18n.t('old-form-version'))
        return
      }
      const checkValueCellAddress = embeddedConfig[addressesKey][zz.keys.checkValueCellAddress]
      const checkValue = sheet.cell(checkValueCellAddress).value()
      console.debug(`${sheet.name()} has check value='${checkValue}' in cell='${checkValueCellAddress}'`)
      if (checkValue < 0) {
        const msgPart1 = this.$i18n.t('de-failed-upload-1')
        const msgPart2 = this.$i18n.t('de-form-check-failed')
        validationFailures.push(`${msgPart1}'${sheet.name()}' ${msgPart2}`)
        return
      }
      try {
        await parseFormHeaders(sheet, validationFailures, embeddedConfig, this.$i18n)
      } catch (err) {
        zz.consoleError('Failed to parse form headers', err)
        validationFailures.push(this.$i18n.t('eg-parse-headers-fail'))
        return
      }
      const dataStartRow = zz.findMarkerRow(sheet, zz.markers.dataStart)
      const dataEndRow = zz.findMarkerRow(sheet, zz.markers.dataEnd)
      const endOfDataMarkerRow = zz.findMarkerRow(sheet, endOfDataText)
      const isNoTeis = dataStartRow === endOfDataMarkerRow
      if (isNoTeis) {
        return []
      }
      const dataEndColIndex = hiddenConfigCol - 1
      const dataEndCol = sheet.column(dataEndColIndex).columnName()
      const headerRow = await (async () => {
        const extraValues = await fetchColumnDefs()
        const rangeValues = sheet.range(`A${hiddenConfigRow}:${dataEndCol}${hiddenConfigRow}`).value()
        const rowValues = rangeValues[0]
        return rowValues.map(e => {
          const basicHeader = JSON.parse(e)
          const headerId = basicHeader[zz.headerIdField]
          const record = Object.assign(basicHeader, extraValues[headerId])
          if (record.optionSet && record.optionSet.options) {
            record.options = record.optionSet.options.reduce((accum, curr) => {
              accum[curr.displayName] = curr.code
              return accum
            }, {})
          }
          return record
        })
      })()
      const records = await fetchRecordsForUpload(embeddedConfig.empGroup)
      // TODO could add 'touched' flag to records so later, we can just updated the touched ones
      const teiColIndex = headerRow.findIndex(e => e[zz.headerIdField] === teiIdType)
      for (let rowIndex = dataStartRow; rowIndex <= dataEndRow; rowIndex++) {
        const rowValues = sheet.range(`A${rowIndex}:${dataEndCol}${rowIndex}`).value()[0]
        const teiId = rowValues[teiColIndex]
        if (!teiId) {
          zz.consoleLog(`User uploaded EG form with missing TEI ID, which means they've deleted a row.`)
          const msgPart1 = this.$i18n.t('eg-row-prefix')
          const msgPart2 = this.$i18n.t('eg-dont-delete-rows')
          validationFailures.push(`${msgPart1}${rowIndex}${msgPart2}`)
          return
        }
        const record = records[teiId]
        if (!record) {
          zz.consoleLog(`User uploaded EG form with TEI='${teiId}' for orgUnit='${embeddedConfig.empGroup}' but the ` +
            `server doesn't know about this TEI (for this orgUnit).`)
          const msgPart1 = this.$i18n.t('eg-row-prefix')
          const msgPart2 = this.$i18n.t('eg-unknown-tei-id')
          validationFailures.push(`${msgPart1}${rowIndex}${msgPart2}`)
          return
        }
        record.row = rowIndex // just for helpful error msgs, be sure to delete this field before sending to server
        for (let colIndex = 0; colIndex < headerRow.length; colIndex++) {
          if (colIndex === teiColIndex) {
            continue
          }
          const header = headerRow[colIndex]
          const updateInplaceUsingStrategy = this.getStrategy(header, embeddedConfig.empGroup) ||
            zz.die(`No strategy found for header='${JSON.stringify(header)}'`)
          try {
            await updateInplaceUsingStrategy(record, rowValues[colIndex])
          } catch (err) {
            const columnLetter = sheet.column(colIndex + 1).columnName()
            zz.consoleLog(`Error parsing uploaded EG form at row=${rowIndex}, col=${columnLetter} for tei=${teiId}, ` +
              `\nerror=${err.stack}\n\nthe current record=${JSON.stringify(record, null, 2)}`)
            const msgPart1 = this.$i18n.t('on-row')
            const msgPart2 = this.$i18n.t('eg-at-col')
            const msgPart3 = this.$i18n.t('eg-val-cant-be-parsed')
            validationFailures.push(`${msgPart1}${rowIndex}${msgPart2}${columnLetter}${msgPart3}`)
            return
          }
        }
      }
      return Object.values(records)
    },
    getStrategy (colDef, orgUnit) {
      const colId = colDef[zz.headerIdField]
      if (colId === empGroupEnrolDateType) {
        const dateStrategy = this.mixinGetColTypeStrategy(zz.dateType, false)
        return async function (record, value) {
          if (!value) {
            return
          }
          const programId = await zz.getProgramId()
          const mappedValue = await dateStrategy.displayToValue(value)
          const enrollment = record.enrollments.find(e =>
            e.orgUnit === orgUnit &&
            e.status === 'ACTIVE' &&
            e.program === programId)
          if (!enrollment) {
            throw new Error(`Could not find active enrollment for orgUnit='${orgUnit}', ` +
              `tei='${record.trackedEntityInstance}', program='${programId}', ` +
              `with enrollments=${JSON.stringify(record.enrollments, null, 2)}`)
          }
          enrollment.enrollmentDate = mappedValue
        }
      }
      const self = this
      return async function (record, value) {
        if (!value) {
          return
        }
        const colType = colDef.valueType
        const hasOptions = colDef.optionSetValue
        record.attributes = record.attributes || []
        const valueMapperStrategy = self.mixinGetColTypeStrategy(colType, hasOptions)
        const mappedValue = await valueMapperStrategy.displayToValue(value, colDef.options)
        const attribute = record.attributes.find(e => e.attribute === colId)
        if (attribute) {
          attribute.value = mappedValue
          return
        }
        record.attributes.push({
          attribute: colId,
          value: mappedValue
        })
      }
    },
    async createSheet (workbook, embeddedConfig) {
      let teis = await fetchTeisForDownload(embeddedConfig.empGroup)
      const headerRow = await getHeaderRow()
      const columnDefs = await fetchColumnDefs()
      const rowData = await this._generateRowData(teis, headerRow, columnDefs)
      const sheet = workbook.sheet(0)
      const dataRowStart = zz.findMarkerRow(sheet, zz.markers.dataStart)
      deleteExistingMarker(sheet, zz.markers.dataStart)
      const hiddenConfigRowNum = zz.findMarkerRow(sheet, zz.markers.hiddenConfig)
      const endColLetter = sheet.column(headerRow.length).columnName()
      const serialisedHeaders = headerRow.map(e => JSON.stringify(e))
      zz.writeHiddenValueRange(sheet, `A${hiddenConfigRowNum}:${endColLetter}${hiddenConfigRowNum}`, [ serialisedHeaders ])
      const hiddenConfigColumn = sheet.column(headerRow.length + 1).columnName()
      this._addVillageAndActivityGroupAddresses(sheet, embeddedConfig)
      const configMarkerAndVersion = zz.buildColHeader(zz.markers.hiddenConfig, embeddedConfig)
      zz.writeHiddenValue(sheet, `${hiddenConfigColumn}${hiddenConfigRowNum}`, configMarkerAndVersion)
      const dataRowMaxEnd = zz.findMarkerRow(sheet, zz.markers.dataEnd)
      const availableRows = dataRowMaxEnd - dataRowStart
      if (teis.length > availableRows) {
        zz.consoleError(`Too many TEIs '${teis.length}' for available rows '${availableRows}' in empowerment group form, ` +
          `config='${JSON.stringify(embeddedConfig)}'`)
        teis = teis.slice(0, availableRows)
      }
      deleteExistingMarker(sheet, zz.markers.dataEnd)
      const dataRowEnd = dataRowStart + teis.length - 1
      const isNoTeis = teis.length === 0
      const isOneTei = teis.length === 1
      if (isNoTeis || isOneTei) {
        const combinedMarkers = `${zz.markers.dataStart} ${zz.markers.dataEnd}`
        zz.writeHiddenValue(sheet, `${hiddenConfigColumn}${dataRowStart}`, combinedMarkers)
      } else {
        zz.writeHiddenValue(sheet, `${hiddenConfigColumn}${dataRowStart}`, zz.markers.dataStart)
        zz.writeHiddenValue(sheet, `${hiddenConfigColumn}${dataRowEnd}`, zz.markers.dataEnd)
      }
      sheet.cell(`A${dataRowEnd + 1}`)
        .value(`(${endOfDataText} - ${teis.length} ${this.$i18n.t('plhiv')} )`)
        .style('fontColor', '000000')
        .style('fontSize', 12)
      await this.setOrgUnitHeader(sheet, embeddedConfig)
      const fullNameAttributeId = await zz.getFullNameAttributeId()
      const fullNameIndex = headerRow.findIndex(e => e[zz.headerIdField] === fullNameAttributeId)
      rowData.sort((a, b) => {
        if (a[fullNameIndex] < b[fullNameIndex]) { return -1 }
        if (a[fullNameIndex] > b[fullNameIndex]) { return 1 }
        return 0
      })
      // write rows with data
      sheet
        .range(`A${dataRowStart}:${endColLetter}${dataRowEnd}`)
        .value(rowData)
      // set "end of data rows" row styles so we can see the message
      sheet
        .range(`A${dataRowEnd + 1}:${endColLetter}${dataRowEnd + 1}`)
        .style({
          fill: { type: 'solid', color: 'ffffff' },
          borderColor: 'cccccc',
          topBorderColor: '000000'
        })
      // blank out the cells under the end message
      sheet
        .range(`A${dataRowEnd + 2}:${endColLetter}${dataRowMaxEnd}`)
        .style({
          fill: { type: 'solid', color: 'ffffff' },
          borderColor: 'cccccc'
        })
      // add hints to not use blank rows
      sheet
        .range(`A${dataRowEnd + 2}:A${dataRowMaxEnd}`)
        .style('fontColor', '808080')
        .value(this.$i18n.t('dont_add_data'))
      const tasks = []
      for (let i = 0; i < headerRow.length; i++) {
        const currColLetter = String.fromCharCode(asciiCapitalA + i)
        const currHeader = headerRow[i]
        tasks.push(this.processCol(currHeader, currColLetter, dataRowStart, dataRowEnd, dataRowMaxEnd + 2, columnDefs, sheet))
      }
      await Promise.all(tasks)
    },
    async setOrgUnitHeader (sheet, embeddedConfig) {
      try {
        const orgUnitHierarchy = await zz.getOrgUnitHierarchy()
        zz.writeValueForMarker(zz.markers.region, orgUnitHierarchy.region, sheet)
        zz.writeValueForMarker(zz.markers.district, orgUnitHierarchy.district, sheet)
        zz.writeValueForMarker(zz.markers.cluster, orgUnitHierarchy.cluster, sheet)
        zz.writeValueForMarker(zz.markers.ward, embeddedConfig.wardName, sheet)
        zz.writeValueForMarker(zz.markers.empGroup, embeddedConfig.empGroupNameForDownload, sheet)
        zz.writeValueForMarker(zz.markers.zone, orgUnitHierarchy.zone, sheet)
        const groupRegoNumAttrId = await zz.getGroupRegoNumAttrId()
        const groupRegoNum = (embeddedConfig.groupAttributeValues.find(e => e.attribute.id === groupRegoNumAttrId) || {}).value
        zz.writeValueForMarker(zz.markers.groupRegoNum, groupRegoNum, sheet)
        const villageAttrId = await zz.getVillageAttrId()
        const villageName = (embeddedConfig.groupAttributeValues.find(e => e.attribute.id === villageAttrId) || {}).value
        zz.writeValueForMarker(zz.markers.village, villageName, sheet)
        await this._processActivityGroups(sheet, embeddedConfig)
      } catch (err) {
        throw zz.chainedError(err, `Failed to set org unit hierarchy headers for embeddedConfig=${JSON.stringify(embeddedConfig)}`)
      }
    },
    async processCol (currHeader, currColLetter, dataRowStart, dataRowEnd, startOfSuggestionsRow, columnDefs, sheet) {
      const colDefId = currHeader[zz.headerIdField]
      const colDef = columnDefs[colDefId] || zz.die(`No colDef found for id='${colDefId}' in ${JSON.stringify(columnDefs)}`)
      const colType = colDef.valueType
      const range = sheet.range(`${currColLetter}${dataRowStart}:${currColLetter}${dataRowEnd}`)
      const specialColProcessors = {
        [teiIdType]: function (range) {
          range.style({ 'fontSize': '1', 'fontColor': 'ffffff' })
        },
        [empGroupEnrolDateType]: function (range) {
          const dvOfFirstCell = range.startCell().dataValidation()
          range.dataValidation(dvOfFirstCell)
        }
      }
      const isTeis = dataRowEnd >= dataRowStart
      const specialProcessor = specialColProcessors[colType]
      if (specialProcessor) {
        if (isTeis) {
          specialProcessor(range)
        }
        return
      }
      const hasOptions = colDef.optionSetValue
      const strategy = this.mixinGetColTypeStrategy(colType, hasOptions)
      if (!strategy) {
        throw new Error(`Programmer problem: no validation defined for ` +
          `column='${currHeader[zz.headerTitleField]}' with type='${colType}'` +
          `, hasOptions='${hasOptions}'`)
      }
      if (!strategy.getValues || typeof (strategy.getValues) !== 'function') {
        throw new Error(`Programmer problem: there is no 'getValues()' for '${colType}' columns`)
      }
      const completedValidation = await strategy.getValues({ colDef })
      if (!completedValidation) {
        return
      }
      let rowCounter = 0
      for (const curr of completedValidation.itemArray) {
        zz.writeHiddenValue(sheet, `${currColLetter}${startOfSuggestionsRow + rowCounter}`, curr)
        rowCounter++
      }
      if (!isTeis) {
        return
      }
      range.dataValidation(completedValidation.validationObj)
    },
    async _buildRow (tei, headerRow, columnDefs) {
      const specialColProcessors = {
        [teiIdType]: t => t.trackedEntityInstance,
        [empGroupEnrolDateType]: t => {
          if (!t.enrollments.length) {
            return null
          }
          const dateStr = t.enrollments[0].enrollmentDate // shouldn't be more than 1
          const strategy = this.mixinGetColTypeStrategy(zz.dateType, false)
          return strategy.valueToDisplay(null, dateStr)
        }
      }
      const mapCell = async (curr) => {
        const colId = curr[zz.headerIdField]
        const specialProcessor = specialColProcessors[colId]
        if (specialProcessor) {
          return specialProcessor(tei)
        }
        const attr = tei.attributes.find(e => e.attribute === colId)
        if (!attr) {
          return null
        }
        const colDef = columnDefs[colId] || zz.die(`Column with id='${colId}' not found`)
        const colType = colDef.valueType
        const hasOptions = colDef.optionSetValue
        const strategy = this.mixinGetColTypeStrategy(colType, hasOptions)
        if (!strategy) {
          throw new Error(`Programmer problem: no validation defined for column='${curr[zz.headerTitleField]}'` +
            ` with type='${colType}', hasOptions='${hasOptions}'`)
        }
        const mappedValue = await strategy.valueToDisplay(colDef, attr.value)
        return mappedValue
      }
      const promises = headerRow.map(curr => { return mapCell(curr) })
      return Promise.all(promises)
    },
    async _generateRowData (teis, headerRow, columnDefs) {
      const promises = teis.map(curr => {
        return this._buildRow(curr, headerRow, columnDefs)
      })
      return Promise.all(promises)
    },
    _findActivityGroupAddress (sheet, marker) {
      const cell = zz.findMarkerCell(marker, sheet)
      const startCol = cell.columnName()
      const startRow = cell.rowNumber()
      const rangeAddress = `${startCol}${startRow}:${startCol}${startRow + 2}`
      return rangeAddress
    },
    _addVillageAndActivityGroupAddresses (sheet, embeddedConfig) {
      const villageCell = zz.findMarkerCell(zz.markers.village, sheet) || zz.die(`No village marker='${zz.markers.village}' found on sheet`)
      const villageCellAddress = villageCell.address()
      const groupRegoNumCell = zz.findMarkerCell(zz.markers.groupRegoNum, sheet) || zz.die(`No group registration number ` +
        `marker='${zz.markers.groupRegoNum}' found on sheet`)
      const groupRegoNumCellAddress = groupRegoNumCell.address()
      const checkValueCellAddress = zz.getCheckValueCellAddress(sheet)
      embeddedConfig[addressesKey] = {
        economic: this._findActivityGroupAddress(sheet, zz.markers.activities.economic),
        communityHiv: this._findActivityGroupAddress(sheet, zz.markers.activities.communityHiv),
        social: this._findActivityGroupAddress(sheet, zz.markers.activities.social),
        village: villageCellAddress,
        groupRegoNum: groupRegoNumCellAddress,
        [zz.keys.checkValueCellAddress]: checkValueCellAddress
      }
    },
    async _writeActivityGroupValues (sheet, rangeAddress, attributeIds, groupVocab, groupData) {
      const values = zz.extractActivityGroup(attributeIds, groupVocab, groupData)
      const dv = await this.mixinBuildValidationForAttribute(attributeIds[0])
      sheet.range(rangeAddress)
        .dataValidation(dv)
        .value(values.map(e => [e]))
    },
    async _processActivityGroups (sheet, embeddedConfig) {
      const groupVocab = await _mockable.fetchActivityGroupVocab()
      const attributeIds = await _mockable.fetchGroupActivityAttributeIds()
      try {
        const promises = [
          this._writeActivityGroupValues(sheet, embeddedConfig[addressesKey].economic, attributeIds.economic,
            groupVocab, embeddedConfig.groupAttributeValues),
          this._writeActivityGroupValues(sheet, embeddedConfig[addressesKey].communityHiv, attributeIds.communityHiv,
            groupVocab, embeddedConfig.groupAttributeValues),
          this._writeActivityGroupValues(sheet, embeddedConfig[addressesKey].social, attributeIds.social,
            groupVocab, embeddedConfig.groupAttributeValues)
        ]
        await Promise.all(promises)
      } catch (err) {
        throw zz.chainedError(err, 'Failed while populating group activities')
      }
    },
    handleError (msg, error) {
      this.mixinLastDitchErrorHandler(msg, error)
      this.isError = true
    }
  }
}

async function addOrgUnitToProgram (orgUnitId) {
  try {
    const programId = await zz.getProgramId()
    await axios.post(`${zz.apiBaseUrl}/programs/${programId}/organisationUnits/${orgUnitId}`)
  } catch (err) {
    throw zz.chainedError(err, `Failed while adding orgUnit='${orgUnitId}' to program`)
  }
}

/*
 * Adds the orgUnit to the "empowerment group" org unit group.
 */
async function addOrgUnitToGroup (orgUnitId) {
  try {
    const empGroupOrgUnitGroup = await zz.getOrgUnitGroupId()
    await axios.post(`${zz.apiBaseUrl}/organisationUnitGroups/${empGroupOrgUnitGroup}/organisationUnits/${orgUnitId}`)
  } catch (err) {
    throw zz.chainedError(err, `Failed while adding orgUnit='${orgUnitId}' to org unit group`)
  }
}

async function parseVillage (sheet, embeddedConfig) {
  const villageAttrId = await zz.getVillageAttrId()
  return _parseAttribute(sheet, embeddedConfig, villageAttrId, 'village')
}

async function parseGroupRegoNum (sheet, embeddedConfig) {
  const groupRegoNumAttrId = await zz.getGroupRegoNumAttrId()
  return _parseAttribute(sheet, embeddedConfig, groupRegoNumAttrId, 'groupRegoNum')
}

async function _parseAttribute (sheet, embeddedConfig, attrId, configKey) {
  const cellAddress = (embeddedConfig[addressesKey] || {})[configKey]
  if (!cellAddress) {
    throw new Error(`Could not find '${configKey}' cell address in embeddedConfig=${JSON.stringify(embeddedConfig)}`)
  }
  const oldValue = (embeddedConfig.groupAttributeValues.filter(e => e.attribute.id === attrId) || {}).value
  const newValue = sheet.cell(cellAddress).value()
  if (oldValue === newValue) {
    console.debug(`${configKey} value='${oldValue}' has not changed, skipping update.`)
    const nothing = []
    return nothing
  }
  return {
    attribute: {
      id: attrId
    },
    value: newValue
  }
}

async function parseActivityGroups (sheet, validationFailures, embeddedConfig, i18n) {
  const addresses = embeddedConfig[addressesKey]
  const attributeIds = await _mockable.fetchGroupActivityAttributeIds()
  const promises = [
    parseSingleActivityGroup(sheet, validationFailures, addresses.economic, attributeIds.economic,
      i18n.t('economic_activities'), i18n),
    parseSingleActivityGroup(sheet, validationFailures, addresses.communityHiv, attributeIds.communityHiv,
      i18n.t('community_hiv_activities'), i18n),
    parseSingleActivityGroup(sheet, validationFailures, addresses.social, attributeIds.social,
      i18n.t('social_activities'), i18n)
  ]
  const results = await Promise.all(promises)
  return results.reduce((accum, curr) => {
    return accum.concat(curr)
  }, [])
}

async function parseFormHeaders (sheet, validationFailures, embeddedConfig, i18n) {
  let attributeValues = []
  attributeValues = attributeValues.concat(await parseActivityGroups(sheet, validationFailures, embeddedConfig, i18n))
  attributeValues = attributeValues.concat(await parseVillage(sheet, embeddedConfig))
  attributeValues = attributeValues.concat(await parseGroupRegoNum(sheet, embeddedConfig))
  if (validationFailures.length) {
    return
  }
  const orgUnit = embeddedConfig.empGroup
  try {
    const url = `${zz.apiBaseUrl}/organisationUnits/${orgUnit}`
    const getResp = await axios.get(url, {
      params: { fields: '*' }
    })
    const body = getResp.data
    body.attributeValues = attributeValues
    await axios.put(url, body)
  } catch (err) {
    throw zz.chainedError(err, `Failed to update attribute values for empGroup='${orgUnit}'`)
  }
}

async function parseSingleActivityGroup (sheet, validationFailures, range, relevantIds, groupName, i18n) {
  const values = sheet.range(range).value()
  const lookup = await _mockable.fetchAttributeOptions(relevantIds[0])
  const result = []
  for (let i = 0; i < relevantIds.length; i++) {
    const attributeId = relevantIds[i]
    const displayNameValue = values[i][0]
    if (!displayNameValue) {
      continue
    }
    const code = (lookup.find(e => e.displayName === displayNameValue) || {}).code
    if (!code) {
      validationFailures.push(`${i18n.t('the_value')} '${displayNameValue}' ${i18n.t('is_not_valid_option')} ` +
        `${groupName} ${i18n.t('activity_group')}`)
      continue
    }
    result.push({
      value: code,
      attribute: { id: attributeId }
    })
  }
  // TODO consider checking for duplicate entries and validationFailures.push(); return
  return result
}

async function fetchRecordsForUpload (orgUnit) {
  const ouMode = 'SELECTED'
  const noFilter = null
  const fields = '*'
  try {
    const teis = await zz.getTeiRecords(orgUnit, ouMode, noFilter, fields)
    const result = teis.reduce((accum, curr) => {
      accum[curr.trackedEntityInstance] = curr
      return accum
    }, {})
    return result
  } catch (err) {
    throw zz.chainedError(err, 'Failed to fetch existing TEI records for update')
  }
}

// TODO make this config in the admin page
async function getHeaderRow () {
  const c = zz.buildColHeader
  const fullNameAttributeId = await zz.getFullNameAttributeId()
  return [
    c(teiIdType, 'TEI ID'),
    c(fullNameAttributeId, 'Full name'),
    c('jeiN3PX6zqu', 'Gender'),
    c('nWEOJSdLtH3', 'Date of birth'),
    c('V5T27cVPMG0', 'Marital Status'),
    c('p6pdYqyYsvM', 'Number of Boys under 18 years'),
    c('bof4IRwgHwa', 'Number of Girls under 18 years'),
    c(ctcAttributeId, 'CTC Number'),
    c('e3ENws3guve', 'On ART?'),
    c('vBZ7e7XM2kH', 'Name of CTC attended'),
    c('pgBG2Bic7vl', 'Date started ART'),
    c(empGroupEnrolDateType, 'Empowerment group enrollment date'),
    c('fWlJ0KzmOZs', 'Empowerment Group (Role)'),
    c('OavhN80L78w', 'Individual economic activity'),
    c('z2q6ASGYCt0', 'Phone number')
  ]
}

function deleteExistingMarker (sheet, marker) {
  const cell = zz.findMarkerCell(marker, sheet)
  cell.value('')
}

async function fetchColumnDefs () {
  try {
    const resp = await axios.get(`${zz.apiBaseUrl}/trackedEntityAttributes`, {
      params: {
        paging: false,
        fields: `
          id,
          displayName,
          valueType,
          optionSetValue,
          optionSet[
            options[
              code,
              displayName
            ]
          ]`.replace(/\s/g, '')
      }
    })
    const mapped = resp.data.trackedEntityAttributes.reduce((accum, curr) => {
      accum[curr.id] = curr
      return accum
    }, {})
    mapped[teiIdType] = {valueType: teiIdType}
    mapped[empGroupEnrolDateType] = {valueType: empGroupEnrolDateType}
    return mapped
  } catch (err) {
    throw zz.chainedError(err, `Failed to fetch tracked entity attributes column definitions`)
  }
}

async function fetchTeisForDownload (empGroup) {
  const ouMode = 'SELECTED'
  const noFilter = null
  return zz.getTeiRecords(empGroup, ouMode, noFilter)
}

/**
 * Interrogate the error to see if it was caused by a CTC collision
 */
function checkCtcCollision (err) {
  try {
    const body = err.response.data
    if (body.httpStatusCode !== 409) {
      return false
    }
    const conflicts = body.response.conflicts
    const conflictMsg = conflicts && conflicts[0] && conflicts[0].value
    if (!conflictMsg) {
      return false
    }
    const isAboutCtc = conflictMsg.indexOf(ctcAttributeId) >= 0
    if (!isAboutCtc) {
      return false
    }
    return true
  } catch (err) {
    zz.consoleError('Failed while checking error for CTC collisions', err)
    return false
  }
}
