import axios from 'axios'
import * as zz from './common'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-balham.css'
import 'ag-grid-community'
import {AgGridVue} from 'ag-grid-vue'
import CreateTei from '@/components/subcomponents/CreateTeiComponent'
import {validateCtc} from '@/components/subcomponents/CreateTei'
const moment = require('moment')

const tableOrgUnitFieldName = 'orgUnit'

export const UserMgmt = {
  name: 'UserMgmt',
  mixins: [zz.commonMixin],
  data() {
    const result = {
      isLoading: true,
      moveTeiStatus: 'initial',
      movedTeiCount: 0,
      isError: false,
      genders: [],
      selectedWard: '',
      wards: [],
      selectedOrgUnit: '',
      wardsAndGroups: [],
      isWardSelected: null,
      teisForMoveWithNoCtc: [],
      searchTeiStatus: 'initial',
      clusterId: null,
      columnDefs: null, // replaced in created()
      rowData: [],
      agGridApi: null,
      presetEgFilterText: null,
      selectedTeis: [],
      displayedRowCount: 0,
      teisToMoveCount: 0,
    }
    return result
  },
  created() {
    if (this.getPreselectQueryParam()) {
      this.searchTeiStatus = 'searching'
    }
    Promise.all([this.fetchFormValues(), this._configureColumnDefs()])
      .then(() => {
        this.autoTriggerSearchIfRequired()
      })
      .catch((err) => {
        this.handleError(
          'Failed to run .created() function for UserMgmt component',
          err,
        )
      })
  },
  components: {
    'ag-grid-vue': AgGridVue,
    'create-tei': CreateTei,
  },
  computed: {
    isFilterActive() {
      return this.agGridApi && this.agGridApi.isAnyFilterPresent()
    },
  },
  methods: {
    getPreselectQueryParam() {
      return this.$route.query[zz.userMgmtOuPreselectQueryParamName]
    },
    async autoTriggerSearchIfRequired() {
      const preselectSearchOu = this.getPreselectQueryParam()
      if (!preselectSearchOu || preselectSearchOu.length < 11) {
        console.debug('No org unit is preselected')
        return
      }
      const ward = this.wards.find((w) => {
        return w.children.find((c) => c.id === preselectSearchOu)
      })
      if (!ward) {
        console.warn(
          `Valid-ish preselect OU was passed='${preselectSearchOu}', but could not find matching ward`,
        )
        return
      }
      const egName = ward.children.find((e) => e.id === preselectSearchOu)
        .displayName
      this.presetEgFilterText = zz.concatWardAndEg(ward.displayName, egName)
      this.selectedWard = ward.id
      this.searchForTeis()
    },
    async _configureColumnDefs() {
      // needs to be done here because we need to be able to run async functions
      // eslint-disable-next-line
      let yesLabel
      // eslint-disable-next-line
      let noLabel
      try {
        yesLabel = await zz.getYesConstantDisplayName()
        noLabel = await zz.getNoConstantDisplayName()
      } catch (err) {
        throw zz.chainedError(err, 'Failed to fetch yes/no labels')
      }
      this.columnDefs = [
        {headerName: 'Action', checkboxSelection: true, width: 25},
        {headerName: this.$i18n.t('full_name'), field: 'fullName'},
        {headerName: this.$i18n.t('date_of_birth'), field: 'dob', width: 70},
        {headerName: this.$i18n.t('gender'), field: 'gender', width: 80},
        {
          headerName: 'CTC',
          field: 'ctc',
          valueFormatter: (params) => params.value || '-',
          width: 100,
        },
        {
          headerName: 'Nacopha ID',
          field: 'nacophaId',
          valueFormatter: (params) => params.value || '(ERROR)',
          cellStyle: (params) => {
            return {backgroundColor: params.data.nacophaId ? null : 'pink'}
          },
          width: 90,
        },
        {
          headerName: this.$i18n.t('ward_emp_group'),
          field: tableOrgUnitFieldName,
          width: 200,
        }, // ,
      ]
      if (this.$route.query.showhealth) {
        // trigger with query param: ?showhealth=true
        this.columnDefs.push({
          headerName: this.$i18n.t('ready_to_record_data'),
          width: 75,
          valueGetter: (params) =>
            params.data.recordHealth ? yesLabel : noLabel,
          cellStyle: (params) => {
            return {
              backgroundColor: params.data.recordHealth ? 'lightgreen' : 'pink',
            }
          },
        })
      }
    },
    getLocaleText() {
      return {
        noRowsToShow: `<h2>${this.$i18n.t('no_table_rows')}</h2>`,
      }
    },
    onGridFilterChanged(params) {
      this.displayedRowCount = params.api.getDisplayedRowCount()
      if (this.displayedRowCount === 0) {
        params.api.showNoRowsOverlay()
      } else {
        params.api.hideOverlay()
      }
    },
    onGridReady(params) {
      params.api.sizeColumnsToFit()
      this.agGridApi = params.api
      this.onGridFilterChanged(params)
      if (!this.presetEgFilterText) {
        return
      }
      const colFilter = this.agGridApi.getFilterInstance(tableOrgUnitFieldName)
      colFilter.setModel({
        type: 'equals', // because the ward name is prefixed
        filter: this.presetEgFilterText,
      })
      this.agGridApi.onFilterChanged()
    },
    rowSelectionChanged() {
      if (!this.agGridApi) {
        return
      }
      const selectedRows = this.agGridApi.getSelectedNodes()
      this.selectedTeis = selectedRows.map((e) => e.data.id)
      const existingCtcs = this.teisForMoveWithNoCtc.reduce((accum, curr) => {
        accum[curr.id] = curr.ctc
        return accum
      }, {})
      const newValue = selectedRows
        .filter((e) => !e.data.ctc)
        .map((e) => {
          return {
            id: e.data.id,
            fullName: e.data.fullName,
            ctc: existingCtcs[e.data.id],
          }
        })
      this.teisForMoveWithNoCtc = newValue
    },
    async fetchFormValues() {
      this.isLoading = true
      this.isError = false
      try {
        await Promise.all([
          this._fetchWards(),
          this._fetchGenders(),
          this._fetchCluster(),
        ])
      } catch (err) {
        this.handleError('Failed to populate form values', err)
      } finally {
        this.isLoading = false
      }
    },
    async _fetchGenders() {
      try {
        this.genders = await zz.fetchGenders()
      } catch (err) {
        throw zz.chainedError(err, 'Failed to get a list of genders.')
      }
    },
    async _fetchCluster() {
      try {
        const cluster = await zz.fetchCluster()
        this.clusterId = cluster.id
      } catch (err) {
        this.handleError('Failed to get the current cluster', err)
      }
    },
    async _fetchWards() {
      this.wardsAndGroups = await zz.fetchWardsAndGroups()
      this.wards = this.wardsAndGroups.filter((e) => e.isWard)
    },
    resetTableFilters() {
      if (!this.agGridApi) {
        return
      }
      this.presetEgFilterText = null
      this.agGridApi.setFilterModel(null)
    },
    async searchForTeis() {
      this.isError = false
      this.rowData = []
      this.searchTeiStatus = 'searching'
      try {
        this.resetTableFilters()
        this.rowData = await zz.fetchTeiTable(
          this.selectedWard,
          this.wardsAndGroups,
        )
      } catch (err) {
        this.searchTeiStatus = 'failed'
        zz.consoleError('Failed to search for TEIs', err)
        return
      }
      this.searchTeiStatus = 'success'
    },
    cellDoubleClicked(cell) {
      console.log(`${cell.data.fullName} has ID ${cell.data.id}`)
    },
    onOrgUnitForMoveSelected() {
      this.isWardSelected = this.wardsAndGroups.find(
        (e) => e.id === this.selectedOrgUnit,
      ).isWard
    },
    isCtcEntryRequired() {
      if (this.isWardSelected === null) {
        return false
      }
      const isEmpGroupSelected = !this.isWardSelected
      return isEmpGroupSelected && this.teisForMoveWithNoCtc.length
    },
    isMoveFormValid() {
      for (const curr of this.teisForMoveWithNoCtc) {
        const ctcAlwaysRequired = true
        const result = validateCtc(ctcAlwaysRequired, curr.ctc, this.$i18n)
        this.$set(curr, 'validationMsg', result.msg) // https://vuejs.org/v2/guide/list.html#Object-Change-Detection-Caveats
      }
      const isAllCtcsValid = this.teisForMoveWithNoCtc.every(
        (e) => !e.validationMsg,
      )
      const isCtcEntryValid = !this.isCtcEntryRequired() || isAllCtcsValid
      return this.selectedOrgUnit && this.selectedTeis.length && isCtcEntryValid
    },
    async moveTeis() {
      this.isError = false
      this.moveTeiStatus = 'loading'
      this.movedTeiCount = 0
      this.teisToMoveCount = 0
      try {
        const programId = await zz.getProgramId()
        const cluster = await zz.fetchCluster()
        const clusterId = cluster.id
        const selectedNodes = this.agGridApi.getSelectedNodes()
        this.teisToMoveCount = this.selectedTeis.length
        for (const currTei of this.selectedTeis) {
          // consider running in parallel
          const getResp = await axios.get(`${zz.apiBaseUrl}/enrollments`, {
            params: {
              program: programId,
              programStatus: 'ACTIVE', // status of the *enrollment* in the given program
              ou: clusterId,
              ouMode: 'DESCENDANTS',
              skipPaging: true,
              trackedEntityInstance: currTei,
            },
          })
          for (const currEnrol of getResp.data.enrollments) {
            try {
              await axios.put(
                `${zz.apiBaseUrl}/enrollments/${
                  currEnrol.enrollment
                }/completed`,
              )
            } catch (err) {
              throw zz.chainedError(
                err,
                `Failed to mark the enrollment='${
                  currEnrol.enrollment
                }' as completed`,
              )
            }
          }
          try {
            await axios.post(`${zz.apiBaseUrl}/enrollments`, {
              trackedEntityInstance: currTei,
              orgUnit: this.selectedOrgUnit,
              program: programId,
              enrollmentDate: today(),
            })
            const newCtc = (
              this.teisForMoveWithNoCtc.find((e) => e.id === currTei) || {}
            ).ctc
            await updateTeiDetails(currTei, this.selectedOrgUnit, newCtc)
          } catch (err) {
            // TODO more info about the failure might be useful for the user
            const fullName = this.rowData.find((e) => e.id === currTei).fullName
            this.moveTeiStatus = {singleFailure: true, msg: fullName}
            zz.consoleError(
              `Failed to move tei='${currTei}' to orgUnit='${
                this.selectedOrgUnit
              }'`,
              err,
            )
            return
          }
          this.movedTeiCount++
          // deselect as we go, so if there's an error, users won't try to re-move already moved ones
          selectedNodes.find((e) => e.data.id === currTei).setSelected(false)
        }
        await this.searchForTeis()
        this.selectedTeis = []
        this.teisForMoveWithNoCtc = []
        this.moveTeiStatus = 'success'
      } catch (err) {
        this.moveTeiStatus = 'failed'
        this.handleError(err, 'Failed to move TEIs')
      }
    },
    handleError(msg, error) {
      this.mixinLastDitchErrorHandler(msg, error)
      this.isError = true
    },
  },
}

function today() {
  const todayInUtc = moment()
    .toISOString()
    .substr(0, 10)
  return todayInUtc
}

async function updateTeiDetails(teiId, orgUnitId, newCtc) {
  try {
    const url = `${zz.apiBaseUrl}/trackedEntityInstances/${teiId}`
    const getResp = await axios.get(url, {
      params: {fields: '*'},
    })
    const body = getResp.data
    body.orgUnit = orgUnitId
    if (newCtc) {
      console.debug(`Setting new CTC='${newCtc}' for tei='${teiId}'`)
      const ctcAttrId = await zz.getCTCAttributeId()
      const existingCtcAttrIndex = body.attributes.findIndex(
        (e) => e.attribute === ctcAttrId,
      )
      const ctcAttribute = {attribute: ctcAttrId, value: newCtc}
      if (existingCtcAttrIndex < 0) {
        console.debug(`TEI='${teiId}' has no existing CTC, creating.`)
        body.attributes.push(ctcAttribute)
      } else {
        // not expecting to hit this case, as we only ask for CTC where none exists
        console.debug(`TEI='${teiId}' has an existing CTC, overwriting.`)
        body.attributes[existingCtcAttrIndex] = ctcAttribute
      }
    }
    await axios.put(url, body)
  } catch (err) {
    throw zz.chainedError(
      err,
      `Failed to update orgUnit to '${orgUnitId}' for tei='${teiId}'`,
    )
  }
}
