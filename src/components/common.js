import axios from 'axios'
const moment = require('moment')
const Rollbar = require('rollbar')
const isRollbarEnabled = process.env.NODE_ENV === 'production'
if (!isRollbarEnabled) {
  console.log(
    `Note: Rollbar is disabled for this environment=${process.env.NODE_ENV}`,
  )
}

const rollbar = new Rollbar({
  accessToken: '9cf2a653299d423b87ed002e7e6dd1bc', // uses post_client_item token
  environment: '%%ROLLBAR_ENV%%', // replace in built binary before deploy. It's clunky but works
  captureUncaught: true,
  captureUnhandledRejections: true,
  enabled: isRollbarEnabled,
})

export const orgUnitsChangedEvent = 'orgunits-changed'
export const dataEntryTemplateVersion = 'de-1' // bump every time you make a breaking change to the template
export const empGroupTemplateVersion = 'eg-1' // bump every time you make a breaking change to the template
export const apiBaseUrl = '/api/29'
export const empGroupFormTemplateName = 'NACOPHA-empGroup-mgmt-form-template'
export const headerIdField = 'id'
export const headerTitleField = 'title'
export const fullNameColHeader = 'full-name'
export const fullNameType = 'fullName'
export const dateType = 'DATE'
export const userMgmtOuPreselectQueryParamName = 'preselectSearchOu'
export const userMgmtRoute = '/user-mgmt'
export const asciiCapitalA = 65
export const dataEntryRouteName = 'DE'
export const selectedProgramIdKey = 'nacopha_selected_program_id'
const usernameKey = 'username'
const selectedClusterIdKey = 'nacopha_selected_cluster_id'
const dataStoreBaseUrl = `${apiBaseUrl}/dataStore/nacopha`
const fullNameAttrIdUrl = `${dataStoreBaseUrl}/fullNameAttrId`
const dobAttrIdUrl = `${dataStoreBaseUrl}/dobAttrId`
const nacophaIdAttrIdUrl = `${dataStoreBaseUrl}/nacophaIdAttrId`
const ctcAttrIdUrl = `${dataStoreBaseUrl}/ctcAttrId`
const villageAttrIdUrl = `${dataStoreBaseUrl}/villageAttrId`
const groupRegoNumAttrIdUrl = `${dataStoreBaseUrl}/groupRegoNumAttrId`
const economicGroupAttrIdUrl = `${dataStoreBaseUrl}/economicGroupAttrId`
const socialGroupAttrIdUrl = `${dataStoreBaseUrl}/socialGroupAttrId`
const communityHivGroupAttrIdUrl = `${dataStoreBaseUrl}/communityHivGroupAttrId`
const zoneLabelPrefixUrl = `${dataStoreBaseUrl}/zoneLabelPrefix`
const maxPeriodsUrl = `${dataStoreBaseUrl}/maxPeriods`
const genderAttrIdUrl = `${dataStoreBaseUrl}/genderAttrId`
const personTeiTypeIdUrl = `${dataStoreBaseUrl}/personTeiTypeId`
const formDosElementIdUrlPrefix = `${dataStoreBaseUrl}/formDosElementId`
const yesConstantIdUrl = `${dataStoreBaseUrl}/yesConstantId`
const noConstantIdUrl = `${dataStoreBaseUrl}/noConstantId`
const programIdUrl = `${dataStoreBaseUrl}/programId`
const orgUnitGroupUrl = `${dataStoreBaseUrl}/orgUnitGroupId`
const maxFormCountUrl = `${dataStoreBaseUrl}/maxFormCount`
let cache = {}
const libreCalcDateMagicOffset = 2 // it just works

// FIXME should these orgUnit levels be server-side?
const regionLevel = 1
const zonalLevel = 2
const clusterLevel = 3
const wardLevel = 4
const empGroupLevel = 5

export const teiMappingModes = {
  everyone: 'everyone',
  withCtcOnly: 'withCtc',
  withoutCtcOnly: 'withoutCtc',
}

export function resetCache() {
  console.debug(`Purging cache`)
  // keep this, as a best effort to add username to Rollbar error messages
  const username = cache[usernameKey] || '(username not set)'
  cache = {}
  cache[usernameKey] = username
}

export const markers = {
  hiddenConfig: '!HIDDENCONFIG',
  dataStart: '!DATASTART',
  dataEnd: '!DATAEND',
  zone: '!ZONE',
  region: '!REGION',
  district: '!DISTRICT',
  cluster: '!CLUSTER',
  ward: '!WARD',
  empGroup: '!EG',
  period: '!PERIOD',
  village: '!VILLAGE',
  groupRegoNum: '!GROUP_REG_NUM',
  activities: {
    economic: '!ACT_ECO',
    communityHiv: '!ACT_COMHIV',
    social: '!ACT_SOC',
  },
  check: '!CHECK_ABOVE',
}

export const keys = {
  orgUnit: 'orgUnit',
  fullNameLookup: 'fullNameLookup',
  templateVersion: 'templateVersion',
  checkValueCellAddress: 'checkValueCellAddress',
}

export const errorCodes = (function() {
  return ['CHNV01'].reduce((accum, curr) => {
    accum[curr] = curr
  }, {})
})()

export function rollbarLog(msg, data) {
  rollbar.log(msg, null, data)
}

export function rollbarError(msg, err, data) {
  const username = cache[usernameKey] || '(username not set)'
  const msgWithUsername = `[DHIS username=${username}] ${msg}`
  rollbar.error(msgWithUsername, err, data)
}

export function rollbarWarn(msg, data) {
  rollbar.warn(msg, null, data)
}

export function consoleError(msg, err) {
  const initialMsgLength = (msg || '').length
  if (err) {
    if (err.config && err.config.data) {
      msg += `\n\n  HTTP request body=${err.config.data}`
    }
    if (err.config && err.config.url) {
      msg += `\n\n  HTTP request url=${err.config.url}`
    }
    if (err.config && err.config.headers) {
      msg += `\n\n  HTTP request headers=${JSON.stringify(err.config.headers)}`
    }
    if (err.response && err.response.data) {
      msg += `\n\n  HTTP response=${JSON.stringify(err.response.data)}`
    }
    if (msg.length > initialMsgLength) {
      msg += '\n\n'
    }
  }
  console.error(msg, err)
  rollbarError(msg, err)
}

export function consoleWarn(msg) {
  console.warn(msg)
  rollbarWarn(msg)
}

export function consoleLog(msg) {
  console.log(msg)
  rollbarLog(msg)
}

export function logFailedAdminUnlock(password) {
  const username = cache[usernameKey] || '(username not set)'
  consoleLog(
    `Failed unlock attempt against /admin page from user='${username}', password='${password}'`,
  )
}

/**
 * Either return the cached value or trigger a refresh and return the
 * promise. Future calls will receieve the (already resolved) promise.
 *
 * Only intended to be called as a helper in this file. Don't await the
 * call, just return the promise and the caller of our caller will (probably)
 * await the promise.
 *
 * This is the preferred way to do caching but it was introduced late so it's
 * not yet used anywhere and we couldn't do the testing to implement it. So,
 * if you find yourself making changes, refactor to use this where you can.
 */
export async function fromCache(cacheKey, refreshFn) {
  if (cache[cacheKey]) {
    return cache[cacheKey]
  }
  // there may be a race condition here if there are two calls made for the same key but with different
  // params to the refreshFn.
  cache[cacheKey] = refreshFn().catch(err => {
    throw chainedError(
      err,
      `Failed while refreshing the cache entry for key='${cacheKey}',`,
    )
  })
  return cache[cacheKey]
}

export function triggerDownload(blob, filename) {
  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveOrOpenBlob(blob, filename)
    return
  }
  const url = window.URL.createObjectURL(blob)
  const a = document.createElement('a')
  document.body.appendChild(a)
  a.href = url
  a.download = filename
  a.click()
  window.URL.revokeObjectURL(url)
  document.body.removeChild(a)
}

export async function getTemplateBytes(templateNameFragment) {
  const currLocale = await getCurrLocale()
  return getTemplateBytesForLocale(templateNameFragment, currLocale)
}

export async function getTemplateBytesForLocale(templateNameFragment, locale) {
  const formTemplateName = buildFormTemplateName(templateNameFragment, locale)
  const getDocResp = await axios.get(`${apiBaseUrl}/documents`, {
    params: {
      filter: `displayName:eq:${formTemplateName}`,
    },
  })
  const docs = getDocResp.data.documents
  if (docs.length === 0) {
    throw new Error(
      `Failed to get form template. Could not find a document with name='${formTemplateName}'`,
    )
  }
  if (docs.length > 1) {
    rollbar.warn(
      `Expected 1 existing document to be found, but found ${
        docs.length
      }. Using the first!`,
    )
  }
  const docId = docs[0].id
  const templateFileResp = await axios.get(
    `${apiBaseUrl}/documents/${docId}/data`,
    {
      responseType: 'arraybuffer',
    },
  )
  return templateFileResp.data
}

export async function getCurrLocale() {
  try {
    const resp = await axios.get(`${apiBaseUrl}/userSettings/keyDbLocale`)
    return resp.data
  } catch (err) {
    if (err.response.status === 404) {
      return 'en'
    }
    throw chainedError(err, 'Failed to get user DB locale')
  }
}

export function buildFormTemplateName(filename, locale) {
  return `${filename}_${locale}`
}

export function concatHeaders(dataElementArray) {
  return dataElementArray.map(x => buildColHeaderFromDataElement(x))
}

export function buildColHeaderFromDataElement(dataElement) {
  const titleForDebugging = dataElement.formName || dataElement.displayName
  return buildColHeader(dataElement.id, titleForDebugging)
}

export function buildColHeader(id, title) {
  return {
    [headerIdField]: id,
    [headerTitleField]: title,
  }
}

async function _getSavedId(url, loggingFragment) {
  if (cache[url]) {
    const result = cache[url]
    console.debug(`Cache hit for key='${url}', returning='${result}'`)
    return result
  }
  console.debug(`Cache miss for key='${url}', fetching new value`)
  cache[url] = getValue()
  return cache[url]
  async function getValue() {
    try {
      const resp = await axios.get(url)
      const result = deserialise(resp.data)
      console.debug(`Cache updated for key='${url}', new value='${result}'`)
      return result
    } catch (err) {
      if (err.response && err.response.status === 404) {
        // can't explode because we need to support first load of the admin page
        console.warn(
          `Didn't find a value for '${loggingFragment}' at url='${url}'. Things are probably about to break.`,
        )
        return null
      }
      throw chainedError(
        err,
        `Failed to get ${loggingFragment} attribute/constant ID.`,
      )
    }
  }
}

export async function getGenderAttributeId() {
  return _getSavedId(genderAttrIdUrl, 'gender')
}

export async function getFullNameAttributeId() {
  return _getSavedId(fullNameAttrIdUrl, 'full name')
}

export async function getDobAttributeId() {
  return _getSavedId(dobAttrIdUrl, 'DOB')
}

export async function getNacophaIdAttributeId() {
  return _getSavedId(nacophaIdAttrIdUrl, '"NACOPHA ID"')
}

export async function getCTCAttributeId() {
  return _getSavedId(ctcAttrIdUrl, '"CTC"')
}

export async function getVillageAttrId() {
  return _getSavedId(villageAttrIdUrl, 'village')
}

export async function getGroupRegoNumAttrId() {
  return _getSavedId(groupRegoNumAttrIdUrl, '"group registration number"')
}

export async function getEconomicGroupAttrId(groupNum) {
  return _getSavedId(
    economicGroupAttrIdUrl + groupNum,
    `"economic group ${groupNum}"`,
  )
}

export async function getSocialGroupAttrId(groupNum) {
  return _getSavedId(
    socialGroupAttrIdUrl + groupNum,
    `"social group ${groupNum}"`,
  )
}

export async function getCommunityHivGroupAttrId(groupNum) {
  return _getSavedId(
    communityHivGroupAttrIdUrl + groupNum,
    `"community HIV group ${groupNum}"`,
  )
}

export async function getPersonTeiTypeId() {
  return _getSavedId(personTeiTypeIdUrl, 'person TEI type')
}

export async function getMaxFormCount() {
  const defaultFormCount = 3
  const result = await _getSavedId(maxFormCountUrl, 'max form count')
  return result || defaultFormCount
}

/**
 * Gets the currently selected program. This could be Sauti Yetu or one of the generic programs.
 * If no program has been selected, we'll return Sauti Yetu as the default.
 */
export async function getProgramId() {
  const programList = await getProgramList()
  return localStorage.getItem(selectedProgramIdKey) || programList.defaultItemId
}

export async function getSautiYetuProgramId() {
  return _getSavedId(programIdUrl, 'Sauti Yetu data entry program')
}

export async function getOrgUnitGroupId() {
  return _getSavedId(
    orgUnitGroupUrl,
    '"the orgUnit group for empowerment groups"',
  )
}

export async function getZoneLabelPrefix() {
  return _getSavedId(zoneLabelPrefixUrl, '"zone label prefix"')
}

function buildDosUrl(programId, formId) {
  return `${formDosElementIdUrlPrefix}_${programId}_${formId}`
}

export async function getFormDosId(programId, formId) {
  if (!formId) {
    die(
      `refusing to get form date of service ID for program='${programId}', formId='${formId}' ` +
        'due to invalid formId.',
    )
  }
  const url = buildDosUrl(programId, formId)
  return _getSavedId(
    url,
    `"program ${programId} form ${formId} date of service"`,
  )
}

export async function getYesConstantId() {
  return _getSavedId(yesConstantIdUrl, '"yes"')
}

export async function getNoConstantId() {
  return _getSavedId(noConstantIdUrl, '"no"')
}

export async function getMaxReportingPeriods() {
  return _getSavedId(maxPeriodsUrl, '"max reporting periods"')
}

async function save(val, label, url) {
  console.debug(`Setting ${label}='${val}'`)
  const body = serialise(val)
  try {
    cache = {}
    await axios.post(url, body)
  } catch (err) {
    if (err.response.status === 409) {
      try {
        console.debug(
          `initial save attempt for ${label} failed, trying harder!`,
        )
        await axios.put(url, body)
        console.debug(`successfully saved ${label}`)
        return
      } catch (err2) {
        throw chainedError(
          err2,
          `Failed to save (PUT) ${label} attribute/constant ID.`,
        )
      }
    }
    throw chainedError(
      err,
      `Failed to save (POST) ${label} attribute/constant ID.`,
    )
  }
}

export async function saveFullName(val) {
  return save(val, 'full name', fullNameAttrIdUrl)
}

export async function saveGender(val) {
  return save(val, 'gender', genderAttrIdUrl)
}

export async function saveDob(val) {
  return save(val, 'DOB', dobAttrIdUrl)
}

export async function saveNacophaId(val) {
  return save(val, 'NACOPHA ID', nacophaIdAttrIdUrl)
}

export async function saveCtcId(val) {
  return save(val, 'CTC', ctcAttrIdUrl)
}

export async function saveVillageAttrId(val) {
  return save(val, 'village', villageAttrIdUrl)
}

export async function saveGroupRegoNumAttrId(val) {
  return save(val, '"group registration number"', groupRegoNumAttrIdUrl)
}

export async function saveEconomicGroupAttrId(groupNum, val) {
  return save(
    val,
    `"economic group ${groupNum}"`,
    economicGroupAttrIdUrl + groupNum,
  )
}

export async function saveSocialGroupAttrId(groupNum, val) {
  return save(
    val,
    `"social group ${groupNum}"`,
    socialGroupAttrIdUrl + groupNum,
  )
}

export async function saveCommunityHivGroupAttrId(groupNum, val) {
  return save(
    val,
    `"community HIV group ${groupNum}"`,
    communityHivGroupAttrIdUrl + groupNum,
  )
}

export async function savePersonTeiType(val) {
  return save(val, 'person TEI type', personTeiTypeIdUrl)
}

export async function saveSautiYetuProgramId(val) {
  return save(val, 'Sauti Yetu data entry program', programIdUrl)
}

export async function saveOrgUnitGroupId(val) {
  return save(val, 'the orgUnit group for empowerment groups', orgUnitGroupUrl)
}

export async function saveMaxFormCount(val) {
  return save(val, 'max form count', maxFormCountUrl)
}

export async function saveFormDosId(programId, formId, val) {
  const url = buildDosUrl(programId, formId)
  return save(val, `"Program ${programId} Form ${formId} date of service"`, url)
}

export async function saveYesId(val) {
  return save(val, '"yes"', yesConstantIdUrl)
}

export async function saveNoId(val) {
  return save(val, '"no"', noConstantIdUrl)
}

export async function saveZoneLabelPrefix(val) {
  return save(val, '"zone label prefix"', zoneLabelPrefixUrl)
}

export async function saveMaxReportingPeriods(val) {
  const parsedVal = parseInt(val)
  if (isNaN(parsedVal)) {
    throw new Error(
      `Supplied val='${val}' is not a number, cannot save as max reporting periods!`,
    )
  }
  return save(val, '"max reporting periods"', maxPeriodsUrl)
}

function serialise(val) {
  return { value: val }
}

function deserialise(obj) {
  return obj.value
}

async function _fetchAttributeOptions(attributeId) {
  const resp = await axios.get(`${apiBaseUrl}/attributes/${attributeId}`, {
    params: {
      fields: `
          id,
          displayName,
          valueType,
          optionSet[
            options[
              code,
              displayName
            ]
          ]`.replace(/\s/g, ''),
    },
  })
  const r = resp.data
  r.valueType === 'TEXT' ||
    die(
      `Expected attribute ${r.displayName} (${
        r.id
      }) to be valueType=TEXT but instead found '${r.valueType}'`,
    )
  !!(r.optionSet && r.optionSet.options && r.optionSet.options.length) ||
    die(`Attribute ${r.displayName} (${r.id}) has no options available`)
  return r.optionSet.options
}

export function findMarkerRow(sheet, marker) {
  sheet || die(`No 'sheet' param passed, cannot continue`)
  const found = sheet.find(marker)
  if (found.length !== 1) {
    throw new Error(
      `Failed to find marker '${marker}' on sheet '${sheet.name()}'. Expected exactly 1 instance, found ${
        found.length
      }.`,
    )
  }
  const theCell = found[0]
  return theCell.rowNumber()
}

export function findMarkerColNumber(sheet, marker) {
  const found = sheet.find(marker)
  if (found.length !== 1) {
    throw new Error(
      `Failed to find marker '${marker}' on sheet '${sheet.name()}'. Expected exactly 1 instance, found ${
        found.length
      }.`,
    )
  }
  const theCell = found[0]
  return theCell.columnNumber()
}

export async function getTeiRecords(orgUnit, ouMode, filter, fields) {
  const fieldsParams =
    fields ||
    `
    trackedEntityInstance,
    enrollments[
      enrollmentDate
    ],
    attributes[
      attribute,
      value,
      displayName,
    ]`.replace(/\s/g, '')
  let params
  try {
    const programId = await getProgramId()
    params = {
      ou: orgUnit,
      ouMode,
      program: programId,
      programStatus: 'ACTIVE',
      fields: fieldsParams,
      filter,
      paging: false, // we want all the names
    }
    const teisResp = await axios.get(`${apiBaseUrl}/trackedEntityInstances`, {
      params,
    })
    return teisResp.data.trackedEntityInstances
  } catch (err) {
    throw chainedError(
      err,
      `Failed to get TEIs with params='${JSON.stringify(params)}'`,
    )
  }
}

async function getTeiMapping(orgUnitId, teiMappingMode) {
  Object.values(teiMappingModes).indexOf(teiMappingMode) > -1 ||
    die(`invalid teiMappingMode='${teiMappingMode}'`)
  const ctcAttributeId = await _mockable.getCTCAttributeId()
  let assertCtcPresent
  let teis
  if (teiMappingModes.everyone === teiMappingMode) {
    assertCtcPresent = false
    const ouMode = 'DESCENDANTS'
    teis = await _mockable.getTeiRecords(orgUnitId, ouMode)
  } else {
    const isWithCtc = teiMappingModes.withCtcOnly === teiMappingMode
    let filter = ''
    assertCtcPresent = false
    if (isWithCtc) {
      filter = `${ctcAttributeId}:ne:""`
      assertCtcPresent = true
    }
    const isIncludeWard = isWithCtc // currently they're determined the same way, also assuming orgUnit is a ward
    const ouMode = isIncludeWard ? 'DESCENDANTS' : 'CHILDREN' // TODO should CHILDREN be SELECTED? No time to investigate
    teis = await _mockable.getTeiRecords(orgUnitId, ouMode, filter)
    if (!isWithCtc) {
      // TODO make more efficient. Can't figure out how to filter at query time for TEIs that
      // *don't* have the CTC attribute, so we need to get everything, and filter it client-side.
      teis = teis.filter(
        e => !e.attributes.find(y => y.attribute === ctcAttributeId),
      )
    }
  }
  const fullNameAttributeId = await _mockable.getFullNameAttributeId()
  const dobAttributeId = await _mockable.getDobAttributeId()
  const genderResolver = await getGenderLabelResolver()
  const result = teis.reduce((accum, curr) => {
    const trackedEntityId = curr.trackedEntityInstance
    const fullName = getAttributeValue(
      curr.attributes,
      fullNameAttributeId,
      '(No name)',
    )
    const dob = getAttributeValue(curr.attributes, dobAttributeId, '(No DOB)')
    const gender = genderResolver(curr.attributes)
    const ctc = getAttributeValue(curr.attributes, ctcAttributeId, null)
    if (!ctc && assertCtcPresent) {
      throw new Error(
        `Data problem: expected TEI='${trackedEntityId}' to have a CTC='${ctc}', but it doesn't. ` +
          `Cannot continue as we won't be able to reliably process the data on upload.`,
      )
    }
    const coreLabel = `${dob}; ${gender}`
    if (ctc) {
      const labelNameFirst = `${fullName}; ${ctc}; ${coreLabel}`
      addEntryNoClobber(accum, labelNameFirst, trackedEntityId)
      const labelCtcFirst = `${ctc}; ${fullName}; ${coreLabel}`
      addEntryNoClobber(accum, labelCtcFirst, trackedEntityId)
    } else {
      const labelNameFirst = `${fullName}; ${coreLabel}`
      addEntryNoClobber(accum, labelNameFirst, trackedEntityId)
    }
    return accum
  }, {})
  return result
  /**
   * add the entry to accumulator but ensure the key is unique so we don't clobber anything.
   */
  function addEntryNoClobber(accum, key, value) {
    if (!key) {
      throw new Error(`Received invalid looking key='${key}'`)
    }
    let uniqueKey = key
    while (accum[uniqueKey]) {
      uniqueKey += ' '
    }
    accum[uniqueKey] = value
  }
}

async function getGenderLabelResolver() {
  const genderList = await _mockable.fetchGenders()
  const genderMap = genderList.reduce((accum, curr) => {
    accum[curr.code] = curr.displayName
    return accum
  }, {})
  const genderAttributeId = await _mockable.getGenderAttributeId()
  return function(attributes) {
    const genderCode = getAttributeValue(attributes, genderAttributeId, null)
    const result = genderMap[genderCode]
    if (!genderCode || !result) {
      return '(No gender)'
    }
    return result
  }
}

function getAttributeValue(attributes, attrId, defaultValue) {
  const theAttr = attributes.find(y => y.attribute === attrId)
  if (!theAttr) {
    return defaultValue
  }
  return theAttr.value
}

function formRequiresTeisWithCtcs(formId) {
  if (!formId) {
    throw new Error(`Form ID should be an integer >= 1, but was '${formId}'`)
  }
  const val = parseInt(formId)
  if (val < 1) {
    throw new Error(
      `Form ID should be an integer >= 1, but was parsed value='${val}'`,
    )
  }
  return val > 1
}

export const _mockable = {
  fetchActivityGroupVocab: _fetchActivityGroupVocab,
  fetchAttributeOptions: _fetchAttributeOptions,
  fetchGenders: _fetchGenders,
  fetchGroupActivityAttributeIds: _fetchGroupActivityAttributeIds,
  fetchProgramStartDate,
  getCTCAttributeId,
  getDobAttributeId,
  getFullNameAttributeId,
  getGenderAttributeId,
  getMaxReportingPeriods,
  _getNoConstantDisplayName,
  _getYesConstantDisplayName,
  getTeiRecords,
}
export const fetchGroupActivityAttributeIds = async function() {
  return _mockable.fetchGroupActivityAttributeIds()
}
export const fetchActivityGroupVocab = async function() {
  return _mockable.fetchActivityGroupVocab()
}
export const fetchGenders = async function() {
  return _mockable.fetchGenders()
}
export const fetchAttributeOptions = async function(attributeId) {
  return _mockable.fetchAttributeOptions(attributeId)
}
export const getYesConstantDisplayName = async function() {
  return _mockable._getYesConstantDisplayName()
}
export const getNoConstantDisplayName = async function() {
  return _mockable._getNoConstantDisplayName()
}

export const commonMixin = {
  name: 'Common Mixin',
  methods: {
    mixinGetColTypeStrategy(colType, hasOptions) {
      const colTypeStrategies = this._buildColTypeStrategyMap()
      const vocabness = hasOptions ? 'vocab' : 'novocab'
      const keyWithOptionsFlag = `${colType}_${vocabness}`
      const result = colTypeStrategies[keyWithOptionsFlag]
      if (result) {
        return result
      }
      // fallback to not using the hasOptions modifier
      return colTypeStrategies[colType]
    },
    _buildColTypeStrategyMap() {
      const notApplicable = () => {
        return null
      }
      const result = {}
      const justUseTheValue = {
        getValues: notApplicable,
        displayToValue: rawValue => {
          return rawValue
        },
        valueToDisplay: async (colDef, value) => {
          return value
        },
      }
      const dateMapper = {
        getValues: notApplicable,
        displayToValue: rawValue => {
          const dateObj = getJsDateFromExcel(rawValue)
          const momentDate = moment(dateObj)
          if (!momentDate.isValid()) {
            throw new Error(
              `Moment.js tells us that the supplied date='${rawValue}' is NOT valid`,
            )
          }
          const result = momentDate.format('YYYY-MM-DD')
          return result
        },
        valueToDisplay: async (colDef, value) => {
          const libreCalcDay1 = moment('1900-01-01', 'YYYY-MM-DD')
          const momentDate = moment.utc(value, 'YYYY-MM-DD')
          const result = momentDate.diff(libreCalcDay1, 'days')
          return result + libreCalcDateMagicOffset
        },
      }
      result.BOOLEAN = {
        getValues: this._booleanGetValuesStrategy,
        displayToValue: async rawValue => {
          const yesLabel = await _mockable._getYesConstantDisplayName()
          const noLabel = await _mockable._getNoConstantDisplayName()
          if (yesLabel === rawValue) {
            return true
          }
          if (noLabel === rawValue) {
            return false
          }
          return null
        },
        valueToDisplay: async (colDef, value) => {
          const yesLabel = await _mockable._getYesConstantDisplayName()
          const noLabel = await _mockable._getNoConstantDisplayName()
          if (value === 'true') {
            return yesLabel
          }
          if (value === 'false') {
            return noLabel
          }
          return null
        },
      }
      result.TEXT_vocab = {
        getValues: this._textGetValuesStrategy,
        displayToValue: (rawValue, options) => {
          if (!rawValue) {
            return rawValue
          }
          const result = options[rawValue]
          if (!result) {
            throw new Error(
              `Data problem: expected to be able to resolve rawValue='${rawValue}' ` +
                `from available options='${JSON.stringify(
                  options,
                )}' but couldn't.`,
            )
            // FIXME set flag so UI knows it's a data issue
          }
          return result
        },
        valueToDisplay: async (colDef, value) => {
          if (!value) {
            return value
          }
          colDef ||
            die(
              `No column definition passed for TEXT_vocab lookup for value='${value}'`,
            )
          const opts =
            (colDef.optionSet && colDef.optionSet.options) ||
            die(`no options on colDef id='${colDef.id}'`)
          const optDef = opts.find(e => e.code === value)
          if (!optDef) {
            throw new Error(
              `No option with code='${value}' found in options='${JSON.stringify(
                opts,
              )}'`,
            )
          }
          return optDef.displayName
        },
      }
      result.TEXT_novocab = justUseTheValue
      result[fullNameType] = {
        getValues: this._fullNameValidation,
        displayToValue: async (
          rawValue,
          embeddedTeiMapping,
          orgUnitId,
          formId,
        ) => {
          const teiId = embeddedTeiMapping[rawValue]
          if (!teiId) {
            const err = new Error(
              `Validation failure! Could not find a TEI ID for full name='${rawValue}' in embedded ` +
                `name list=${JSON.stringify(embeddedTeiMapping, null, 2)}`,
            )
            // don't set err.nacophaMsg, just use the default
            throw err
          }
          const serverTeiMapping = await this.mixinGetFullNameMapping(
            orgUnitId,
            formId,
          )
          const isTeiStillInServerList =
            Object.values(serverTeiMapping).indexOf(teiId) >= 0
          if (!isTeiStillInServerList) {
            const err = new Error(
              `Validation failure! TEI ID='${teiId}' for full name='${rawValue}' is in embedded ` +
                `name list but *not* in server name list=${JSON.stringify(
                  serverTeiMapping,
                  null,
                  2,
                )}`,
            )
            err.nacophaMsg = this.$i18n.t('de_full_name_fail_server')
            throw err
          }
          return teiId
        },
        valueToDisplay: async (colDef, value) => {
          throw new Error('Programmer error: do not call this')
        },
      }
      result[dateType] = dateMapper
      result.AGE = dateMapper
      result.NUMBER = justUseTheValue // FIXME - do some checking here
      result.PHONE_NUMBER = justUseTheValue
      return result
    },
    async _booleanGetValuesStrategy() {
      const booleanTruthy = await _getYesConstantDisplayName()
      const booleanFalsy = await _getNoConstantDisplayName()
      return this._buildValidation([booleanTruthy, booleanFalsy])
    },
    _textGetValuesStrategy(options) {
      const colDef = options.colDef
      const hasOptions =
        colDef.optionSet &&
        colDef.optionSet.options &&
        colDef.optionSet.options.length > 0
      if (!hasOptions) {
        throw new Error(
          `Expected option set to exist as this question specifies it needs them, instead found='${
            colDef.optionSet
          }'`,
        )
      }
      const items = colDef.optionSet.options.map(x => x.displayName)
      return this._buildValidation(items)
    },
    async mixinGetFullNameMapping(orgUnitId, formId) {
      const teiMappingMode = await determineTeiMappingMode(formId)
      const cacheKey = `fullNameItems_${teiMappingMode}_${orgUnitId}`
      if (!cache[cacheKey]) {
        const result = await getTeiMapping(orgUnitId, teiMappingMode)
        cache[cacheKey] = result
      }
      return cache[cacheKey]
    },
    async _fullNameValidation(options) {
      const orgUnitId = options.orgUnitId
      const formId = options.formId
      const teiMapping = await this.mixinGetFullNameMapping(orgUnitId, formId)
      const result = Object.keys(teiMapping)
      result.sort()
      return this._buildValidation(result)
    },
    _buildValidation(itemArray) {
      const formula1 = itemArray.map(x => `"${x}"`).join(',')
      const validationObj = {
        type: 'list',
        allowBlank: true,
        showErrorMessage: true,
        error: this.$i18n.t('value_cant_be_blank'),
        errorTitle: this.$i18n.t('invalid_selection'),
        operator: 'String',
        formula1: formula1,
        formula2: 'String',
      }
      return {
        itemArray,
        validationObj,
      }
    },
    async mixinBuildValidationForAttribute(attributeId) {
      try {
        const options = await _mockable.fetchAttributeOptions(attributeId)
        const displayNames = options.map(e => e.displayName)
        const result = this._buildValidation(displayNames)
        return result.validationObj
      } catch (err) {
        throw chainedError(
          err,
          `Failed to build validation for attribute='${attributeId}'`,
        )
      }
    },
    mixinLastDitchErrorHandler(msg, error) {
      error = error || '(no cause error in chain)'
      consoleError(msg, error)
      alert(this.$i18n.t('last_ditch_error') + '\n' + msg)
    },
  },
}

async function _getYesConstantDisplayName() {
  const cacheKey = 'yesDisplayName'
  let result = cache[cacheKey]
  if (!result) {
    const yesId = await getYesConstantId()
    const constants = await getConstants()
    const element = constants.filter(e => yesId === e.id)
    if (!element || !element[0] || !element[0].displayName) {
      throw new Error(`Failed to lookup mapping for yes boolean value`)
    }
    result = element[0].displayName
    cache[cacheKey] = result
  }
  return result
}

async function _getNoConstantDisplayName() {
  const cacheKey = 'noDisplayName'
  let result = cache[cacheKey]
  if (!result) {
    const noId = await getNoConstantId()
    const constants = await getConstants()
    const element = constants.filter(e => noId === e.id)
    if (!element || !element[0] || !element[0].displayName) {
      throw new Error(`Failed to lookup mapping for no boolean value`)
    }
    result = element[0].displayName
    cache[cacheKey] = result
  }
  return result
}

export async function getConstants() {
  async function _fetchConstants() {
    try {
      const resp = await axios.get(`${apiBaseUrl}/constants`)
      return resp.data.constants
    } catch (err) {
      throw chainedError(err, 'Failed to get the list of constants.')
    }
  }
  if (!cache.constants) {
    cache.constants = _fetchConstants()
  }
  return cache.constants
}

function getJsDateFromExcel(excelDate) {
  // thanks https://gist.github.com/christopherscott/2782634
  return new Date(
    (excelDate - (25567 + libreCalcDateMagicOffset)) * 86400 * 1000,
  )
}

export function chainedError(err, msg) {
  err.message = `${msg}\nCaused by: ${err.message}`
  return err
}

export async function fetchWardsAndGroups() {
  const wardResp = await fetchWards()
  const result = wardResp.reduce((accum, currWard) => {
    currWard.isWard = true
    accum.push(currWard)
    const children = currWard.children.map(e => {
      return {
        id: e.id,
        displayName: concatWardAndEg(currWard.displayName, e.displayName),
      }
    })
    return accum.concat(children)
  }, [])
  result.sort(displayNameSorterFn)
  return result
}

export function concatWardAndEg(ward, eg) {
  return `${ward} - ${eg}`
}

export function displayNameSorterFn(a, b) {
  const adn = a.displayName.toUpperCase()
  const bdn = b.displayName.toUpperCase()
  if (adn < bdn) {
    return -1
  }
  if (adn > bdn) {
    return 1
  }
  return 0
}

async function _fetchGenders() {
  try {
    const genderAttrId = await getGenderAttributeId()
    const resp = await axios.get(
      `${apiBaseUrl}/trackedEntityAttributes/${genderAttrId}`,
      {
        params: {
          paging: false,
          fields: `
          optionSet[
            options[
              code,
              displayName
            ]
          ]`.replace(/\s/g, ''),
        },
      },
    )
    const optionSet = resp.data.optionSet
    if (!optionSet || !optionSet.options || !optionSet.options.length) {
      const optionSetStr = JSON.stringify(optionSet)
      const respBodyStr = JSON.stringify(resp.body)
      throw new Error(
        `Response was successful but no genders were returned:\noptionSet=${optionSetStr}\nresp.body=${respBodyStr}`,
      )
    }
    return optionSet.options
  } catch (err) {
    throw chainedError(err, 'Failed to get a list of genders.')
  }
}

export function writeHiddenValue(sheet, address, val) {
  writeHiddenValueRange(sheet, `${address}:${address}`, val)
}

export function writeHiddenValueRange(sheet, range, val) {
  let toWrite = val
  if (val.constructor === Array) {
    // all good
  } else if (typeof val !== 'string') {
    toWrite = JSON.stringify(val)
  }
  sheet
    .range(range)
    .value(toWrite)
    .style('fontColor', 'FFFFFF')
    .style('fontSize', 1)
}

/*
 * Use it as a one-liner to fail an assertion, like:
 *
 *   const thingy = obj['nonExistantField'] || die ('field is not there')
 *   const thingy2 = fnThatReturnsFalsy() || die('returned non-truthy')
 */
export function die(msg) {
  throw new Error(msg)
}

export function findMarkerCell(marker, sheet) {
  const rowNum = findMarkerRow(sheet, marker)
  const colNum = findMarkerColNumber(sheet, marker)
  return sheet.column(colNum).cell(rowNum)
}

export function writeValueForMarker(marker, value, sheet) {
  findMarkerCell(marker, sheet).value(value)
}

async function _fetchMyDetails() {
  return fromCache('_fetchMyDetails', async () => {
    try {
      const resp = await axios.get(`${apiBaseUrl}/me`, {
        params: {
          paging: false,
          // This query is crazy! We have to support 3 cases:
          //  1. user is assigned to cluster level (normal): so we need to go two
          //     levels up (district/zone and region) and two down (ward and empowerment group)
          //  2. user is at district/zone level: so we go one up and three down
          //  3. user is at region level: so we go four down
          // We're lucky the API just ignores bit of the query that don't return anything so we
          // can stuff all possibilities into one query :D
          fields: `
            id,
            displayName,
            userCredentials[
              username
            ],
            organisationUnits[
              id,
              displayName,
              level,
              children[
                id,
                displayName,
                level,
                children[
                  id,
                  displayName,
                  level,
                  attributeValues[
                    value,
                    attribute[
                      id
                  ]],
                  children[
                    id,
                    displayName,
                    level,
                    attributeValues[
                      value,
                      attribute[
                        id
                    ]],
                    children[
                      id,
                      displayName,
                      attributeValues[
                        value,
                        attribute[
                          id
                  ]]]],
                  parent[
                    id,
                    level,
                    displayName,
                    organisationUnitGroups[
                      id,
                      name,
                      displayName
                ]]],
                parent[
                  id,
                  level,
                  displayName,
                  organisationUnitGroups[
                    id,
                    name,
                    displayName
                ]],
              ],
              parent[
                id,
                level,
                displayName,
                organisationUnitGroups[
                  id,
                  name,
                  displayName
              ]
            ]`.replace(/\s/g, ''),
        },
      })
      return resp.data
    } catch (err) {
      throw chainedError(
        err,
        'Failed to fetch profile/details for current user',
      )
    }
  })
}

export async function fetchMyName() {
  try {
    const resp = await _fetchMyDetails()
    const username = (resp.userCredentials || {}).username
    cache[usernameKey] = username // it's a best effort to add username to Rollbar error messages
    return resp.displayName || username || resp.id
  } catch (err) {
    throw chainedError(err, 'Failed to get the displayName of the current user')
  }
}

/**
 * Get the org unit hierarchy of the current user.
 *
 * We need pieces of it in different places so we'll pull it all in one go
 * to improve performance. Plus, we can also centralise handling situations
 * like zonal users using the app and we can make them select a cluster that
 * they will "act as".
 */
async function _fetchMyOrgHierarchy() {
  try {
    const canUserSelecteAClusterToActAs = !(await isUserAtClusterLevel())
    if (canUserSelecteAClusterToActAs) {
      const result = await _getActingAsCluster()
      return result
    }
    const resp = await _fetchMyDetails()
    const orgUnits = resp.organisationUnits
    if (!orgUnits || orgUnits.length !== 1) {
      let msg = orgUnits
      if (orgUnits) {
        msg = orgUnits.map(e => {
          return { id: e.id, displayName: e.displayName, level: e.level }
        })
      }
      const username = await fetchMyName()
      throw new Error(
        `Current user ('${username}') is not configured correctly in DHIS, ` +
          `they must be assigned to exactly ONE cluster. Assigned orgUnits=${JSON.stringify(
            msg,
          )}`,
      )
    }
    const result = orgUnits[0]
    const myOrgUnitLevel = result.level
    if (myOrgUnitLevel > clusterLevel) {
      throw new Error(
        `Programmer error: we shouldn't be running here for level='${myOrgUnitLevel}'. ` +
          `Ward users, or lower in the org unit hierarchy, shouldn't exist!`,
      )
    }
    return result
  } catch (err) {
    throw chainedError(
      err,
      'Failed to fetch org unit hierarchy for current user',
    )
  }
}

async function _getActingAsCluster() {
  try {
    let selectedClusterId = _getSelectedClusterId()
    const clusterList = await fetchSuperuserAvailableClusters()
    const found = clusterList.find(e => e.id === selectedClusterId)
    if (!selectedClusterId || !found) {
      const firstCluster = clusterList[0]
      if (!firstCluster) {
        throw new Error(
          'User is a >cluster level but has no clusters under them, they cannot use this app until you' +
            'add at least one child cluster to their org unit',
        )
      }
      storeSelectedClusterId(firstCluster.id)
      selectedClusterId = _getSelectedClusterId()
    }
    const result = clusterList.find(e => e.id === selectedClusterId)
    if (!result) {
      throw new Error(
        `Could not find the cluster for id='${selectedClusterId}'`,
      )
    }
    return result
  } catch (err) {
    throw chainedError(
      err,
      'Failed to get the cluster that the current user is acting as',
    )
  }
}

/**
 * Set the cluster to "act as".
 * Only user's assigned higher than cluster level (so zonal or region)
 * will use this.
 */
export function storeSelectedClusterId(id) {
  localStorage.removeItem(selectedClusterIdKey)
  if (typeof id === 'undefined' || id === null) {
    return
  }
  localStorage.setItem(selectedClusterIdKey, id)
}

function _getSelectedClusterId() {
  return localStorage.getItem(selectedClusterIdKey)
}

/**
 * Get a list of clusters for a superuser to act as.
 */
export async function fetchSuperuserAvailableClusters() {
  try {
    const isUserCanOnlySeeTheirOwnCluster = await isUserAtClusterLevel()
    if (isUserCanOnlySeeTheirOwnCluster) {
      return [await fetchCluster()]
    }
    return await fromCache('fetchSuperuserAvailableClusters', async () => {
      const resp = await _fetchMyDetails()
      return parseResponseForFetchSuperuserAvailableClusters(resp)
    })
  } catch (err) {
    throw chainedError(
      err,
      'Failed to get list of available clusters for superuser',
    )
  }
}

const parseResponseStrategies = {
  [regionLevel]: resp => {
    return resp.organisationUnits.reduce((accum, curr) => {
      accum = accum.concat(curr.children)
      return accum
    }, [])
  },
  [zonalLevel]: resp => {
    return resp.organisationUnits
  },
}

function parseResponseForFetchSuperuserAvailableClusters(resp) {
  if (!resp.organisationUnits.length) {
    return []
  }
  const myOrgUnitLevel = resp.organisationUnits[0].level
  const strategy = parseResponseStrategies[myOrgUnitLevel]
  if (!strategy) {
    throw new Error(
      `Programmer error: we shouldn't be running here for level='${myOrgUnitLevel}'. ` +
        `Cluster users are handled and ward users shouldn't exist!`,
    )
  }
  const zones = strategy(resp)
  const result = zones.reduce((accum, currZone) => {
    const mappedClusters = currZone.children.map(e => {
      e.zoneName = currZone.displayName
      e.clusterName = e.displayName
      e.displayName = `${currZone.displayName} - ${e.displayName}`
      return e
    })
    accum = accum.concat(mappedClusters)
    return accum
  }, [])
  result.sort(displayNameSorterFn)
  return result
}

export async function fetchWards() {
  try {
    const resp = await _fetchMyOrgHierarchy()
    const result = resp.children
    result.sort(displayNameSorterFn)
    return result
  } catch (err) {
    throw chainedError(err, 'Failed to fetch the wards and empowerment groups')
  }
}

export async function fetchCluster() {
  try {
    const resp = await _fetchMyOrgHierarchy()
    return { id: resp.id, displayName: resp.displayName }
  } catch (err) {
    throw chainedError(err, 'Failed to get the current cluster')
  }
}

export async function getOrgUnitHierarchy() {
  try {
    const orgUnit = await _fetchMyOrgHierarchy()
    const zoneLabelPrefix = await getZoneLabelPrefix()
    const result = parseOrgUnitStructure(orgUnit, zoneLabelPrefix)
    return result
  } catch (err) {
    throw chainedError(
      err,
      `Failed to get org unit hierarchy from current user`,
    )
  }
}

export async function isUserAtClusterLevel() {
  try {
    const resp = await _fetchMyDetails()
    const result = resp.organisationUnits[0].level === clusterLevel
    if (result) {
      // clear previous selected ID from >cluster user login
      storeSelectedClusterId(null)
    }
    return result
  } catch (err) {
    throw chainedError(
      err,
      `Failed to get current user's org unit level when asserting cluster level`,
    )
  }
}

function parseOrgUnitStructure(rawHierarchy, zoneLabelPrefix) {
  const strategies = {
    2: function(result, curr) {
      result.region = curr.displayName
      const zoneOrgUnitGroup = curr.organisationUnitGroups.find(e =>
        e.name.startsWith(zoneLabelPrefix),
      )
      if (!zoneOrgUnitGroup) {
        return
      }
      result.zone = zoneOrgUnitGroup.displayName.replace(zoneLabelPrefix, '')
    },
    3: function(result, curr) {
      result.district = curr.displayName
      result.cluster = curr.displayName
    },
    4: function(result, curr) {
      result.ward = curr.displayName
    },
    5: function(result, curr) {
      result.empowermentGroupName = curr.displayName
    },
  }
  const result = {}
  let curr = rawHierarchy
  let isMoreParents
  do {
    const strat =
      strategies[curr.level] ||
      die(`no strategy found for level='${curr.level}'`)
    strat(result, curr)
    isMoreParents = curr.parent && curr.parent.level > 1
    curr = curr.parent
  } while (isMoreParents)
  return result
}

async function _fetchActivityGroupVocab() {
  try {
    const resp = await axios.get(`${apiBaseUrl}/attributes`, {
      params: {
        paging: false,
        fields: `
          id,
          displayName,
          valueType,
          optionSetValue,
          optionSet[
            options[
              code,
              displayName
            ]
          ]`.replace(/\s/g, ''),
      },
    })
    return resp.data.attributes.reduce((accum, curr) => {
      accum[curr.id] = curr
      return accum
    }, {})
  } catch (err) {
    throw chainedError(err, 'Failed to GET orgUnit activities')
  }
}

export function extractActivityGroup(relevantAttributeIds, vocab, values) {
  return relevantAttributeIds.map(a => {
    const matchingValue = values.find(r => r.attribute.id === a)
    if (!matchingValue) {
      return null
    }
    const rawValue = matchingValue.value
    const relevantVocab =
      vocab[a] || die(`Failed to find vocab for attribute id='${a}'`)
    const opts = relevantVocab.optionSet.options
    const matchingOption =
      opts.find(o => o.code === rawValue) ||
      die(`Could not find '${rawValue}' in options ${JSON.stringify(opts)}`)
    return matchingOption.displayName
  })
}

async function _fetchGroupActivityAttributeIds() {
  return {
    economic: await Promise.all([
      getEconomicGroupAttrId(1),
      getEconomicGroupAttrId(2),
      getEconomicGroupAttrId(3),
    ]),
    communityHiv: await Promise.all([
      getCommunityHivGroupAttrId(1),
      getCommunityHivGroupAttrId(2),
      getCommunityHivGroupAttrId(3),
    ]),
    social: await Promise.all([
      getSocialGroupAttrId(1),
      getSocialGroupAttrId(2),
      getSocialGroupAttrId(3),
    ]),
  }
}

/**
 * Generate an array of reporting periods starting from the start
 * date of the reporting program until the date provided, limited by
 * the max periods param.
 */
export async function generatePeriods(programId, untilDate, maxPeriods) {
  untilDate = untilDate || new Date()
  maxPeriods = maxPeriods || (await _mockable.getMaxReportingPeriods())
  if (!programId) {
    die('No programId was passed, cannot continue generating periods')
  }
  const result = []
  const includeCurrentMonth = 1
  try {
    const programStartDateStr = await _mockable.fetchProgramStartDate(programId)
    const programStartDate = moment(programStartDateStr)
    const periodCutOffDate = moment(untilDate).subtract(
      maxPeriods - includeCurrentMonth,
      'months',
    )
    let startDate = programStartDate
    if (startDate.isBefore(periodCutOffDate)) {
      startDate = periodCutOffDate
    }
    const currPeriod = moment(startDate)
    while (currPeriod.isSameOrBefore(untilDate, 'month')) {
      result.push(currPeriod.format('YYYY-MM'))
      currPeriod.add(1, 'months')
    }
    return result
  } catch (err) {
    throw chainedError(err, 'Failed to generate reporting periods')
  }
}

async function fetchProgramStartDate(programId) {
  return fromCache('fetchProgramStartDate', async () => {
    try {
      const resp = await axios.get(`${apiBaseUrl}/programs/${programId}`, {
        params: { fields: 'created' },
      })
      return resp.data.created
    } catch (err) {
      throw chainedError(err, `Failed to fetch program start date from server`)
    }
  })
}

export async function fetchDataEntryForms() {
  try {
    const programStageId = await getProgramStageId()
    const resp = await axios.get(
      `${apiBaseUrl}/programStages/${programStageId}`,
      {
        params: {
          fields: `
          programStageSections[
            id,
            displayName,
            dataElements[
              id,
              formName,
              displayName,
              valueType,
              optionSetValue,
              optionSet[
                id,
                options[
                  id,
                  code,
                  displayName
                ]
              ]
            ]
          ]`.replace(/\s/g, ''),
        },
      },
    )
    return resp.data.programStageSections.reduce((accum, curr) => {
      let formId = parseInt(curr.displayName.substr(0, 1))
      if (isNaN(formId)) {
        formId = 1
        consoleWarn(
          `Could not extract form number from '${curr.displayName}'. ` +
            `First character *must* be an integer! Defaulting to '1'.`,
        )
      }
      let formSections = accum[formId]
      if (!formSections) {
        formSections = []
        accum[formId] = formSections
      }
      formSections.push(curr)
      return accum
    }, {})
  } catch (err) {
    throw chainedError(
      err,
      'Failed to get a list of program stage (data entry form) questions.',
    )
  }
}

export async function fetchTeiTable(
  orgUnitId,
  orgUnitNameLookup /* [{id: 'string', displayName: 'string'}] */,
) {
  try {
    const resp = await axios.get(`${apiBaseUrl}/trackedEntityInstances`, {
      params: buildGetTeiParamObj({
        ou: orgUnitId,
        ouMode: 'DESCENDANTS',
        skipPaging: true,
      }),
    })
    return _produceTeiTable(
      resp.data.trackedEntityInstances,
      orgUnitNameLookup,
      function(values, e) {
        values.recordHealth = computeRecordHealth(e)
      },
    )
  } catch (err) {
    throw chainedError(err, 'Failed to fetch TEI table')
  }
}

/**
 * Builds a structure the same as fetchTeiTable() but from a list of TEI IDs
 */
export async function generateTeiTableFromIds(teiIds /* ['string'] */) {
  async function doGet(teiId) {
    const resp = await axios.get(
      `${apiBaseUrl}/trackedEntityInstances/${teiId}`,
      {
        params: buildGetTeiParamObj({}),
      },
    )
    return resp.data
  }
  const promises = teiIds.map(e => doGet(e))
  const teiRecords = await Promise.all(promises)
  const orgUnitNameLookup = await getWardNameLookupForEverywhere()
  return _produceTeiTable(teiRecords, orgUnitNameLookup)
}

async function getWardNameLookupForEverywhere() {
  const cacheKey = 'allWardNameLookup'
  if (cache[cacheKey]) {
    return cache[cacheKey]
  }
  try {
    const resp = await axios.get(`${apiBaseUrl}/organisationUnits`, {
      params: {
        filter: `level:eq:${wardLevel}`,
        paging: false,
        fields: `
          id,
          displayName,
          children[
            id,
            displayName
          ],
          parent[
            displayName
          ]`.replace(/\s/g, ''),
      },
    })
    const result = resp.data.organisationUnits.reduce((accum, currWard) => {
      const parentName = (
        currWard.parent || { displayName: 'ERROR no cluster name' }
      ).displayName
      const clusterAndWardName = `${parentName} - ${currWard.displayName}`
      accum.push({
        id: currWard.id,
        displayName: clusterAndWardName,
      })
      for (const currEg of currWard.children) {
        accum.push({
          id: currEg.id,
          displayName: clusterAndWardName, // yep, we want the ward name
        })
      }
      return accum
    }, [])
    cache[cacheKey] = result
    return cache[cacheKey]
  } catch (err) {
    throw chainedError(
      err,
      'Failed to build ward name lookup for all wards in system',
    )
  }
}

function buildGetTeiParamObj(yourParams) {
  const defaultParams = {
    fields: `
      trackedEntityInstance,
      attributes,
      orgUnit,
      enrollments[
        orgUnit,deleted,status,program
      ]`.replace(/\s/g, ''),
  }
  return Object.assign(defaultParams, yourParams)
}

async function _produceTeiTable(
  teiRecords,
  orgUnitNameLookup,
  extraRecordProcessorFn,
) {
  const genderAttrId = await getGenderAttributeId()
  const relevantAttributeIds = {
    [await getFullNameAttributeId()]: 'fullName',
    [await getDobAttributeId()]: 'dob',
    [await getCTCAttributeId()]: 'ctc',
    [genderAttrId]: 'gender',
    [await getNacophaIdAttributeId()]: 'nacophaId',
  }
  const genders = await fetchGenders()
  const result = teiRecords.map(e => {
    const relevantAttributes = e.attributes.filter(x =>
      Object.keys(relevantAttributeIds).find(y => y === x.attribute),
    )
    const values = relevantAttributes.reduce(
      getAttributeReducer(genders, relevantAttributeIds, genderAttrId),
      {},
    )
    values.id = e.trackedEntityInstance
    extraRecordProcessorFn && extraRecordProcessorFn(values, e)
    const empGroupName = (
      orgUnitNameLookup.find(x => x.id === e.orgUnit) || { displayName: '-' }
    ).displayName
    values.orgUnit = empGroupName
    return values
  })
  return result
}

function getAttributeReducer(genders, relevantAttributeIds, genderAttrId) {
  return function(accum, curr) {
    const key = relevantAttributeIds[curr.attribute]
    const isGender = curr.attribute === genderAttrId
    if (isGender) {
      accum[key] = genders.find(e => e.code === curr.value).displayName
    } else {
      accum[key] = curr.value
    }
    return accum
  }
}

/**
 * Computes the health of a TEI record, see NA-81 for history.
 */
function computeRecordHealth(record) {
  // shouldn't ever see status=ACTIVE and deleted=true. Deleting enrollment from UI yeilds status=CANCELLED and deleted=true
  const activeEnrollment = record.enrollments.find(
    e => e.status === 'ACTIVE' && !e.deleted,
  )
  if (!activeEnrollment) {
    return false
  }
  const isOrgUnitMatching = activeEnrollment.orgUnit === record.orgUnit
  // if we create more than 1 program, we'll need to check activeEnrollment.program too
  return isOrgUnitMatching
}

export function getCheckValueCellAddress(sheet) {
  const checkMarkerCell = findMarkerCell(markers.check, sheet)
  if (!checkMarkerCell) {
    throw new Error(
      `Programmer problem: Expected marker='${
        markers.check
      }' to be on form, but wasn't.`,
    )
  }
  checkMarkerCell.value('')
  const targetCellIsAbove = 1
  const row = checkMarkerCell.rowNumber() - targetCellIsAbove
  const result = `${checkMarkerCell.columnName()}${row}`
  return result
}

export async function getProgramList() {
  return fromCache('getProgramList', async () => {
    try {
      const resp = await axios.get(`${apiBaseUrl}/programs`, {
        params: {
          fields: 'id,displayName,programStages[id]',
          paging: false,
        },
      })
      const sautiYetuProgramId = await getSautiYetuProgramId()
      if (!resp.data.programs || !resp.data.programs.length) {
        throw new Error(
          `Cannot continue, expected programs in response, but instead got resp=${
            resp.data
          }`,
        )
      }
      const programs = resp.data.programs.map(e => {
        const stageCount = (e.programStages && e.programStages.length) || 0
        if (!stageCount) {
          throw new Error(
            `Program (ID='${e.id}', name='${
              e.displayName
            }') should have exactly 1 stage but found '${stageCount}'`,
          )
        }
        const stageId = e.programStages[0].id
        e.programStageId = stageId
        delete e.programStages
        return e
      })
      const sautiYetuProgram =
        programs.find(e => e.id === sautiYetuProgramId) ||
        die('Could not find Sauti Yetu program')
      const items = programs.filter(e => e.id !== sautiYetuProgramId)
      items.sort(displayNameSorterFn)
      items.unshift(sautiYetuProgram)
      return {
        items: items,
        defaultItemId: sautiYetuProgramId,
      }
    } catch (err) {
      throw chainedError(err, `Failed to get list of programs.`)
    }
  })
}

export async function getProgramName() {
  let programId
  try {
    programId = await getProgramId()
  } catch (err) {
    throw chainedError(
      err,
      `Failed to get current program ID in preparation for getting program name`,
    )
  }
  try {
    const resp = await getProgramList()
    const allPrograms = resp.items
    return (allPrograms.find(e => e.id === programId) || {}).displayName
  } catch (err) {
    throw chainedError(
      err,
      `Failed to resolve program name for ID=${programId}`,
    )
  }
}

export async function getProgramStageId() {
  return fromCache('getProgramStageId', async () => {
    let programId
    try {
      programId = await getProgramId()
    } catch (err) {
      throw chainedError(
        err,
        `Failed to get current program ID in preparation for getting stage ID`,
      )
    }
    try {
      const resp = await getProgramList()
      const allPrograms = resp.items
      return (allPrograms.find(e => e.id === programId) || {}).programStageId
    } catch (err) {
      throw chainedError(
        err,
        `Failed to resolve program stage ID for ID=${programId}`,
      )
    }
  })
}

export function buildDataEntryFormTemplateKey(programId) {
  return 'program_' + programId
}

export function buildDumpAllDataFormTemplateKey(programId) {
  return 'dump-all-data_' + programId
}

export function replaceWeirdCharacters(val) {
  return val.replace(/[^a-zA-Z0-9-_.]/g, '_')
}

export async function isSautiYetuProgram() {
  try {
    const [selectedProgramId, sautiYetuProgramId] = await Promise.all([
      getProgramId(),
      getSautiYetuProgramId(),
    ])
    return selectedProgramId === sautiYetuProgramId
  } catch (err) {
    throw chainedError(
      err,
      'Failed while trying to determine if Sauti Yetu is the current program',
    )
  }
}

/**
 * Determine how we should populate TEIs (PLHIV) on a given form.
 * For Sauti Yetu, we need to take with/without CTC in account,
 * but for generic programs we want everyone on all forms.
 */
export async function determineTeiMappingMode(formId) {
  const isSautiYetu = await isSautiYetuProgram()
  const isWithCtc = formRequiresTeisWithCtcs(formId)
  const key1 = isSautiYetu ? 'sy' : '!sy'
  const key2 = isWithCtc ? 'ctc' : 'noctc'
  const lookup = {
    'sy|ctc': teiMappingModes.withCtcOnly,
    'sy|noctc': teiMappingModes.withoutCtcOnly,
    '!sy|ctc': teiMappingModes.everyone,
    '!sy|noctc': teiMappingModes.everyone,
  }
  const lookupKey = `${key1}|${key2}`
  const result =
    lookup[lookupKey] ||
    die(`Programmer error: unknown situation '${lookupKey}'`)
  console.debug(`TEI mapping mode for form ID='${formId}' is '${result}'`)
  return result
}

async function _fetchCluserAndZoneNameLookup() {
  return fromCache('_fetchCluserAndZoneNameLookup', async () => {
    try {
      const resp = await axios.get(`${apiBaseUrl}/organisationUnits`, {
        params: {
          paging: false,
          fields: `
            id,
            displayName,
            parent[
              displayName
            ]`.replace(/\s/g, ''),
          filter: `level:eq:${clusterLevel}`,
        },
      })
      return resp.data.organisationUnits.reduce((accum, curr) => {
        accum[curr.id] = {
          cluster: curr.displayName,
          zone: curr.parent.displayName,
        }
        return accum
      }, {})
    } catch (err) {
      throw chainedError(err, 'Failed to GET cluster/zone name lookup data')
    }
  })
}

export async function lookupZoneNameByClusterId(clusterId) {
  const lookup = await _fetchCluserAndZoneNameLookup()
  const found = lookup[clusterId]
  if (!found) {
    return null
  }
  return found.zone
}

export async function lookupClusterName(clusterId) {
  const lookup = await _fetchCluserAndZoneNameLookup()
  const found = lookup[clusterId]
  if (!found) {
    return null
  }
  return found.cluster
}

async function _fetchWardNameLookup() {
  return fromCache('_fetchWardNameLookup', async () => {
    try {
      const resp = await axios.get(`${apiBaseUrl}/organisationUnits`, {
        params: {
          paging: false,
          fields: `
            id,
            parent[
              displayName
            ]`.replace(/\s/g, ''),
          filter: `level:eq:${empGroupLevel}`,
        },
      })
      return resp.data.organisationUnits.reduce((accum, curr) => {
        accum[curr.id] = curr.parent.displayName
        return accum
      }, {})
    } catch (err) {
      throw chainedError(err, 'Failed to GET ward name lookup data')
    }
  })
}

export async function lookupWardNameByEmpGroupId(empGroupId) {
  if (!empGroupId) {
    throw new Error(
      `Programmer problem: supplied empGroupId cannot be falsy, value='${empGroupId}'`,
    )
  }
  const lookup = await _fetchWardNameLookup()
  return lookup[empGroupId]
}

export function validateForSqlInjection(val, $i18n) {
  const isContainsSingleQuote = (val || '').indexOf("'") >= 0
  if (isContainsSingleQuote) {
    return buildResult(false, $i18n.t('not_sql_injection_valid'))
  }
  return buildResult(true, null)
  function buildResult(isValid, msg) {
    return { isValid, msg }
  }
}

export const _testonly = {
  parseResponseForFetchSuperuserAvailableClusters,
  computeRecordHealth,
  generatePeriods,
  getTeiMapping,
  parseOrgUnitStructure,
  teiMappingModes,
  validateForSqlInjection,
}
