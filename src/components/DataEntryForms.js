import XlsxPopulate from 'xlsx-populate/lib/XlsxPopulate'
import * as zz from '@/components/common'
import DataEntryUploadForms from '@/components/subcomponents/DataEntryUploadFormsComponent'
import DataEntryFormsDump from '@/components/subcomponents/DataEntryFormsDumpComponent'
import ZonalDataDump from '@/components/subcomponents/ZonalDataDumpComponent'

export const DataEntryForms = {
  name: 'DataEntryForms',
  mixins: [zz.commonMixin],
  data () {
    const result = {
      isZonalDumpVisible: false,
      uiStatus: 'initial',
      isError: false,
      processedFormCount: 0,
      totalFormCount: '?',
      isLoading: false,
      wards: [],
      selectedWardCode: '',
      period: '',
      periods: []
    }
    return result
  },
  created () {
    this.fetchFormValues()
  },
  components: {
    'upload-forms': DataEntryUploadForms,
    'dump-data': DataEntryFormsDump,
    'zonal-dump-data': ZonalDataDump
  },
  methods: {
    async fetchFormValues () {
      this.isError = false
      this.isLoading = true
      try {
        await Promise.all([
          this._fetchWards(),
          this._generatePeriods(),
          this._checkIsSuperuser()
        ])
      } catch (err) {
        this.handleError('Failed to fetch form data', err)
      } finally {
        this.isLoading = false
      }
    },
    async _generatePeriods () {
      const selectedProgramCode = await zz.getProgramId()
      const periods = await zz.generatePeriods(selectedProgramCode)
      periods.reverse() // let's put most recent first for good UX
      this.periods = periods
    },
    async _fetchWards () {
      this.wards = await zz.fetchWards()
    },
    async _checkIsSuperuser () {
      this.isZonalDumpVisible = !(await zz.isUserAtClusterLevel())
    },
    isFormValid () {
      return this.selectedWardCode && this.period
    },
    onWardSelected () {
      if (this.period) {
        return
      }
      this.period = this.periods[0]
    },
    _getSelectedWard () {
      const matching = this.wards.find(e => e.id === this.selectedWardCode)
      if (!matching) {
        throw new Error(`Expected 1 matching ward for '${this.selectedWardCode}' but found none`)
      }
      return matching
    },
    getWardObj () {
      const o = this._getSelectedWard()
      return {
        id: o.id,
        displayName: o.displayName
      }
    },
    async doDownload () {
      this.isError = false
      this.processedFormCount = 0
      this.uiStatus = 'preparingForm'
      try {
        const programId = await zz.getProgramId()
        const programName = await zz.getProgramName()
        const today = new Date().toLocaleDateString('sw')
        const filename = zz.replaceWeirdCharacters(
          `nacopha_${programName}_${this.getWardObj().displayName}_${today}.xlsx`
        )
        let workbook
        try {
          const templateBytes = await zz.getTemplateBytes(zz.buildDataEntryFormTemplateKey(programId))
          workbook = await XlsxPopulate.fromDataAsync(templateBytes)
        } catch (err) {
          this.uiStatus = 'failed'
          zz.consoleError('Failed to get the data entry template', err)
          return
        }
        const forms = await zz.fetchDataEntryForms()
        this.totalFormCount = Object.keys(forms).length
        for (const currForm of Object.keys(forms)) {
          try {
            const orgUnitId = this.selectedWardCode
            const embeddedConfig = {
              [zz.keys.templateVersion]: zz.dataEntryTemplateVersion,
              [zz.keys.orgUnit]: orgUnitId,
              [zz.markers.ward]: this.getWardObj(),
              [zz.markers.period]: this.period
            }
            await this._createSheet(workbook, currForm, forms[currForm], embeddedConfig)
            this.processedFormCount += 1
          } catch (err) {
            this.uiStatus = 'failed'
            zz.consoleError('Failed to populate the data entry template', err)
            return
          }
        }
        // this doesn't seem to scroll the view, so be sure to save the template when scrolled to the top
        workbook.sheet(0).active(true).activeCell('A1') // make the first sheet active
        let completeWorkbook
        try {
          completeWorkbook = await workbook.outputAsync()
        } catch (err) {
          this.uiStatus = 'failed'
          zz.consoleError('Failed to generate the completed data entry workbook', err)
          return
        }
        zz.triggerDownload(completeWorkbook, filename)
      } catch (err) {
        this.uiStatus = 'failed'
        zz.consoleError('Failed to prepare the data entry forms for download', err)
        return
      }
      this.uiStatus = 'success'
    },
    async _createSheet (workbook, formId, formSections, embeddedConfig) {
      const formIdInt = parseInt(formId) // Object.keys() makes all keys a string
      const formIndex = formIdInt - 1
      let sheet = workbook.sheet(formIndex)
      if (!sheet) {
        workbook.addSheet(`Form ${formIdInt}`, formIndex)
        sheet = workbook.sheet(formIndex)
      }
      const headerRow = formSections.reduce((accum, currFormSection) => {
        const headersForThisSection = zz.concatHeaders(currFormSection.dataElements)
        return accum.concat(headersForThisSection)
      }, [])
      await assertHeaderOrder(formId, headerRow)
      headerRow.splice(1, 0, zz.buildColHeader(zz.fullNameColHeader, zz.fullNameColHeader))
      const headerRowEndColLetter = sheet.column(headerRow.length).columnName()
      const hiddenConfigRowNum = zz.findMarkerRow(sheet, zz.markers.hiddenConfig)
      const serialisedHeaders = headerRow.map(e => JSON.stringify(e))
      zz.writeHiddenValueRange(sheet, `A${hiddenConfigRowNum}:${headerRowEndColLetter}${hiddenConfigRowNum}`, [serialisedHeaders])
      const dataRowStart = zz.findMarkerRow(sheet, zz.markers.dataStart)
      const dataRowEnd = zz.findMarkerRow(sheet, zz.markers.dataEnd)
      try {
        await setOrgUnitHeader(sheet, embeddedConfig)
      } catch (err) {
        throw zz.chainedError(err, 'Failed to set orgUnit header')
      }
      embeddedConfig[zz.keys.checkValueCellAddress] = zz.getCheckValueCellAddress(sheet)
      const initialColumnDefs = {
        [zz.fullNameColHeader]: { valueType: zz.fullNameType }
      }
      const columnDefs = formSections.reduce((accum, currFormSection) => {
        currFormSection.dataElements.forEach(currDataElement => {
          const colHeader = zz.buildColHeaderFromDataElement(currDataElement)
          accum[colHeader[zz.headerIdField]] = currDataElement
        })
        return accum
      }, initialColumnDefs)
      const hiddenConfigColumn = sheet.column(headerRow.length + 1).columnName()
      const teiMapping = await this.mixinGetFullNameMapping(embeddedConfig[zz.keys.orgUnit], formIdInt)
      embeddedConfig[zz.keys.fullNameLookup] = teiMapping
      const configMarkerAndVersion = zz.buildColHeader(zz.markers.hiddenConfig, embeddedConfig)
      zz.writeHiddenValue(sheet, `${hiddenConfigColumn}${hiddenConfigRowNum}`, configMarkerAndVersion)
      zz.writeHiddenValue(sheet, `${hiddenConfigColumn}${dataRowStart}`, zz.markers.dataStart)
      zz.writeHiddenValue(sheet, `${hiddenConfigColumn}${dataRowEnd}`, zz.markers.dataEnd)
      sheet.cell(`A${dataRowEnd + 1}`).value('(end of data entry)')
      const self = this
      async function processCol (i) {
        const currHeader = headerRow[i]
        const currColLetter = i < 26 ? String.fromCharCode(zz.asciiCapitalA + i)
          : 'A' + String.fromCharCode(zz.asciiCapitalA + i - 26)
        const colDef = columnDefs[currHeader[zz.headerIdField]]
        const colType = colDef.valueType
        const hasOptions = colDef.optionSetValue
        const strategy = self.mixinGetColTypeStrategy(colType, hasOptions)
        if (!strategy) {
          throw new Error(`Programmer problem: no validation defined for column='${currHeader[zz.headerTitleField]}' with type='${colType}'` +
            `, hasOptions='${hasOptions}' on form='${formId}'`)
        }
        if (!strategy.getValues || typeof (strategy.getValues) !== 'function') {
          throw new Error(`Programmer problem: there is no 'getValues()' for '${colType}' columns`)
        }
        const range = sheet.range(`${currColLetter}${dataRowStart}:${currColLetter}${dataRowEnd}`)
          .clear()
        const completedValidation = await strategy.getValues({
          colDef,
          orgUnitId: embeddedConfig[zz.keys.orgUnit],
          formId: formIdInt
        })
        if (!completedValidation) {
          return
        }
        let rowCounter = 2
        for (const curr of completedValidation.itemArray) {
          zz.writeHiddenValue(sheet, `${currColLetter}${dataRowEnd + rowCounter}`, curr)
          rowCounter++
        }
        range.dataValidation(completedValidation.validationObj)
      }
      const tasks = []
      for (let i = 0; i < headerRow.length; i++) {
        tasks.push(processCol(i))
      }
      await Promise.all(tasks)
    },
    handleError (msg, error) {
      this.mixinLastDitchErrorHandler(msg, error)
      this.isError = true
    }
  }
}

async function assertHeaderOrder (formId, headerRow) {
  const programId = await zz.getProgramId()
  const dateOfServiceColId = await zz.getFormDosId(programId, formId)
  if (!dateOfServiceColId) {
    throw new Error(`Failed to get the data element ID for the "date of service" column in form ${formId}`)
  }
  const isFirstColDateOfService = headerRow[0].id === dateOfServiceColId
  if (!isFirstColDateOfService) {
    throw new Error(`Expected form ${formId} to have "date of service" (${dateOfServiceColId}) as first column, and it doesn't. Cannot continue. First two headers=${JSON.stringify(headerRow.slice(0, 2))}`)
  }
}

async function setOrgUnitHeader (sheet, embeddedConfig) {
  const orgUnitHierarchy = await zz.getOrgUnitHierarchy()
  zz.writeValueForMarker(zz.markers.region, orgUnitHierarchy.region, sheet)
  zz.writeValueForMarker(zz.markers.district, orgUnitHierarchy.district, sheet)
  zz.writeValueForMarker(zz.markers.cluster, orgUnitHierarchy.cluster, sheet)
  zz.writeValueForMarker(zz.markers.ward, embeddedConfig[zz.markers.ward].displayName, sheet)
  zz.writeValueForMarker(zz.markers.period, embeddedConfig[zz.markers.period], sheet)
}
