import axios from 'axios'
import * as zz from './common'
import FormMgmtComponent from '@/components/subcomponents/admin/FormMgmtComponent'
import OrgUnitGroupComponent from '@/components/subcomponents/admin/OrgUnitGroupComponent'
import OrgAttributesComponent from '@/components/subcomponents/admin/OrgAttributesComponent'
import ZoneLabelPrefixComponent from '@/components/subcomponents/admin/ZoneLabelPrefixComponent'
import MaxPeriodsComponent from '@/components/subcomponents/admin/MaxPeriodsComponent'
import DataElementsComponent from '@/components/subcomponents/admin/DataElementsComponent'

export const AdminComponent = {
  name: 'Admin',
  mixins: [zz.commonMixin],
  data () {
    return {
      password: null,
      isAuthd: false,
      isPasswordFail: false,
      isShowTemplateHelp: false,
      attrMappingSaveState: 'initial',
      constantMappingSaveState: 'initial',
      empGroupFormFilename: zz.empGroupFormTemplateName,
      templateFile: null,
      uploadResult: null,
      dobAttributeId: null,
      fullNameAttributeId: null,
      nacophaIdAttributeId: null,
      ctcAttributeId: null,
      genderAttributeId: null,
      yesConstantId: null,
      noConstantId: null,
      personTeiType: null,
      teiTypeMappingSaveState: 'initial',
      isLoading: false,
      attrs: [],
      constants: [],
      templateLocale: null,
      selectedProgram: null,
      programs: [],
      programMappingSaveState: 'initial',
      enrolTeiState: 'initial',
      enrolTeiProgram: null,
      enrolTeiId: null,
      enrolTeiDate: new Date().toISOString().substr(0, 10)
    }
  },
  created () {
    this.fetchFormValues()
  },
  components: {
    'form-mgmt': FormMgmtComponent,
    'org-attributes': OrgAttributesComponent,
    'zone-label-prefix': ZoneLabelPrefixComponent,
    'max-periods': MaxPeriodsComponent,
    'org-unit-group': OrgUnitGroupComponent,
    'data-elements': DataElementsComponent
  },
  methods: {
    async fetchFormValues () {
      this.isLoading = true
      const tasks = [
        this._populateTrackedEntityAttributes(),
        this._populateConstants(),
        this._populateTeiTypes(),
        this._populatePrograms()
      ]
      try {
        await Promise.all(tasks)
      } catch (err) {
        this.mixinLastDitchErrorHandler('Failed to populate form.', err)
      } finally {
        this.isLoading = false
      }
    },
    resetPasswordFail () {
      this.isPasswordFail = false
    },
    onUnlock () {
      this.resetPasswordFail()
      const adminPassword = 'password' // TODO allow password to be set dynamically
      if (this.password === adminPassword) {
        this.isAuthd = true
        return
      }
      this.isPasswordFail = true
      this.isAuthd = false
      zz.logFailedAdminUnlock(this.password)
    },
    async _populateTeiTypes () {
      this.personTeiType = await zz.getPersonTeiTypeId()
      try {
        const resp = await axios.get(`${zz.apiBaseUrl}/trackedEntityTypes`)
        this.teiTypes = resp.data.trackedEntityTypes
      } catch (err) {
        throw zz.chainedError(err, 'Failed to get TEI types')
      }
    },
    isAllAttrsSelected () {
      return this.fullNameAttributeId && this.dobAttributeId && this.nacophaIdAttributeId &&
        this.ctcAttributeId && this.genderAttributeId
    },
    buildDataEntryTemplateKey (programId) {
      return zz.buildDataEntryFormTemplateKey(programId)
    },
    buildDumpAllDataTemplateKey (programId) {
      return zz.buildDumpAllDataFormTemplateKey(programId)
    },
    async _populatePrograms () {
      const tasks = [
        async () => { this.selectedProgram = await zz.getSautiYetuProgramId() }
      ]
      await Promise.all(tasks.map(e => e()))
      try {
        const resp = await axios.get(`${zz.apiBaseUrl}/programs`, {
          params: {
            paging: false
          }
        })
        this.programs = resp.data.programs
      } catch (err) {
        throw zz.chainedError(err, 'Failed to get the list of programs.')
      }
    },
    async _populateTrackedEntityAttributes () {
      const tasks = [
        async () => { this.dobAttributeId = await zz.getDobAttributeId() },
        async () => { this.fullNameAttributeId = await zz.getFullNameAttributeId() },
        async () => { this.ctcAttributeId = await zz.getCTCAttributeId() },
        async () => { this.genderAttributeId = await zz.getGenderAttributeId() },
        async () => { this.nacophaIdAttributeId = await zz.getNacophaIdAttributeId() }
      ]
      await Promise.all(tasks.map(e => e()))
      try {
        const resp = await axios.get(`${zz.apiBaseUrl}/trackedEntityAttributes`)
        this.attrs = resp.data.trackedEntityAttributes
      } catch (err) {
        throw zz.chainedError(err, 'Failed to get the list of tracked entity attributes.')
      }
    },
    async _populateConstants () {
      const tasks = [
        async () => { this.yesConstantId = await zz.getYesConstantId() },
        async () => { this.noConstantId = await zz.getNoConstantId() },
        async () => { this.constants = await zz.getConstants() }
      ]
      await Promise.all(tasks.map(e => e()))
    },
    async saveTeiTypeMappings () {
      this.teiTypeMappingSaveState = 'initial'
      try {
        await zz.savePersonTeiType(this.personTeiType)
        this.teiTypeMappingSaveState = 'success'
      } catch (err) {
        this.teiTypeMappingSaveState = 'failed'
        throw zz.chainedError(err, 'Failed to save TEI type mappings.')
      }
    },
    async saveConstantMappings () {
      this.constantMappingSaveState = 'initial'
      try {
        await zz.saveYesId(this.yesConstantId)
        await zz.saveNoId(this.noConstantId)
        this.constantMappingSaveState = 'success'
      } catch (err) {
        this.constantMappingSaveState = 'failed'
        throw zz.chainedError(err, 'Failed to save the constant mappings.')
      }
    },
    async saveAttributeMappings () {
      this.attrMappingSaveState = 'initial'
      try {
        await zz.saveDob(this.dobAttributeId)
        await zz.saveFullName(this.fullNameAttributeId)
        await zz.saveNacophaId(this.nacophaIdAttributeId)
        await zz.saveCtcId(this.ctcAttributeId)
        await zz.saveGender(this.genderAttributeId)
        this.attrMappingSaveState = 'success'
      } catch (err) {
        this.attrMappingSaveState = 'failed'
        throw zz.chainedError(err, 'Failed to save the attribute mappings.')
      }
    },
    async saveProgramMappings () {
      this.programMappingSaveState = 'initial'
      try {
        await zz.saveSautiYetuProgramId(this.selectedProgram)
        this.programMappingSaveState = 'success'
      } catch (err) {
        this.programMappingSaveState = 'failed'
        throw zz.chainedError(err, 'Failed to save the program and stage mappings.')
      }
    },
    isAllProgramsSelected () {
      return this.selectedProgram
    },
    async enrolTei () {
      try {
        this.enrolTeiState = 'loading'
        const orgUnit = await getOrgUnitForTei(this.enrolTeiId)
        await axios.post(`${zz.apiBaseUrl}/enrollments`, {
          enrollmentDate: this.enrolTeiDate,
          orgUnit: orgUnit,
          program: this.enrolTeiProgram,
          status: 'ACTIVE',
          trackedEntityInstance: this.enrolTeiId
        })
        this.enrolTeiState = 'success'
      } catch (err) {
        this.enrolTeiState = 'failed'
      }
    },
    isEnrolTeiFormValid () {
      return this.enrolTeiProgram && this.enrolTeiId && this.enrolTeiId.length === 11 && this.enrolTeiDate
    }
  }
}

async function getOrgUnitForTei (teiId) {
  try {
    const resp = await axios.get(`${zz.apiBaseUrl}/trackedEntityInstances/${teiId}`)
    return resp.data.orgUnit
  } catch (err) {
    throw zz.chainedError(err, `Failed to get orgUnit for tei='${teiId}'`)
  }
}
