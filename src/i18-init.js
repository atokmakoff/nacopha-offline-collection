import Vue from 'vue'
import i18next from 'i18next'
import VueI18Next from '@panter/vue-i18next'
import translations from './assets/i18n/translations.json'
import * as zz from './components/common'

Vue.use(VueI18Next)

function transformLocales (localeData) {
  const supportedLocales = ['en', 'sw']
  var transformedData = {}

  // Create a holder for each supported locale
  supportedLocales.forEach(function (entry) {
    transformedData[entry] = {}
  })

  // Now copy the data items over
  for (var tag in localeData) {
    if (localeData.hasOwnProperty(tag)) {
      // Iterate through the language options
      supportedLocales.forEach(function (locale) {
        if (transformedData[locale] === undefined) {
          // Create a holder
          transformedData[locale] = {}
        }
        transformedData[locale][tag] = localeData[tag][locale]
      })
    }
  }
  return (transformedData)
}

function getLocaleAndInit (locales, currentLocale) {
  i18next.init({
    lng: currentLocale,
    resources: {
      en: { translation: locales.en },
      sw: { translation: locales.sw }
    }
  })
  const i18n = new VueI18Next(i18next)
  return i18n
}

export function initWithSpecifiedLocale (locale) {
  const locales = transformLocales(translations)
  return getLocaleAndInit(locales, locale)
}

export async function initWithDhisDbLocale () {
  const currentLocale = await zz.getCurrLocale()
  console.log(`Using locale '${currentLocale}' for i18n`)
  return initWithSpecifiedLocale(currentLocale)
}
