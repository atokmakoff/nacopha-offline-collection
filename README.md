> A webapp to facilitate download/collection of Excel data entry templates, that are used for offline data entry

## Technologies
 - [Vue.js](https://vuejs.org/) for UI binding
 - [Bootstrap-Vue](https://bootstrap-vue.js.org/) for styling
 - [xlsx-populate](https://github.com/dtjohnson/xlsx-populate) for spreadsheet rw

This app is intended to be deployed as an [Open Web App](https://developer.mozilla.org/en-US/docs/Archive/B2G_OS/Quickstart/Intro_to_open_web_apps). A platform like [DHIS2](https://www.dhis2.org/) should be able to run this app.

## Limitations
List validation doesn't work in Google Sheets, but it does in OpenOffice. An export from Google Sheets as `.xlsx` is readable by `xlsx-populate` but a file saved in OpenOffice (as Microsoft format) can't be read by `xlsx-populate`; you get a `ReferenceError: DRc is not defined` error when reading the file.

## How to run locally

  1. install the dependencies

        yarn

  1. optionally, set your login credentials for the dev DHIS server. These details are used by the webpack dev server so it can proxy requests to the DHIS server. If you don't this and the default credentials are wrong, your browser will prompt you.

        export DHIS_AUTH='admin:district' # change credentials to suit

  1. start the webpack dev server

        yarn start

  1. open your web browser to http://localhost:8080 to view the app

The webpack dev server should handle authentication to the DHIS API so all your requests will work.
If they don't work and you get errors, possibly related to CORS, make sure you've cleared your browser
cache and disable caching for HTTP requests (just for the initial load, then you can turn it back on).
That should fix any issues.

## Logging out during local dev
The logout link expects the app to be running *inside* DHIS so it uses a relative link to the logout URL, which will
kill the session. This doesn't work locally because we're running from the webpack-dev-server so this URL doesn't exist.

The easiest way to workaround this is to always open the app in a private browsing window. Then to logout, just close
the window.

If you need to actually logout, it's a bit harder. You need to grab your `JSESSIONID` cookie and then send that to the
logout URL, like:
```bash
curl -H 'Cookie: JSESSIONID=B05DD89C343844334AD60326D1F287A9' https://dev.nacopha.maraxis.com/dhis-web-commons-security/logout.action
```

The problem is that webpack-dev-server remembers your credentials and will just automatically repond to the login
challenge and you'll be logged in again. I'm not sure how to get webpack-dev-server to forget your credentials.

## How to run tests
``` bash
# run unit tests
yarn run unit

# run all tests
yarn test
```

## How to build and deploy app (to DHIS)

  1. build and deploy the build app to DHIS

        export ROLLBAR_ACCESS_TOKEN=123abc  # change me to post_server_item key in rollbar
        export DHIS_AUTH='admin:district'   # change me
        ./deploy-to-dhis.sh dev # could also pass 'train' or 'prod'

## One time app config (post deployment)

Once you've deployed the app to DHIS, you can launch it from the `App Management` menu in DHIS.

Once in the app, you need to go to the secret page `/admin`. There's no link, you need to edit the URL in the address bar. For example: http://hostname/api/apps/Nacopha-Offline-Collection/index.html#/admin.

Read the instructions on the admin page, it will tell you what to do. You can find a template to upload in the `/template` directory of the root of this repository.

## Debugging the unit tests
The tests run using ChromeHeadless, which is nice, until you need to debug something. To do that, edit the `test/unit/karma.conf.js` file and change the lines:
```
browsers: ['ChromeHeadless'],
...
browserNoActivityTimeout: 60000,
```
...to remove the `Headless` suffix and increase the timeout, so it looks like:
```
browsers: ['Chrome'],
...
browserNoActivityTimeout: 60000000,
```
Just **DON'T CHECK THIS IN**. Revert the change before you commit.

Now when you run the tests, a Chrome window will pop up. In that window, there is a `Debug` button. Hit that and open the chrome dev tools in this window. The tests will run each time you refresh the page so set some breakpoints and refresh the page.

On a side note, the line numbers in the source maps are terrible. Debugging is almost unusable in some instances. I think it's an issue with Webpack. Just write code that doesn't need debugging and you'll be fine :D

=======
## Vue.js Dev Tools for Chrome

https://github.com/vuejs/vue-devtools
