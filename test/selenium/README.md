Some tests intended to be run against the local dev version of this app.

The tests were created with https://chrome.google.com/webstore/detail/selenium-ide/mooikfkahbdckldjjndioackbalphokd/related?hl=en.

## To run

  1. install the Selenium plugin
  1. launch the plugin
  1. open the `nacopha-offline.side` project
  1. launch the app with `yarn dev`
  1. open the app and enter the login details for the DHIS API (so the tests don't get stuck)
  1. run all the tests in the Selenium plugin
