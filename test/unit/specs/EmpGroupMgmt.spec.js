import Vue from 'vue'
import router from '@/router'
import { initWithSpecifiedLocale } from '@/i18-init'
import XlsxPopulate from 'xlsx-populate/lib/XlsxPopulate'
import {
  empGroupEnrolDateType,
  _mockable as egMockable,
  teiIdType
} from '@/components/EmpGroupMgmt'
import EmpGroupMgmtComponent from '@/components/EmpGroupMgmtComponent'
import * as zz from '@/components/common'
import { colDefs } from '../trackedEntityAttributeDefs'

zz._mockable.getYesConstantDisplayName = function () { return 'Yes' }
zz._mockable.getNoConstantDisplayName = function () { return 'No' }
zz._mockable.fetchAttributeOptions = mockFetchAttribute
egMockable.fetchActivityGroupVocab = stubActivityGroupVocab
egMockable.fetchGroupActivityAttributeIds = stubActivityAttributes

const Oct16in1973 = 26953
const Feb02in1992 = 33636
const Jun02in2015 = 42157

let objectUnderTest

before(() => {
  EmpGroupMgmtComponent.mixins = [zz.commonMixin] // not actually required, but makes the override explicit
  EmpGroupMgmtComponent.created = () => {}
  const Constructor = Vue.extend(EmpGroupMgmtComponent)
  const i18n = initWithSpecifiedLocale('en')
  objectUnderTest = new Constructor({ i18n, router }).$mount()
})

describe('EmpGroupMgmt', () => {
  describe('_buildRow()', () => {
    it('should build a single row from a TEI when all data is present', async () => {
      const tei = fullyPopulatedTei()
      const result = await objectUnderTest._buildRow(tei, getHeaderRow(), colDefs)
      expect(result).to.have.ordered.members([
        'B1eb60egdlW',
        'test-Some Person',
        'Male',
        Oct16in1973,
        '06-06-0844-0272',
        'No',
        Feb02in1992,
        Jun02in2015,
        'Chair',
        'Saving/Credit Schemes',
        '1234567890'
      ])
    })

    it('should handle missing values', async () => {
      const tei = fullyPopulatedTei()
      tei.attributes.splice(5) // remove some attributes
      const result = await objectUnderTest._buildRow(tei, getHeaderRow(), colDefs)
      expect(result).to.have.ordered.members([
        'B1eb60egdlW',
        'test-Some Person',
        null,
        Oct16in1973,
        null,
        'No',
        null,
        Jun02in2015,
        'Chair',
        null,
        null
      ])
    })
  })

  describe('_generateRowData', () => {
    it('should handle missing values', async () => {
      const teis = [
        fullyPopulatedTei(),
        fullyPopulatedTei(),
        fullyPopulatedTei()
      ]
      const result = await objectUnderTest._generateRowData(teis, getHeaderRow(), colDefs)
      expect(result).to.have.lengthOf(3)
    })
  })

  describe('processActivityGroups', () => {
    it('should write values for the activity groups when there are values', async () => {
      const workbook = await XlsxPopulate.fromBlankAsync()
      const sheet = workbook.sheet(0)
      const embeddedConfig = {
        groupAttributeValues: [
          { value: '1', attribute: { id: 'OtTGAV2WIMb' } },
          { value: '7', attribute: { id: 'yxbdu4QIqrE' } },
          { value: '5', attribute: { id: 'A4wdIOu7h58' } },
          { value: '2', attribute: { id: 'MIVvzoGunLT' } }
        ],
        addresses: {
          economic: 'A1:A3',
          communityHiv: 'B1:B3',
          social: 'D8:D10'
        }
      }
      await objectUnderTest._processActivityGroups(sheet, embeddedConfig)
      const economic1 = sheet.cell('A1').value()
      expect(economic1).to.eql('Animal Husbandry')
      const economic2 = sheet.cell('A2').value()
      expect(economic2).to.eql('Rentals')
      const comHiv1 = sheet.cell('B1').value()
      expect(comHiv1).to.eql('Proper use of condoms and distribution')
      const social1 = sheet.cell('D8').value()
      expect(social1).to.eql('Support the Elderly')
    })
  })
})

function getHeaderRow () {
  const c = zz.buildColHeader
  const result = [
    c(teiIdType, 'TEI ID'),
    c('Oe2oAS9TfGA', 'Full name'),
    c('jeiN3PX6zqu', 'Gender'),
    c('nWEOJSdLtH3', 'Date of birth'),
    c('FvpuJ1Ks9nL', 'CTC Number'),
    c('e3ENws3guve', 'On ART?'),
    c('pgBG2Bic7vl', 'Date started ART'),
    c(empGroupEnrolDateType, ''),
    c('fWlJ0KzmOZs', 'Empowerment Group (Role)'),
    c('OavhN80L78w', 'Individual economic activity'),
    c('z2q6ASGYCt0', 'Phone number')
  ]
  return result
}

function fullyPopulatedTei () {
  return {
    trackedEntityInstance: 'B1eb60egdlW',
    enrollments: [
      { enrollmentDate: '2015-06-02' }
    ],
    attributes: [
      {
        displayName: 'Person: HIV Status',
        attribute: 'T5ft6x3yYJJ',
        value: '1'
      }, {
        displayName: 'Person: Full Name',
        attribute: 'Oe2oAS9TfGA',
        value: 'test-Some Person'
      }, {
        displayName: 'Person: Empowerment Group (Role)',
        attribute: 'fWlJ0KzmOZs',
        value: '0'
      }, {
        displayName: 'Person: Age',
        attribute: 'nWEOJSdLtH3',
        value: '1973-10-16'
      }, {
        displayName: 'Person: On ART?',
        attribute: 'e3ENws3guve',
        value: 'false'
      }, {
        displayName: 'Person: Individual Economic Activity',
        attribute: 'OavhN80L78w',
        value: '2'
      }, {
        displayName: 'Person: CTC',
        attribute: 'FvpuJ1Ks9nL',
        value: '06-06-0844-0272'
      }, {
        displayName: 'Person: Date started ART',
        attribute: 'pgBG2Bic7vl',
        value: '1992-02-02'
      }, {
        displayName: 'Person: Gender',
        attribute: 'jeiN3PX6zqu',
        value: '1'
      }, {
        displayName: 'Person: NACOPHA ID',
        attribute: 'Ek1Ce9w7ua6',
        value: '201840-00412'
      }, {
        displayName: 'Person: Phone Number',
        attribute: 'z2q6ASGYCt0',
        value: '1234567890'
      }, {
        displayName: 'Person: Initials',
        attribute: 'hDrhKE59EGO',
        value: 'AA'
      }
    ]
  }
}

function stubActivityAttributes () {
  return {
    economic: ['OtTGAV2WIMb', 'A4wdIOu7h58'],
    communityHiv: ['yxbdu4QIqrE'],
    social: ['MIVvzoGunLT']
  }
}

function stubActivityGroupVocab () {
  const economicOptionSet = {
    options: [
      { code: '0', displayName: 'Agriculture' },
      { code: '1', displayName: 'Animal Husbandry' },
      { code: '2', displayName: 'Saving/Credit Schemes' },
      { code: '3', displayName: ' Village Community Banks (VICOBA)' },
      { code: '4', displayName: 'Entrepreneurships' },
      { code: '5', displayName: 'Rentals' }
    ]
  }
  return {
    OtTGAV2WIMb: {
      id: 'OtTGAV2WIMb',
      displayName: '(EG) Economic activity 1',
      valueType: 'TEXT',
      optionSet: economicOptionSet
    },
    A4wdIOu7h58: {
      id: 'A4wdIOu7h58',
      displayName: '(EG) Economic activity 2',
      valueType: 'TEXT',
      optionSet: economicOptionSet
    },
    yxbdu4QIqrE: {
      id: 'yxbdu4QIqrE',
      displayName: '(EG) Community HIV activity 1',
      valueType: 'TEXT',
      optionSet: {
        options: [
          { code: '1', displayName: 'HIV testing Promotion' },
          { code: '2', displayName: 'HIV/AIDS Literacy' },
          { code: '3', displayName: 'Lost to Follow up Tracking' },
          { code: '4', displayName: 'Community ART Dispensing' },
          { code: '5', displayName: ' HIV related Stigma and Discrimination Awareness' },
          { code: '6', displayName: 'HIV standard Operating Procedures (SOPs)' },
          { code: '7', displayName: 'Proper use of condoms and distribution' },
          { code: '8', displayName: 'Nutrition' }
        ]
      }
    },
    MIVvzoGunLT: {
      id: 'MIVvzoGunLT',
      displayName: '(EG) Social activity 1',
      valueType: 'TEXT',
      optionSet: {
        options: [
          { code: '1', displayName: 'Support OVCs' },
          { code: '2', displayName: 'Support the Elderly' },
          { code: '3', displayName: 'Children’s Rights awareness' },
          { code: '4', displayName: 'Human rights awareness' },
          { code: '5', displayName: 'Gender Based Violence awareness' }
        ]
      }
    }
  }
}

function mockFetchAttribute (attributeId) {
  const resultMap = {
    OtTGAV2WIMb: [],
    yxbdu4QIqrE: [],
    A4wdIOu7h58: [],
    MIVvzoGunLT: []
  }
  return resultMap[attributeId]
}
