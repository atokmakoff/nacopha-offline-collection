import Vue from 'vue'
import HomeComponent from '@/components/HomeComponent'
import router from '@/router'
import { initWithSpecifiedLocale } from '@/i18-init'

describe('HomeComponent.vue', () => {
  let objectUnderTest

  before(() => {
    const Constructor = Vue.extend(HomeComponent)
    const i18n = initWithSpecifiedLocale('en')
    objectUnderTest = new Constructor({ i18n, router }).$mount()
  })

  it('should render correct contents', () => {
    expect(objectUnderTest.$el.querySelector('h1').textContent).to.equal('Home')
  })
})
