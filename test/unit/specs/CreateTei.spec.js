/* eslint no-unused-expressions: 0 */
import {validateCtc} from '@/components/subcomponents/CreateTei'

const mockI18n = {t: (key) => key}

describe('CreateTei', () => {
  describe('validateCtc', () => {
    it('should handle the new format', () => {
      const ctc = '12-34-5678-9081'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.true
      expect(result.msg).to.be.null
    })

    it('should accept null when no CTC is required', () => {
      const ctc = null
      const isCtcRequired = false
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.true
      expect(result.msg).to.be.null
    })

    it('should accept the old, short, all digits format', () => {
      const ctc = '079-223'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.true
      expect(result.msg).to.be.null
    })

    it('should accept the old, long, all digits format', () => {
      const ctc = '07217-002756'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.true
      expect(result.msg).to.be.null
    })

    it('should accept the old, digits and characters format', () => {
      const ctc = 'A63-114'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.true
      expect(result.msg).to.be.null
    })

    it('should reject something that contains the correct format but is padded with other stuff', () => {
      const ctc = 'A63-114aa'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.false
      expect(result.msg).to.eql('ctc_format_not_ok')
    })

    it('should reject something that looks like the right length but is missing the hyphen', () => {
      const ctc = 'A633114'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.false
      expect(result.msg).to.eql('ctc_format_not_ok')
    })

    it('should reject a null CTC when it is required', () => {
      const ctc = null
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.false
      expect(result.msg).to.eql('ctc_required_create')
    })

    it('should accept a valid four-four CTC (all numbers)', () => {
      const ctc = '1234-1234'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.true
      expect(result.msg).to.be.null
    })

    it('should reject an invalid four-four CTC (has letters)', () => {
      const ctc = '1ab4-d23e'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.false
      expect(result.msg).to.eql('ctc_format_not_ok')
    })

    it('should accept the four-four-five digits format', () => {
      const ctc = '1004-0102-01111'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.true
      expect(result.msg).to.null
    })

    it('should accept the two-two-four-N format for N=4', () => {
      const ctc = '11-22-3333-4444'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.true
      expect(result.msg).to.null
    })

    it('should accept the two-two-four-N format for N=5', () => {
      const ctc = '11-22-3333-55555'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.true
      expect(result.msg).to.null
    })

    it('should accept the two-two-four-N format for N=6', () => {
      const ctc = '11-22-3333-666666'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.true
      expect(result.msg).to.null
    })

    it('should accept the two-two-four-N format for N=7', () => {
      const ctc = '11-22-3333-7777777'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.true
      expect(result.msg).to.null
    })

    it('should accept the two-two-four-N format for N=8', () => {
      const ctc = '11-22-3333-88888888'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.true
      expect(result.msg).to.null
    })

    it('should accept the two-two-four-N format for N=9', () => {
      const ctc = '11-22-3333-999999999'
      const isCtcRequired = true
      const result = validateCtc(isCtcRequired, ctc, mockI18n)
      expect(result.isValid).to.be.true
      expect(result.msg).to.null
    })
  })
})
