import {_testonly} from '@/components/subcomponents/ZonalDataDump'

describe('ZonalDataDump', () => {
  describe('_createLookup', () => {
    it('should find dimension values that exist', () => {
      const metaData = {
        dimensions: {
          aaa: ['aa1', 'aa2'],
        },
        items: {
          aa1: {name: 'AA one', code: 1},
          aa2: {name: 'AA two', code: 2},
        },
      }
      const result = _testonly._createLookup(metaData)
      expect(Object.keys(result)).to.have.lengthOf(1)
      expect(result['aaa']).to.have.lengthOf(2)
      expect(result['aaa']).to.have.deep.members([
        {name: 'AA one', code: 1},
        {name: 'AA two', code: 2},
      ])
    })

    it('should handle empty dimensions', () => {
      const metaData = {
        dimensions: {
          aaa: [],
        },
        items: {},
      }
      const result = _testonly._createLookup(metaData)
      expect(Object.keys(result)).to.have.lengthOf(1)
      expect(result['aaa']).to.have.lengthOf(0)
    })
  })

  describe('NameResolver', () => {
    let systemUnderTest

    before(() => {
      const resp = {
        data: {
          metaData: {
            dimensions: {
              aaa: ['aa1', 'aa2'],
            },
            items: {
              aa1: {name: 'AA one', code: 1},
              aa2: {name: 'AA two', code: 2},
              bbb: {name: 'Triple B', code: 1},
            },
          },
        },
      }
      systemUnderTest = new _testonly.NameResolver(resp, {
        booleanTrueLabel: 'TestYes',
        booleanFalseLabel: 'TestNo',
      })
    })

    it('should resolve a code for a column header', () => {
      const result = systemUnderTest.resolve('bbb', null)
      expect(result).to.eql('Triple B')
    })

    it('should resolve a code for a vocab based field', () => {
      const result = systemUnderTest.resolve(1, {
        valueType: 'TEXT',
        name: 'aaa',
      })
      expect(result).to.eql('AA one')
    })

    it('should handle a header value that is not in the lookup', () => {
      const result = systemUnderTest.resolve('zone', null)
      expect(result).to.eql('zone')
    })

    it('should handle a data value that is not in the lookup', () => {
      const result = systemUnderTest.resolve(5, {
        valueType: 'TEXT',
        name: 'aaa',
      })
      expect(result).to.eql(5)
    })

    it('should handle a false boolean value', () => {
      const result = systemUnderTest.resolve('0', {
        valueType: 'BOOLEAN',
        name: 'ccc',
      })
      expect(result).to.eql('TestNo')
    })

    it('should handle a missing boolean value', () => {
      const result = systemUnderTest.resolve('', {
        valueType: 'BOOLEAN',
        name: 'ccc',
      })
      expect(result).to.eql('') // missing is NOT false
    })

    it('should handle a true boolean value', () => {
      const result = systemUnderTest.resolve('1', {
        valueType: 'BOOLEAN',
        name: 'ccc',
      })
      expect(result).to.eql('TestYes')
    })

    it('should handle a value that uses the default strategy', () => {
      const result = systemUnderTest.resolve('2019-01-30', {
        valueType: 'DATE',
        name: 'ccc',
      })
      expect(result).to.eql('2019-01-30')
    })
  })
})
