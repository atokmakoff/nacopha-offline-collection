import {
  _testonly
} from '@/components/subcomponents/DataEntryUploadForms'

describe('DataEntryUploadForms', () => {
  describe('.isNotInReportingPeriod()', () => {
    it('should handle dates at the start of the period', async () => {
      const result = await _testonly.isNotInReportingPeriod('2018-10', '2018-10-01')
      expect(result).to.be.false
    })

    it('should handle dates at the end of the period', async () => {
      const result = await _testonly.isNotInReportingPeriod('2018-10', '2018-10-31')
      expect(result).to.be.false
    })

    it('should reject dates that do not exist for a given month', async () => {
      const result = await _testonly.isNotInReportingPeriod('2018-11', '2018-11-31')
      expect(result).to.be.true
    })

    it('should handle dates in the middle of the period', async () => {
      const result = await _testonly.isNotInReportingPeriod('2018-10', '2018-10-16')
      expect(result).to.be.false
    })

    it('should reject dates before the period', async () => {
      const result = await _testonly.isNotInReportingPeriod('2018-10', '2018-09-30')
      expect(result).to.be.true
    })

    it('should reject dates after the period', async () => {
      const result = await _testonly.isNotInReportingPeriod('2018-10', '2018-11-01')
      expect(result).to.be.true
    })
  })
})
