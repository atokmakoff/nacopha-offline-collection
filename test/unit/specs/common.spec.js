/* eslint no-unused-expressions: 0 */
import Vue from 'vue'
import i18next from 'i18next'
import VueI18Next from '@panter/vue-i18next'
import * as commonModule from '@/components/common'
commonModule._mockable._getYesConstantDisplayName = function() {
  return 'Yes'
}
commonModule._mockable._getNoConstantDisplayName = function() {
  return 'No'
}
commonModule._mockable.getCTCAttributeId = () => {
  return 'ctcAId'
}
commonModule._mockable.getFullNameAttributeId = () => {
  return 'fullNameAId'
}
commonModule._mockable.getDobAttributeId = () => {
  return 'dobAId'
}
commonModule._mockable.getGenderAttributeId = () => {
  return 'genderAId'
}
commonModule._mockable.fetchGenders = () => {
  return [
    { code: '1', displayName: 'Male' },
    { code: '2', displayName: 'Female' },
  ]
}

Vue.use(VueI18Next)
const i18n = new VueI18Next(i18next)
const Constructor = Vue.extend({
  mixins: [commonModule.commonMixin],
})
const objectUnderTest = new Constructor({ i18n })

describe('common', () => {
  describe('BOOLEAN col strategy', () => {
    const strategy = objectUnderTest.mixinGetColTypeStrategy('BOOLEAN', false)

    describe('valueToDisplay', () => {
      it('should convert a false value', async () => {
        const colDef = {}
        const value = 'false'
        const result = await strategy.valueToDisplay(colDef, value)
        expect(result).to.eql('No')
      })

      it('should convert a true value', async () => {
        const colDef = {}
        const value = 'true'
        const result = await strategy.valueToDisplay(colDef, value)
        expect(result).to.eql('Yes')
      })

      it('should handle no value', async () => {
        const colDef = {}
        const value = null
        const result = await strategy.valueToDisplay(colDef, value)
        expect(result).to.be.null
      })
    })

    describe('displayToValue', () => {
      it('should convert a true value', async () => {
        const result = await strategy.displayToValue('Yes')
        expect(result).to.be.true
      })

      it('should convert a false value', async () => {
        const result = await strategy.displayToValue('No')
        expect(result).to.be.false
      })

      it('should convert no value to false', async () => {
        const result = await strategy.displayToValue(null)
        expect(result).to.be.null
      })
    })
  })

  describe('TEXT_novocab col strategy', () => {
    const strategy = objectUnderTest.mixinGetColTypeStrategy('TEXT', false)

    describe('valueToDisplay', () => {
      it('should leave the value untouched', async () => {
        const colDef = {}
        const value = 'some value'
        const result = await strategy.valueToDisplay(colDef, value)
        expect(result).to.eql(value)
      })
    })

    describe('displayToValue', () => {
      it('should leave the value untouched', async () => {
        const result = await strategy.displayToValue('some value')
        expect(result).to.eql('some value')
      })
    })
  })

  describe('TEXT_vocab col strategy', () => {
    const strategy = objectUnderTest.mixinGetColTypeStrategy('TEXT', true)

    describe('valueToDisplay', () => {
      it('should handle a value where the displayName is found', async () => {
        const colDef = {
          optionSet: { options: [{ code: '2', displayName: 'Two' }] },
        }
        const value = '2'
        const result = await strategy.valueToDisplay(colDef, value)
        expect(result).to.eql('Two')
      })

      it('should handle no value', async () => {
        const colDef = {
          optionSet: { options: [{ code: '2', displayName: 'Two' }] },
        }
        const value = null
        const result = await strategy.valueToDisplay(colDef, value)
        expect(result).to.be.null
      })
    })

    describe('displayToValue', () => {
      it('should handle a displayName where the code mapping is found', async () => {
        const options = { Two: '2' }
        const result = await strategy.displayToValue('Two', options)
        expect(result).to.eql('2')
      })

      it('should handle no displayName', async () => {
        const options = { Two: '2' }
        const noDisplayValue = null
        const result = await strategy.displayToValue(noDisplayValue, options)
        expect(result).to.be.null
      })
    })
  })

  describe('DATE col strategy', () => {
    const strategy = objectUnderTest.mixinGetColTypeStrategy('DATE', false)
    const Oct13In2018 = 43386

    describe('valueToDisplay', () => {
      it('should map from a date string to a number that suits LibreCalc', async () => {
        const colDef = {}
        const value = '2018-10-13'
        const result = await strategy.valueToDisplay(colDef, value)
        expect(result).to.eql(Oct13In2018)
      })
    })

    describe('displayToValue', () => {
      it('should map from a LibreCalc number to a date string', async () => {
        const result = await strategy.displayToValue(Oct13In2018)
        expect(result).to.eql('2018-10-13')
      })

      it('should throw the expected error when we pass an invalid date', async () => {
        try {
          await strategy.displayToValue('some rubbish')
        } catch (err) {
          expect(err.message).to.match(/^Moment.js tells us/)
        }
      })
    })
  })

  describe('extractActivityGroup', () => {
    it('should extract the values when they are present', async () => {
      const relevantIds = ['id1', 'id3'] // note that this implies order too
      const values = [
        { value: '3', attribute: { id: 'id3' } },
        { value: '7', attribute: { id: 'id1' } },
        { value: '2', attribute: { id: 'id2' } },
      ]
      const groupVocab = {
        id1: {
          optionSet: {
            options: [
              { code: '1', displayName: 'ignore me' },
              { code: '7', displayName: 'Value Seven' },
            ],
          },
        },
        id2: {
          optionSet: {
            options: [
              { code: '1', displayName: 'ignore me' },
              { code: '2', displayName: 'Value Two' },
            ],
          },
        },
        id3: {
          optionSet: {
            options: [
              { code: '1', displayName: 'ignore me' },
              { code: '3', displayName: 'Value Three' },
            ],
          },
        },
      }
      const result = commonModule.extractActivityGroup(
        relevantIds,
        groupVocab,
        values,
      )
      expect(result).to.have.ordered.members(['Value Seven', 'Value Three'])
    })
  })

  describe('getTeiMapping', async () => {
    it('should create the expected list when CTCs are NOT required', async () => {
      commonModule._mockable.getTeiRecords = getStubTeis
      const noCtcs = commonModule._testonly.teiMappingModes.withoutCtcOnly
      const result = await commonModule._testonly.getTeiMapping(
        'someOrgUnitId',
        noCtcs,
      )
      expect(result).to.deep.equal({
        'person 2; 2002-02-02; Female': 'tei2',
      })
    })

    it('should create the expected list when CTCs are required', async () => {
      commonModule._mockable.getTeiRecords = getStubTeis
      const useCtcs = commonModule._testonly.teiMappingModes.withCtcOnly
      const result = await commonModule._testonly.getTeiMapping(
        'someOrgUnitId',
        useCtcs,
      )
      expect(result).to.deep.equal({
        'person 1; ctc1; 2001-01-01; Male': 'tei1',
        'ctc1; person 1; 2001-01-01; Male': 'tei1',
        'person 3; ctc3; 2003-03-03; Female': 'tei3',
        'ctc3; person 3; 2003-03-03; Female': 'tei3',
      })
    })

    it('should ensure that duplicate full name labels do NOT clobber', async () => {
      commonModule._mockable.getTeiRecords = function() {
        return [
          {
            trackedEntityInstance: 'tei1',
            attributes: [
              { attribute: 'fullNameAId', value: 'John Smith' },
              { attribute: 'dobAId', value: '2001-01-01' },
              { attribute: 'genderAId', value: '1' },
            ],
          },
          {
            trackedEntityInstance: 'tei2',
            attributes: [
              { attribute: 'fullNameAId', value: 'John Smith' },
              { attribute: 'dobAId', value: '2001-01-01' },
              { attribute: 'genderAId', value: '1' },
            ],
          },
        ]
      }
      const noCtcs = commonModule._testonly.teiMappingModes.withoutCtcOnly
      const result = await commonModule._testonly.getTeiMapping(
        'someOrgUnitId',
        noCtcs,
      )
      expect(result).to.deep.equal({
        'John Smith; 2001-01-01; Male': 'tei1',
        'John Smith; 2001-01-01; Male ': 'tei2',
      })
    })
  })

  describe('.parseOrgUnitStructure()', () => {
    it('should parse the orgUnit structure when all levels are available', () => {
      const apiResp = {
        id: 'ZqbWoSs9kpn',
        level: 5,
        displayName: 'Elerai Group 1',
        parent: {
          id: 'qIr52OXWLhq',
          level: 4,
          displayName: 'Elerai',
          parent: {
            id: 'Ya1xQwpjOBl',
            level: 3,
            displayName: 'Arusha CC Konga',
            parent: {
              id: 'AYRa3VeAM9r',
              level: 2,
              displayName: 'Arusha',
              organisationUnitGroups: [
                {
                  id: 'SKF72aaBcfm',
                  name: 'Zone: Dar',
                  displayName: 'Zone: Dar',
                },
                { id: 'S8mH6C5brSw', name: 'Regions', displayName: 'Regions' },
              ],
              parent: {
                id: 'dibzPfQOEJQ',
                level: 1,
                displayName: 'NACOPHA HQ',
              },
            },
          },
        },
      }
      const result = commonModule._testonly.parseOrgUnitStructure(
        apiResp,
        'Zone: ',
      )
      expect(result).to.deep.equal({
        zone: 'Dar',
        region: 'Arusha',
        district: 'Arusha CC Konga',
        cluster: 'Arusha CC Konga',
        ward: 'Elerai',
        empowermentGroupName: 'Elerai Group 1',
      })
    })

    it('should parse the orgUnit structure when only some levels are passed', () => {
      const apiResp = {
        id: 'Ya1xQwpjOBl',
        level: 3,
        displayName: 'Arusha CC Konga',
        parent: {
          id: 'AYRa3VeAM9r',
          level: 2,
          displayName: 'Arusha',
          organisationUnitGroups: [
            { id: 'SKF72aaBcfm', name: 'Zone: Dar', displayName: 'Zone: Dar' },
            { id: 'S8mH6C5brSw', name: 'Regions', displayName: 'Regions' },
          ],
          parent: {
            id: 'dibzPfQOEJQ',
            level: 1,
            displayName: 'NACOPHA HQ',
          },
        },
      }
      const result = commonModule._testonly.parseOrgUnitStructure(
        apiResp,
        'Zone: ',
      )
      expect(result).to.deep.equal({
        zone: 'Dar',
        region: 'Arusha',
        district: 'Arusha CC Konga',
        cluster: 'Arusha CC Konga',
      })
    })

    it('should throw the expected error when only one level is passed', () => {
      const apiResp = {
        id: 'dibzPfQOEJQ',
        level: 1,
        displayName: 'NACOPHA HQ',
      }
      try {
        commonModule._testonly.parseOrgUnitStructure(apiResp, 'Zone: ')
      } catch (err) {
        expect(err.message).to.equal(`no strategy found for level='1'`)
      }
    })
  })

  describe('.generatePeriods()', () => {
    it('should generate all reporting periods since the start date', async () => {
      commonModule._mockable.fetchProgramStartDate = () => {
        return '2018-08-16T08:00:53.124'
      }
      const programId = 'abc123'
      const untilDate = new Date('2018-10-21')
      const result = await commonModule._testonly.generatePeriods(
        programId,
        untilDate,
        6,
      )
      expect(result).to.have.ordered.members(['2018-08', '2018-09', '2018-10'])
    })

    it('should handle wrapping over a year boundary', async () => {
      commonModule._mockable.fetchProgramStartDate = () => {
        return '2018-11-01T00:00:00.000'
      }
      const programId = 'abc123'
      const untilDate = new Date('2019-03-01')
      const result = await commonModule._testonly.generatePeriods(
        programId,
        untilDate,
        6,
      )
      expect(result).to.have.ordered.members([
        '2018-11',
        '2018-12',
        '2019-01',
        '2019-02',
        '2019-03',
      ])
    })

    it('should respect the max periods to generate', async () => {
      commonModule._mockable.fetchProgramStartDate = () => {
        return '2018-02-01T00:00:00.000'
      }
      commonModule._mockable.getMaxReportingPeriods = () => {
        return 6
      }
      const programId = 'abc123'
      const untilDate = new Date('2019-03-01')
      const result = await commonModule._testonly.generatePeriods(
        programId,
        untilDate,
      )
      expect(result).to.have.lengthOf(6)
      expect(result).to.have.ordered.members([
        '2018-10',
        '2018-11',
        '2018-12',
        '2019-01',
        '2019-02',
        '2019-03',
      ])
    })
  })

  describe('computeRecordHealth()', () => {
    it('should consider a record with ACTIVE enrollment and matching orgUnit as healthy', async () => {
      const record = {
        orgUnit: 'orgUnit1',
        trackedEntityInstance: 'KRR44UvFjfF',
        enrollments: [
          {
            orgUnit: 'orgUnit1',
            program: 'kBqHaz4Y8Sf',
            deleted: false,
            status: 'ACTIVE',
          },
        ],
      }
      const result = await commonModule._testonly.computeRecordHealth(record)
      expect(result).to.be.true
    })

    it('should ignore non-active enrollments', async () => {
      const record = {
        orgUnit: 'orgUnit1',
        trackedEntityInstance: 'KRR44UvFjfF',
        enrollments: [
          {
            orgUnit: 'orgUnit1',
            program: 'kBqHaz4Y8Sf',
            deleted: false,
            status: 'COMPLETED',
          },
          {
            orgUnit: 'orgUnit1',
            program: 'kBqHaz4Y8Sf',
            deleted: false,
            status: 'ACTIVE',
          },
        ],
      }
      const result = await commonModule._testonly.computeRecordHealth(record)
      expect(result).to.be.true
    })

    it('should consider no enrollments as unhealthy', async () => {
      const record = {
        orgUnit: 'orgUnit1',
        trackedEntityInstance: 'KRR44UvFjfF',
        enrollments: [],
      }
      const result = await commonModule._testonly.computeRecordHealth(record)
      expect(result).to.be.false
    })

    it('should consider no ACTIVE enrollments as unhealthy', async () => {
      const record = {
        orgUnit: 'orgUnit1',
        trackedEntityInstance: 'KRR44UvFjfF',
        enrollments: [
          {
            orgUnit: 'orgUnit1',
            program: 'kBqHaz4Y8Sf',
            deleted: false,
            status: 'COMPLETED',
          },
        ],
      }
      const result = await commonModule._testonly.computeRecordHealth(record)
      expect(result).to.be.false
    })

    it('should consider a record with ACTIVE enrollment but non-matching orgUnit as unhealthy', async () => {
      const record = {
        orgUnit: 'orgUnit1',
        trackedEntityInstance: 'KRR44UvFjfF',
        enrollments: [
          {
            orgUnit: 'orgUnit2', // non-matching
            program: 'kBqHaz4Y8Sf',
            deleted: false,
            status: 'ACTIVE',
          },
        ],
      }
      const result = await commonModule._testonly.computeRecordHealth(record)
      expect(result).to.be.false
    })

    it('should consider a record with matching ACTIVE enrollment but deleted as unhealthy', async () => {
      const record = {
        orgUnit: 'orgUnit1',
        trackedEntityInstance: 'KRR44UvFjfF',
        enrollments: [
          {
            orgUnit: 'orgUnit1',
            program: 'kBqHaz4Y8Sf',
            deleted: true, // note logical deletion
            status: 'ACTIVE',
          },
        ],
      }
      const result = await commonModule._testonly.computeRecordHealth(record)
      expect(result).to.be.false
    })
  })

  describe('parseResponseForFetchSuperuserAvailableClusters()', () => {
    it('should parse the response for a zonal user', () => {
      const resp = require('./zonal-user-me-resp.json')
      const result = commonModule._testonly.parseResponseForFetchSuperuserAvailableClusters(
        resp,
      )
      expect(result).to.have.lengthOf(4)
      const displayNames = extractDisplayNames(result)
      expect(displayNames).to.have.ordered.members([
        'Dar es Salaam - Kigamboni MC Konga',
        'Morogoro - Morogoro MC Konga',
        'Mtwara - Newala DC Konga',
        'Mtwara - Tandahimba DC Konga',
      ])
    })

    it('should parse the response for an HQ user', () => {
      const resp = require('./hq-user-me-resp.json')
      const result = commonModule._testonly.parseResponseForFetchSuperuserAvailableClusters(
        resp,
      )
      expect(result).to.have.lengthOf(2)
      const displayNames = extractDisplayNames(result)
      expect(displayNames).to.have.ordered.members([
        'Morogoro - Morogoro MC Konga',
        'Tabora - Nzega DC Konga',
      ])
    })

    it('should handle when no clusters are available', () => {
      const resp = { organisationUnits: [] }
      const result = commonModule._testonly.parseResponseForFetchSuperuserAvailableClusters(
        resp,
      )
      expect(result).to.have.lengthOf(0)
    })
  })
})

describe('validateForSqlInjection()', () => {
  it('should validate a string that does NOT need escaping', () => {
    const result = commonModule._testonly.validateForSqlInjection(
      'john smith',
      i18n,
    )
    expect(result.isValid).to.equal(true)
  })

  it('should parse a string that DOES need escaping', () => {
    const result = commonModule._testonly.validateForSqlInjection(
      "john o'malley",
      i18n,
    )
    expect(result.isValid).to.equal(false)
  })

  it('should parse undefined', () => {
    const result = commonModule._testonly.validateForSqlInjection(
      undefined,
      i18n,
    )
    expect(result.isValid).to.equal(true)
  })
})

function getStubTeis(orgUnitId, ouMode, filter) {
  const result = [
    {
      trackedEntityInstance: 'tei1',
      attributes: [
        { attribute: 'ctcAId', value: 'ctc1' },
        { attribute: 'fullNameAId', value: 'person 1' },
        { attribute: 'dobAId', value: '2001-01-01' },
        { attribute: 'genderAId', value: '1' },
      ],
    },
    {
      trackedEntityInstance: 'tei3',
      attributes: [
        { attribute: 'ctcAId', value: 'ctc3' },
        { attribute: 'fullNameAId', value: 'person 3' },
        { attribute: 'dobAId', value: '2003-03-03' },
        { attribute: 'genderAId', value: '2' },
      ],
    },
  ]
  if (!filter) {
    result.push({
      trackedEntityInstance: 'tei2',
      attributes: [
        // no CTC on purpose!
        { attribute: 'fullNameAId', value: 'person 2' },
        { attribute: 'dobAId', value: '2002-02-02' },
        { attribute: 'genderAId', value: '2' },
      ],
    })
  }
  return result
}

function extractDisplayNames(objArray) {
  return objArray.map(e => e.displayName)
}
