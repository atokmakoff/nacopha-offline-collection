export const colDefs = {
  'gKXIIG4Cpur': {
    'id': 'gKXIIG4Cpur',
    'displayName': 'EG_HIV Response Activities',
    'valueType': 'TEXT',
    'optionSetValue': true,
    'optionSet': {
      'options': [
        {
          'code': '1',
          'displayName': 'HIV testing Promotion'
        },
        {
          'code': '2',
          'displayName': 'HIV/AIDS Literacy'
        },
        {
          'code': '3',
          'displayName': 'Lost to Follow up Tracking'
        },
        {
          'code': '4',
          'displayName': 'Community ART Dispensing'
        },
        {
          'code': '5',
          'displayName': ' HIV related Stigma and Discrimination Awareness'
        },
        {
          'code': '6',
          'displayName': 'HIV standard Operating Procedures (SOPs)'
        },
        {
          'code': '7',
          'displayName': 'Proper use of condoms and distribution'
        },
        {
          'code': '8',
          'displayName': 'Nutrition'
        }
      ]
    }
  },
  'zZzTES2QLyM': {
    'id': 'zZzTES2QLyM',
    'displayName': 'EG_Main Economic Activity',
    'valueType': 'TEXT',
    'optionSetValue': true,
    'optionSet': {
      'options': [
        {
          'code': '0',
          'displayName': 'Agriculture'
        },
        {
          'code': '1',
          'displayName': 'Animal Husbandry'
        },
        {
          'code': '2',
          'displayName': 'Saving/Credit Schemes'
        },
        {
          'code': '3',
          'displayName': ' Village Community Banks (VICOBA)'
        },
        {
          'code': '4',
          'displayName': 'Entrepreneurships'
        },
        {
          'code': '5',
          'displayName': 'Rentals'
        }
      ]
    }
  },
  'm3A7LFTCwmG': {
    'id': 'm3A7LFTCwmG',
    'displayName': 'Empowerment Group (Name)',
    'valueType': 'ORGANISATION_UNIT',
    'optionSetValue': false
  },
  'nWEOJSdLtH3': {
    'id': 'nWEOJSdLtH3',
    'displayName': 'Person: Age',
    'valueType': 'AGE',
    'optionSetValue': false
  },
  'FvpuJ1Ks9nL': {
    'id': 'FvpuJ1Ks9nL',
    'displayName': 'Person: CTC',
    'valueType': 'TEXT',
    'optionSetValue': false
  },
  'pgBG2Bic7vl': {
    'id': 'pgBG2Bic7vl',
    'displayName': 'Person: Date started ART',
    'valueType': 'DATE',
    'optionSetValue': false
  },
  'fWlJ0KzmOZs': {
    'id': 'fWlJ0KzmOZs',
    'displayName': 'Person: Empowerment Group (Role)',
    'valueType': 'TEXT',
    'optionSetValue': true,
    'optionSet': {
      'options': [
        {
          'code': '0',
          'displayName': 'Chair'
        },
        {
          'code': '1',
          'displayName': 'Vice-chair'
        },
        {
          'code': '2',
          'displayName': 'Secretary'
        },
        {
          'code': '3',
          'displayName': 'Treasurer'
        },
        {
          'code': '4',
          'displayName': 'Member'
        }
      ]
    }
  },
  'Oe2oAS9TfGA': {
    'id': 'Oe2oAS9TfGA',
    'displayName': 'Person: Full Name',
    'valueType': 'TEXT',
    'optionSetValue': false
  },
  'jeiN3PX6zqu': {
    'id': 'jeiN3PX6zqu',
    'displayName': 'Person: Gender',
    'valueType': 'TEXT',
    'optionSetValue': true,
    'optionSet': {
      'options': [
        {
          'code': '1',
          'displayName': 'Male'
        },
        {
          'code': '2',
          'displayName': 'Female'
        },
        {
          'code': '3',
          'displayName': 'Other'
        }
      ]
    }
  },
  'T5ft6x3yYJJ': {
    'id': 'T5ft6x3yYJJ',
    'displayName': 'Person: HIV Status',
    'valueType': 'TEXT',
    'optionSetValue': true,
    'optionSet': {
      'options': [
        {
          'code': '1',
          'displayName': 'Positive'
        },
        {
          'code': '2',
          'displayName': 'Negative'
        },
        {
          'code': '3',
          'displayName': 'Unknown'
        }
      ]
    }
  },
  'OavhN80L78w': {
    'id': 'OavhN80L78w',
    'displayName': 'Person: Individual Economic Activity',
    'valueType': 'TEXT',
    'optionSetValue': true,
    'optionSet': {
      'options': [
        {
          'code': '0',
          'displayName': 'Agriculture'
        },
        {
          'code': '1',
          'displayName': 'Animal Husbandry'
        },
        {
          'code': '2',
          'displayName': 'Saving/Credit Schemes'
        },
        {
          'code': '3',
          'displayName': ' Village Community Banks (VICOBA)'
        },
        {
          'code': '4',
          'displayName': 'Entrepreneurships'
        },
        {
          'code': '5',
          'displayName': 'Rentals'
        }
      ]
    }
  },
  'hDrhKE59EGO': {
    'id': 'hDrhKE59EGO',
    'displayName': 'Person: Initials',
    'valueType': 'TEXT',
    'optionSetValue': false
  },
  'Ek1Ce9w7ua6': {
    'id': 'Ek1Ce9w7ua6',
    'displayName': 'Person: NACOPHA ID',
    'valueType': 'TEXT',
    'optionSetValue': false
  },
  'e3ENws3guve': {
    'id': 'e3ENws3guve',
    'displayName': 'Person: On ART?',
    'valueType': 'BOOLEAN',
    'optionSetValue': false
  },
  'z2q6ASGYCt0': {
    'id': 'z2q6ASGYCt0',
    'displayName': 'Person: Phone Number',
    'valueType': 'PHONE_NUMBER',
    'optionSetValue': false
  }
}
