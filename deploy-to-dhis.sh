#!/bin/bash
# A script to build and deploy a binary of our app, including all related Rollbar tasks
set -e
cd `dirname "$0"`

if [ -z "$JUST_DO_IT" ]; then
  git diff --quiet || (echo 'Working directory is dirty, what are you doing!?! Commit (and push) everything before deploying.' && exit 1)
else
  echo '[WARN] just note that YOU are forcing me to deploy dirty code!'
fi
: ${ROLLBAR_ACCESS_TOKEN:?} # use the post_server_item access token
: ${DHIS_AUTH:?} # expecting user:password

ACCESS_TOKEN=$ROLLBAR_ACCESS_TOKEN
LOCAL_USERNAME=`whoami`
REVISION=`git rev-parse --verify --short HEAD`
targetEnv=$1
if [ -z "$targetEnv" ]; then
  echo '[INFO] no target env was provided as first param, defaulting to "dev". Valid options=dev,train,prod'
  targetEnv=dev
fi
if [ "$targetEnv" == "dev" ]; then
  dhisUrlBase=https://dev.nacopha.maraxis.com
  ROLLBAR_ENV=development
elif [ "$targetEnv" == "train" ]; then
  dhisUrlBase=https://training.nacopha.maraxis.com
  ROLLBAR_ENV=training
elif [ "$targetEnv" == "prod" ]; then
  dhisUrlBase=https://dhis.nacopha.maraxis.com
  ROLLBAR_ENV=production
fi
echo "[INFO] using target env=${targetEnv}, with URL=${dhisUrlBase}"

echo '[INFO] building binary'
./prod-build.sh $ROLLBAR_ENV
echo '[INFO] successfully built binary'

# need to do https://docs.rollbar.com/docs/source-maps/#section-enabling-source-map-translation-in-rollbar
appSrcMap=`find dist/static/js -name 'app.*.js.map'`
deployedAppSrcPath=$dhisUrlBase/api/apps/Nacopha-Offline-Collection/`bash -c "echo $appSrcMap | sed 's+dist/\(.*\).map+\1+'"`

echo '[INFO] deploying source maps'
curl https://api.rollbar.com/api/1/sourcemap \
  -F access_token=$ACCESS_TOKEN \
  -F version=$REVISION \
  -F minified_url=$deployedAppSrcPath \
  -F source_map=@$appSrcMap
  # TODO add all source files, like: -F static/js/site.js=@static/js/site.js \
echo '[INFO] successfully deployed source maps'

echo '[INFO] deploying binary to DHIS'
magicSequence=magicSequenceHttpStatusCode
appsUrl=$dhisUrlBase/api/29/apps
expectedStatusCode=204
# upload the .zip
curlOutput=$(curl \
  -X POST \
  --silent \
  -F file=@dist/nacopha-webapp.zip \
  -u $DHIS_AUTH \
  --write-out "$magicSequence%{http_code}" \
  $appsUrl)
set +e
bash -c "echo '$curlOutput' | grep '${magicSequence}${expectedStatusCode}$'" > /dev/null
rc=$?
set -e
if [ "$rc" -ne "0" ]; then
  echo "[ERROR] failed to upload .zip to server, curl output to follow..."
  echo $curlOutput | sed "s/${magicSequence}.*//"
  exit 1
fi

# refresh the list of apps (I think... You just have to do it, okay!)
curlOutput=$(curl \
  -X PUT \
  --silent \
  -u $DHIS_AUTH \
  --write-out "$magicSequence%{http_code}" \
  $appsUrl)
set +e
bash -c "echo '$curlOutput' | grep '${magicSequence}${expectedStatusCode}$'" > /dev/null
rc=$?
set -e
if [ "$rc" -ne "0" ]; then
  echo "[ERROR] failed to refresh apps on server, curl output to follow..."
  echo $curlOutput | sed "s/${magicSequence}.*//"
  exit 1
fi
echo '[INFO] successfully deployed binary to DHIS'

echo '[INFO] notifying Rollbar of deployment'
curl https://api.rollbar.com/api/1/deploy/ \
  -F access_token=$ACCESS_TOKEN \
  -F environment=$ROLLBAR_ENV \
  -F revision=$REVISION \
  -F local_username=$LOCAL_USERNAME
echo "[INFO] Notified Rollbar of deployment to env=$ROLLBAR_ENV, rev=$REVISION"
